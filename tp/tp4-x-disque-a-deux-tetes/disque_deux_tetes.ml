let coutDe r d =
  assert (Array.length r = Array.length d);
  (* on note les positions des têtes pos.(0) et pos.(1) *)
  let pos = Array.make 2 0 in
  (* coût des déplacements effectués : *)
  let c = ref 0 in
  let n = Array.length d in
  for i = 0 to n-1 do
    let t = d.(i) - 1 in
    c := !c + abs(r.(i) - pos.(t));
    pos.(t) <- r.(i)
  done;
  !c

let coutOpt3 r1 r2 r3 =
  let c d2 d3 = coutDe [| r1; r2; r3 |] [| 1; d2; d3 |] in
  let opt d2 = min (c d2 1) (c d2 2) in
  min (opt 1) (opt 2)
;;
