type formation = Formation of int
let formation_no n = Formation n
let no_formation (Formation n) = n

type candidat = Candidat of int
let candidat_no n = Candidat n
let no_candidat (Candidat n) = n

(* Le classement des candidats dans une formation *)
type classement = {
    mutable liste : candidat list;
}

let taille_classement cl = List.length cl.liste
                         
(* voeux d'un candidat, triés par ordre décroissant de préférence *)
type voeux = formation array

type capacites = int array (* capacités des différentes formations *)

type probleme = {
  (* pour chaque candidat, une liste des formations désirées,
     classées par ordre décroissant de préférence *)
  voeux : voeux array;
  (* pour chaque formation, sa capacité *)
  capacites : capacites;
  (* pour chaque formation, une liste des candidats, classés
     par ordre décroissant de priorité sur la formation *)
  classements : classement array;
}

(* seuils associés à chaque formation *)
type seuils = int array

(* Une affectation est la donnée :
   - Pour chaque candidat [c], d'un numéro [r] tel que le candidat [c]
     de numéro [nc] se voit proposer son vœux de rang [r],
     c'est-à-dire la formation [voeux.(nc).(r)].
     Il se peut qu'aucun des vœux du candidat ne soit accepté, d'où
     l'utilisation du type [option].
   - Pour chaque formation, du nombre de places occupées dans la
     formation par cette affectation.
 *)
type affectation = {
  candidats : int option array;
  sur_formations : formation option array;
  charges : int array;
}

let nb_formations pb = Array.length pb.capacites
let nb_candidats pb = Array.length pb.voeux
                     
(* Seuil initial dans l'algorithme SOSM :
   on accepte dans chaque formation tous les étudiants du classement
 *)
let seuil_initial_sosm pb =
  let nbf = nb_formations pb in
  Array.init nbf (fun nf -> taille_classement pb.classements.(nf))

let rec index x l =
  match l with
  | [] -> raise Not_found
  | y::r -> if x = y then 0
            else 1 + index x r
          
let rang_dans_formation pb c f =
  let cl = pb.classements.(no_formation f).liste in
  index c cl
  
(* renvoie le rang du meilleur voeu de rang supérieur ou égal à k en-dessous
   du seuil pour l'étudiant c.
   Renvoie None s'il y a aucun voeu acceptable de rang supérieur ou
   égal à k *)
let rec meilleur_voeu_a_partir pb seuil c k =
  let v = pb.voeux.(no_candidat c) in
  let nbv = Array.length v in
  if k >= nbv then None
  else
    let f = v.(k) in
    let nf = no_formation f in
    let s = seuil.(nf) in
    let r = rang_dans_formation pb c f in
    if r < s then Some k
    else meilleur_voeu_a_partir pb seuil c (k+1)
  
let meilleur_voeu pb seuil nc = meilleur_voeu_a_partir pb seuil nc 0  
  
let affectation pb seuil =
  let nbc = nb_candidats pb in
  let nbf = nb_formations pb in
  let candidats = Array.init nbc (fun nc -> meilleur_voeu pb seuil (candidat_no nc)) in
  let charges = Array.make nbf 0 in
  let sur_formations =
    Array.init nbc
      (fun nc -> let k = candidats.(nc) in
                 match k with
                 | None -> None
                 | Some i -> let f = pb.voeux.(nc).(i) in
                             let nf = no_formation f in
                             let () = charges.(nf) <- charges.(nf) + 1 in
                             Some f)
  in
  { candidats; charges; sur_formations; }
  
