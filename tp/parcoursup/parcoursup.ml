
let pr_string s fmt = Format.fprintf fmt "%s" s
let pr_format s fmt = Format.fprintf fmt s

let pr_array pr_elem a fmt =
  Format.fprintf fmt "@[<hov 3>[|@ ";
  Array.iter (fun e ->
      Format.fprintf fmt "@[<hov 0>";
      pr_elem e fmt;
      Format.fprintf fmt "@];@ ") a;
  Format.fprintf fmt "|]@]@,"

let pr_list pr_elem a fmt =
  Format.fprintf fmt "@[<hov 2>[@ ";
  List.iter (fun e ->
      Format.fprintf fmt "@[<hov 0>";
      pr_elem e fmt;
      Format.fprintf fmt "@];@ ") a;
  Format.fprintf fmt "]@]@,"

let pr_int n fmt = Format.fprintf fmt "%d" n

let pr_seq s fmt = List.iter (fun f -> f fmt) s

let pr_record seq fmt =
  pr_format "@[<hov 2>{@ " fmt;
  List.iter (fun (name, f) ->
      Format.fprintf fmt "@[<hov 2>%s = @[<hov 2>" name;
      f fmt;
      Format.fprintf fmt ";@]@]@ ")
    seq;
  pr_format "}@]@]@," fmt

let pr_option pr x fmt =
  match x with
  | None -> Format.fprintf fmt "None"
  | Some y -> (Format.fprintf fmt "@[<hov 2>(Some@ ";
               pr y fmt;
               Format.fprintf fmt ")@]")

let swap t i j =
  let vi = t.(i) in
  let vj = t.(j) in
  t.(j) <- vi;
  t.(i) <- vj

(* [melange : 'a array -> unit]
   melange le tableau, c'est-à-dire lui applique une permutation aléatoire,
   tirée uniformémement parmi l'ensemble des permutations du tableau.
*)
let melange t =
  let n = Array.length t in
  for i = n downto 2 do
    let j = Random.int i in
    swap t j (i-1)
  done

(* [melange_aux : 'a array -> int -> unit]
   mélange le tableau de façon à mettre, en fin du tableau,
   [k] éléments tirés au hasard de façon uniforme parmi les [n] éléments du tableau.
*)
let melange_aux k t =
  let n = Array.length t in
  assert (k <= n);
  for i = 0 to k-1 do
    let j = Random.int (n-i) in
    swap t j (n-i-1)
  done

(* [trouve_index : 'a -> 'a array -> int]
   retourne l'indice du premier élément du tableau t égal à x
*)
let trouve_index x t =
  let n = Array.length t in
  (* aux i retourne le plus petit indice [j>=i] tel que [t.(j) = x] *)
  let rec aux i =
    if i >= n then raise Not_found;
    if t.(i) = x then i
    else aux (i+1)
  in
  aux 0

(* table d'association abstraite, indexée par des entiers *)
type ('a, 'b) tableau = {
  ind : int -> 'a;
  revind : 'a -> int;
  table : 'b array;
  get : 'a -> 'b;
  set : 'a -> 'b -> unit;
}

let itere_tableau t (f : 'a -> 'b -> unit) =
  for i = 0 to Array.length t.table - 1 do
    f (t.ind i) t.table.(i)
  done

let pr_tableau pr_a pr_b t fmt =
  Format.fprintf fmt "@[<hov 3>[|@ ";
  itere_tableau t (fun a b ->
      Format.fprintf fmt "@[<hov 2>";
      pr_a a fmt;
      Format.fprintf fmt "@ ->@ ";
      pr_b b fmt;
      Format.fprintf fmt "@];@ ");
  Format.fprintf fmt "|]@]@,"

let taille t = Array.length t.table
    
let tableau_of_array ind revind table =
  let get i = table.(revind i) in
  let set i x = table.(revind i) <- x in
  { ind; revind; table; get; set; }

let mapi t f =
  let u = Array.mapi (fun i a -> f (t.ind i) a) t.table in
  tableau_of_array t.ind t.revind u
  

type formation = Formation of int
let formation_no n = Formation n
let no_formation (Formation n) = n

let pr_formation f fmt = Format.fprintf fmt "f%d" (no_formation f)

let formation_tableau_of_array t =
  tableau_of_array formation_no no_formation t
    
let init_formation_tableau nbf f =
  let t = Array.init nbf (fun i -> f (formation_no i)) in
  formation_tableau_of_array t

type candidat = Candidat of int
let candidat_no n = Candidat n
let no_candidat (Candidat n) = n

let pr_candidat c fmt = Format.fprintf fmt "c%d" (no_candidat c)

let candidat_tableau_of_array t =
  tableau_of_array candidat_no no_candidat t
    
let init_candidat_tableau nbc f =
  let t = Array.init nbc (fun i -> f (candidat_no i)) in
  candidat_tableau_of_array t

(* Le classement des candidats dans une formation *)
type classement = {
  mutable liste : candidat list;
}

let pr_classement cl = pr_list pr_candidat cl.liste

(* les fonctions associées *)

let ajoute_candidat c cl =
  cl.liste <- c :: cl.liste

let taille_classement cl = List.length cl.liste

(* [rang_candidat : candidat -> classement -> int] *)
let rang_candidat c (cl : classement) =
  let rec rang_liste x l = match l with
    | [] -> raise Not_found
    | y::r -> if x = y then 0 else 1 + rang_liste x r
  in
  rang_liste c cl.liste
;;

(* retourne le candidat de rang [r] *)
let candidat_de_rang (cl : classement) r = List.nth cl.liste r;;

(* [cree_classement_vide : unit -> classement] *)
let cree_classement_vide () = { liste = []; }

(* [tire_classement : classement -> unit]
   permute les candidats du classement au hasard
*)
let tire_classement cl =
  let t = Array.of_list cl.liste in
  melange t;
  cl.liste <- Array.to_list t

(* Les voeux d'un candidat *)
type voeux = formation array

let pr_voeux = pr_array pr_formation
    
(* Les capacités des formations *)
type capacites = (formation, int) tableau

let pr_capacites = pr_tableau pr_formation pr_int
    
type probleme = {
  (* pour chaque candidat, une liste des formations désirées,
     classées par ordre décroissant de préférence *)
  voeux : (candidat, voeux) tableau;
  (* pour chaque formation, sa capacité *)
  capacites : capacites;
  (* pour chaque formation, une liste des candidats, classés
     par ordre décroissant de priorité sur la formation *)
  classements : (formation, classement) tableau;
}

    
let pr_probleme pb = pr_record
    [ "voeux", pr_tableau pr_candidat pr_voeux pb.voeux;
      "capacites", pr_capacites pb.capacites;
      "classements", pr_tableau pr_formation pr_classement pb.classements;
    ]

let nb_candidats pb = taille pb.voeux
    
let tire_probleme nbc nbvoeux nbf =
  let formations = Array.init nbf formation_no in
  let tire_voeux () =
    let () = melange_aux nbvoeux formations in
    Array.sub formations (nbf - nbvoeux) nbvoeux
  in
  let voeux = init_candidat_tableau nbc (fun c -> tire_voeux()) in
  let classements = init_formation_tableau nbf (fun f -> cree_classement_vide ()) in
  (* on recense les candidats sur les formations *)
  let () = itere_tableau voeux
      (fun c v ->
         Array.iter
           (fun f -> ajoute_candidat c (classements.get f))
           v) in
  (* on classe les candidats sur chaque formation *)
  let () = itere_tableau classements
      (fun f cl -> tire_classement cl) in
  (* on fixe arbitrairement la capacité de chaque formation *)
  let capacites = init_formation_tableau nbf
      (fun f -> taille_classement (classements.get f) / nbvoeux) in
  { voeux; capacites; classements; }

(* les seuils d'affectations sont la donnée, pour chaque formation [f]
   d'une valeur [m] telle que [f] acceptera tous les candidats dont
   le rang [r] vérifie [r < m].
*)
type seuils = (formation, int) tableau

let pr_seuils = pr_tableau pr_formation pr_int

(* Une affectation est la donnée :
   \begin{itemize}
   \item Pour chaque candidat [c], d'un numéro [r] tel que le candidat [c]
     de numéro [nc] se voit proposer son vœux de rang [r],
     c'est-à-dire la formation [(voeux.app c).app r].
     Il se peut qu'aucun des vœux du candidat ne soit accepté, d'où
     l'utilisation du type [option].
   \item Pour chaque formation, du nombre de places occupées dans la
     formation par cette affectation.
  \end{itemize}
*)
type affectation = {
  candidats : (candidat, int option) tableau;
  sur_formations : (candidat, formation option) tableau;
  charges : (formation, int) tableau;
}

let pr_affectation a = pr_record [
    "candidats", pr_tableau pr_candidat (pr_option pr_int) a.candidats;
    "sur_formations", pr_tableau pr_candidat (pr_option pr_formation) a.sur_formations;
    "charges", pr_tableau pr_formation pr_int a.charges;
  ]

(* [rang_meilleur_voeu_a_partir : probleme -> seuils -> candidat -> int -> option int]
   retourne le rang du meilleur voeu de numéro supérieur ou égal à [rv]
   sur lequel le candidat est admis, ou [None] s'il n'en existe aucun.
*)
let rec rang_meilleur_voeu_a_partir pb seuils c rv =
  let v = pb.voeux.get c in
  let nbv = Array.length v in
  if rv >= nbv then None (* pas d'affectation possible *)
  else
    let f = v.(rv) in
    let rc = rang_candidat c (pb.classements.get f) in
    let s = seuils.get f in
    if rc < s then Some rv
    else rang_meilleur_voeu_a_partir pb seuils c (rv+1)

(* [rang_meilleur_voeu : probleme -> seuils -> candidat -> int option] *)
let rang_meilleur_voeu pb seuils c =
  rang_meilleur_voeu_a_partir pb seuils c 0

(* [affecte : probleme -> seuils -> affectation] *)
let affecte pb seuils =
  let charges = mapi pb.classements (fun f _ -> 0) in
  let candidats = mapi pb.voeux
      (fun c _ ->
         let ro = rang_meilleur_voeu pb seuils c in
         let () = match ro with
           | Some r ->
               let f = (pb.voeux.get c).(r) in
               let n = charges.get f in
               charges.set f (n+1)
           | None -> ()
         in
         ro)
  in
  let sur_formations =
    mapi candidats
      (fun c ro -> match ro with
         | None -> None
         | Some r -> Some (pb.voeux.get c).(r))
  in
  { charges; candidats; sur_formations }

(* Seuils initiaux, version école proposant *)
let seuils_initiaux_ecole_prop pb =
  mapi pb.capacites (fun _ cap -> cap)

(* Seuils initiaux, version candidat proposant *)
let seuils_initiaux_candidat_prop pb =
  mapi pb.capacites (fun f _ -> taille_classement (pb.classements.get f))

(* [adapte_formation : probleme -> seuils -> affectation -> formation -> unit]
   adapte le seuil d'une formation au vu de l'affectation.
*)
let adapte_seuil_formation pb s a f =
  let cap = pb.capacites in
  let nbc = taille_classement (pb.classements.get f) in
  let d = cap.get f - a.charges.get f in
  (* Si [d < 0], on diminue le seuil de [ |d| ] rangs,
     sinon on l'augmente de [ |d| ] rangs, sans pour autant
     dépasser le nombre de candidats classés sur la formation.
  *)
  s.set f (min (s.get f + d) nbc)

(* [adapte_seuils : probleme -> seuils -> affectation -> unit] *)
let adapte_seuils pb s a =
  itere_tableau a.charges (fun f _ -> adapte_seuil_formation pb s a f)

let test1 () =
  let p = tire_probleme 40 4 5 in
  let fmt = Format.std_formatter in
  let () = pr_probleme p fmt in
  let s = seuils_initiaux_ecole_prop p in
  let () = pr_seuils s fmt in
  let a = affecte p s in
  let () = pr_affectation a fmt in
  let () = adapte_seuils p s a in
  let () = pr_seuils s fmt in
  let () = pr_affectation a fmt in
  let () = adapte_seuils p s a in
  let () = pr_seuils s fmt in
  ()

let () = test1 ()
    
let test2 () =
  let alpha = formation_no 0 in
  let beta = formation_no 1 in
  let alice = candidat_no 0 in
  let bea = candidat_no 1 in
  let v_alice = [| alpha; beta |] in
  let v_bea = [| beta; alpha |] in
  let voeux = candidat_tableau_of_array [| v_alice; v_bea |] in
  let capacites = formation_tableau_of_array [| 1; 1 |] in
  let cl_alpha = { liste = [bea; alice] } in
  let cl_beta = { liste = [bea; alice] } in
  let classements = formation_tableau_of_array [| cl_alpha; cl_beta |] in
  let p = { voeux; capacites; classements; } in
  let fmt = Format.std_formatter in
  let () = Format.fprintf fmt "@[<hv>TEST 2@ " in
  let () = pr_probleme p fmt in
  let s = seuils_initiaux_ecole_prop p in
  let () = pr_seuils s fmt in
  let a = affecte p s in
  let () = pr_affectation a fmt in
  let () = adapte_seuils p s a in
  let () = pr_seuils s fmt in
  let a2 = affecte p s in
  let () = pr_affectation a2 fmt in
  let () = adapte_seuils p s a2 in
  let () = pr_seuils s fmt in
  let a3 = affecte p s in
  let () = pr_affectation a3 fmt in
  let r = rang_meilleur_voeu p s alice in
  let () = pr_option pr_int r fmt in
  let () = Format.fprintf fmt "@]@ " in
  ()


let () = test2 ()

(* alice et béa, situation bloquante en école proposantes *)
let test3 () =
  let alpha = formation_no 0 in
  let beta = formation_no 1 in
  let alice = candidat_no 0 in
  let bea = candidat_no 1 in
  let v_alice = [| alpha; beta |] in
  let v_bea = [| beta; alpha |] in
  let voeux = candidat_tableau_of_array [| v_alice; v_bea |] in
  let capacites = formation_tableau_of_array [| 1; 1 |] in
  let cl_alpha = { liste = [bea; alice] } in
  let cl_beta = { liste = [alice; bea] } in
  let classements = formation_tableau_of_array [| cl_alpha; cl_beta |] in
  let p = { voeux; capacites; classements; } in
  let fmt = Format.std_formatter in
  let () = Format.fprintf fmt "@[<hv>TEST 3@ " in
  let () = pr_probleme p fmt in
  let s = seuils_initiaux_ecole_prop p in
  let () = pr_seuils s fmt in
  let a = affecte p s in
  let () = pr_affectation a fmt in
  let () = adapte_seuils p s a in
  let () = pr_seuils s fmt in
  let a2 = affecte p s in
  let () = pr_affectation a2 fmt in
  let () = adapte_seuils p s a2 in
  let () = pr_seuils s fmt in
  let a3 = affecte p s in
  let () = pr_affectation a3 fmt in
  let r = rang_meilleur_voeu p s alice in
  let () = pr_option pr_int r fmt in
  let () = Format.fprintf fmt "@]@ " in
  ()

let () = test3 ()

(* alice et béa, même situation qu'en [test3] (situation bloquante en
   école proposantes) mais ici en version candidat proposant *)
let test4 () =
  let alpha = formation_no 0 in
  let beta = formation_no 1 in
  let alice = candidat_no 0 in
  let bea = candidat_no 1 in
  let v_alice = [| alpha; beta |] in
  let v_bea = [| beta; alpha |] in
  let voeux = candidat_tableau_of_array [| v_alice; v_bea |] in
  let capacites = formation_tableau_of_array [| 1; 1 |] in
  let cl_alpha = { liste = [bea; alice] } in
  let cl_beta = { liste = [alice; bea] } in
  let classements = formation_tableau_of_array [| cl_alpha; cl_beta |] in
  let p = { voeux; capacites; classements; } in
  let fmt = Format.std_formatter in
  let () = Format.fprintf fmt "@[<hv>TEST 4@ " in
  let () = pr_probleme p fmt in
  let s = seuils_initiaux_candidat_prop p in
  let () = pr_seuils s fmt in
  let a = affecte p s in
  let () = pr_affectation a fmt in
  let () = adapte_seuils p s a in
  let () = pr_seuils s fmt in
  let a2 = affecte p s in
  let () = pr_affectation a2 fmt in
  let () = adapte_seuils p s a2 in
  let () = pr_seuils s fmt in
  let a3 = affecte p s in
  let () = pr_affectation a3 fmt in
  let r = rang_meilleur_voeu p s alice in
  let () = pr_option pr_int r fmt in
  let () = Format.fprintf fmt "@]@ " in
  ()

let () = test4 ()

