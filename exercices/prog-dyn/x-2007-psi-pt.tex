\emph{D'après le sujet Polytechnique 2007, PSI/PT}

Un tailleur de tissus dispose d'une grande pi\`ece de tissu ainsi que d'une liste de 
$n$ produits qu'il peut fabriquer \`a l'aide de ce tissu. La pi\`ece de tissu n'a qu'une 
seule dimension, il s'agit donc d'un ruban de longueur $\ell{}$. Pour chaque produit, 
le tailleur sait quelle quantit\'e de tissu est n\'ecessaire et il conna\^it son prix de vente. 
Le tailleur doit d\'ecider quels produits il va r\'ealiser pour maximiser sa rentr\'ee 
d'argent $M$. Chacun des $n$ produits r\'ealisables a une longueur $a_{i}$ et un prix 
de vente $v_{i}$, ($0 \leq i < n$). Toutes les dimensions sont des 
entiers strictement positifs. Les prix de ventes sont des flottants
strictement positifs.
%
On rappelle qu'on dispose des fonctions suivantes en OCaml:
\begin{lstlisting}
Array.length : 'a array -> int
Array.make : int -> 'a -> 'a array
Array.make_matrix : int -> int -> 'a -> 'a array array
print_int : int -> unit
print_newline : unit -> unit
\end{lstlisting}

\begin{partie}{Coupe d'un ruban avec répétition}
Dans cette partie, chaque produit peut \^etre r\'ealis\'e un nombre arbitraire de fois 
(\'eventuellement nul). Un d\'ecoupage (non optimal) possible, pour
lequel on obtient $M=8$, est illustr\'e ci-dessous 
pour $n = 2$.

\begin{center}
\includegraphics[width=\linewidth]{x-2007-psi-pt-img.pdf}
\end{center}

Informatiquement, on stockera la suite des longueurs 
$\langle a_{0}, a_{1} , \dots, a_{n-1} \rangle$ et des prix de vente 
$\langle v_{0}, v_{1}, \dots, v_{n-1} \rangle$ dans deux tableaux $a$ et $v$ 
de $n$ \'el\'ements. On remarquera aussi que la d\'ecoupe du tissu est commutative, 
puisque d\'ecouper le produit $i$ puis le produit $j$ donne le m\^eme r\'esultat que 
d\'ecouper le produit $j$ puis le produit $i$.

\begin{ques}
Consid\'erer l'exemple suivant : $n = 2$, $a_{0} = 6$, $a_{1} = 8$, $v_{0} = 3$ et 
$v_{1} = 5$. Calculer $M$ pour $\ell{} = 14$, $16$, $18$, $24$.
\end{ques}

\begin{ques}
  Reprendre l'exemple pr\'ec\'edent avec $v_{0} = 4$.
\end{ques}
\bigskip
On se place d'abord dans les cas $n = 2$ ou $n = 3$.
Pour $n = 2$, on calcule tous les prix de vente obtenus en coupant $0$, $1$, $2$, \dots, 
ou $\floor{\ell{} / a_{0}}$ fois le premier produit et le reste du
tissu avec le deuxi\`eme produit.
Et on retient le $M$ maximal.
De m\^eme pour $n = 3$, on commence par couper plusieurs fois le premier produit, 
puis le deuxi\`eme produit et le reste avec le troisi\`eme produit.

  \begin{ques}
    \'Ecrire une fonction \lstinline{coutRuban2Produits}, de type
    \lstinline{int ->} \lstinline{float ->} \lstinline{int ->}
    \lstinline{float ->} \lstinline{int -> float} répondant à
    la spécification suivante: Étant donnés les longueurs $a_{0}$,
    $a_{1}$ et les prix de ventes $v_{0}$, $v_{1}$ de deux produits et
    la longueur totale $\ell{}$ du ruban de tissu,
    \lstinline{(coutRuban2Produits $a_{0}$ $v_{0}$ $a_{1}$ $v_{1}$ $\ell{}$)}
    retourne le prix de vente maximal $M$ possible
    apr\`es d\'ecoupe.
  \end{ques}

\begin{ques}
  \'Ecrire une fonction \lstinline{coutRuban3Produits}, de type
  \lstinline{int -> float ->} \lstinline{int -> float ->}
  \lstinline{int -> float ->} \lstinline{int -> float} 
  répondant à la spécification suivante: Étant donnés les
  longueurs $a_{0}$, $a_{1}$, $a_{2}$ et les prix de ventes $v_{0}$,
  $v_{1}$, $v_{2}$ de trois produits et la longueur totale $\ell{}$ du
  ruban de tissu, 
  \lstinline{(coutRuban3Produits $a_{0}$ $v_{0}$ $a_{1}$ $v_{1}$ $a_{2}$ $v_{2}$ $\ell{}$)}
  retourne le prix de
  vente maximal $M$ possible apr\`es d\'ecoupe.
\end{ques}

\medskip
On peut montrer qu'un tel algorithme de calcul prend un temps exponentiel en 
$\grandO{\ell{}^{n}}$ dans le cas g\'en\'eral de $n$ produits.
On va donc utiliser une m\'ethode dite de {\og}programmation dynamique{\fg} 
dans ce cas g\'en\'eral en prenant pour acquise la formule de r\'ecurrence suivante, o\`u 
$M(x)$ est le co\^ut maximal obtenu apr\`es d\'ecoupe des $n$ produits pour un 
ruban de longueur $x$ :

\centerline{$M(x) = \max{\set{M(x-a_{i}) + v_{i} | 0 \leq i < n, a_{i} \leq x}}$}

Par convention, on posera que le maximum de l'ensemble vide vaut $0$.
On stockera les valeurs de $M(x)$ pour $0 \leq x \leq \ell{}$ dans un tableau \lstinline{m}.

  \begin{ques}
\'Ecrire une fonction
\lstinline{coutRuban} de type \lstinline{int array ->}
\lstinline{float array ->} \lstinline{int ->} \lstinline{float}
répondant à la spécification suivante: Étant donnés les 
tableaux $a$ des longueurs et $v$ des prix de vente des $n$ produits, ainsi que la 
longueur $\ell{}$ du ruban,
\lstinline{(coutRuban $a$ $v$ $\ell{}$)} retourne le prix de
vente maximal $M(\ell{})$ en calculant le tableau \lstinline{m} d\'ecrit pr\'ec\'edemment.
Donner un ordre de grandeur du temps d'ex\'ecution de cette fonction.
  \end{ques}

\medskip
Pour imprimer la solution donnant le prix de vente maximal, il faut se souvenir de la coupe 
pratiqu\'ee pour obtenir ce $M(x)$ maximal.
Pour cela, on utilise un tableau \lstinline{d} tel que
\lstinline{d.(x)} donne l'indice $i$ du produit 
tel que $M(x-a_{i}) + v_{i}$ soit maximal. Lorsque $M(x) = 0$, on
affectera \lstinline{d.(0)} à $-1$.
Une liste de d\'ecoupes r\'ealisant la valeur $M(\ell{})$ peut alors \^etre retrouv\'ee \`a partir 
du tableau $d$. Par exemple, la premi\`ere coupe est
\lstinline{$i_{0} = $d.($\ell{}$)}, la seconde 
\lstinline{$i_{1} = $ d.($\ell{}$-a.($i_{0}$))}, {\dots} jusqu'\`a ce qu'on tombe sur la 
valeur par d\'efaut $-1$.

\medskip
  \begin{ques}
\'Ecrire une fonction
\lstinline{decoupageRuban} de type
 \lstinline{int array ->} \lstinline{float array ->}
 \lstinline{int ->} \lstinline{unit} répondant à la
 spécification suivante: étant donnés
les tableaux $a$ des longueurs et $v$ des prix de vente, ainsi que la longueur $\ell{}$ du 
ruban, \lstinline{(decoupageRuban $a$ $v$ $\ell{}$)} imprime la
liste des d\'ecoupes successives produisant la valeur $M(\ell{})$, en
calculant le tableau \lstinline{d}.

Donner un ordre de grandeur du temps d'ex\'ecution de cette fonction.
  \end{ques}

\end{partie}

\begin{partie}{Coupe d'un ruban sans r\'ep\'etition}

Dans cette partie, le tailleur ne peut fabriquer plus d'une fois le produit $i$ car la 
demande n'exc\`ede pas ce nombre.

\medskip
  \begin{ques}
Trouver une formule de r\'ecurrence donnant le co\^ut maximal $M(i, x)$ que l'on 
peut obtenir avec un ruban de longueur $x$ en ne d\'ecoupant que parmi les produits 
$0$, $1$, \dots, $i-1$ (en particulier $M(0, x)$ vaut $0$).
  \end{ques}

\medskip
On stockera les valeurs de $M(i, x)$ pour $0 \leq i < n$ et $0 \leq
x \leq \ell{}$ dans un tableau \lstinline{m} \`a deux dimensions.

\medskip
\begin{sloppypar}
  \begin{ques}
\'Ecrire une fonction
\lstinline{coutRubanSansRepetitions} de type
\lstinline{int array ->} \lstinline{float array ->} \lstinline{int ->}
\lstinline{float}
répondant à la spécification suivante:
Étant donnés les tableaux $a$ des longueurs  et $v$ des prix de vente
des $n$ produits 
ainsi que la longueur $\ell{}$ du ruban,
\lstinline{(coutRubanSansRepetitions $a$ $v$ $\ell{}$)}
retourne le prix de vente maximal 
$M(n, \ell{})$ en calculant le tableau \lstinline{m} d\'ecrit pr\'ec\'edemment.
Donner un ordre de grandeur du temps d'ex\'ecution de cette fonction.
  \end{ques}
\end{sloppypar}

\medskip
\begin{ques}
  Récrire cette fonction en ne se servant que de deux tableaux de
  $\ell{}+1$ \'el\'ements, r\'eduisant la taille m\'emoire
  utilis\'ee \`a $\grandO{2(\ell{}+1)}$.
\end{ques}
\end{partie}
