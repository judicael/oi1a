\begin{partie}{Coupe d'un ruban avec répétition}
\begin{ques}
Pour $\ell = 14$, on trouve $M=8$, pour $\ell=16$, $M=10$ pour
$\ell=18$, $M=10$ et pour $\ell=24$, on trouve $M=15$.
\end{ques}

\begin{ques}
  Avec  $v_{0} = 4$, on trouve les valeurs suivantes:

  \begin{tabular}{rr}\toprule
    $\ell$ & $M$\\ \midrule
    14 & 9 \\
    16 & 10\\
    18 & 12\\
    24 & 16
  \end{tabular}
\end{ques}
\bigskip
  \begin{ques}
    \begin{sloppypar}
    Nous introduisons tout d'abord une fonction
    \lstinline{max_int} de type
    \lstinline{int -> int -> (int -> float) -> float}
    telle que \lstinline{max_int $a$ $b$ $f$} retourne le maximum de la
    fonction $f$ sur l'intervalle d'entiers \ii{a,b}, sous
    la précondition $a < b$.
    \end{sloppypar}
    \begin{lstlisting}
      let rec max_int a b f =
        if b - a = 1 then f a
        else max (f a) (max_int (a+1) b f)
    \end{lstlisting}
    On peut alors aisément donner la fonction cherchée:
    \begin{lstlisting}
let coutRuban2Produits a0 v0 a1 v1 l =
  let p = l / a0 in (* nb max de morceaux de type 0 *)
  max_int 0 (p+1)
    (fun n0 -> let l2 = (l - a0 * n0) in
        (* on note n1 le nombre max de morceaux de type 1 lorsqu'on a
           enlevé n0 morceaux de type 0 : *) 
        let n1 = l2 / a1 in
        float_of_int n0 *. v0 +. float_of_int n1 *. v1)
    \end{lstlisting}
  \end{ques}

\begin{ques}
  On peut écrire la fonction \lstinline{coutRuban3Produits} de façon
  similaire: pour chaque choix possible $n_0$ d'un nombre de morceaux
  de type $0$, on regarde comment maximiser le prix qu'on peut tirer
  du reste du ruban avec les produits de type $1$ et $2$:
  \begin{lstlisting}
let coutRuban3Produits a0 v0 a1 v1 a2 v2 l =
  let p = l / a0 in
  max_int 0 (p+1) (fun n0 -> float_of_int n0 *. v0
                             +. coutRuban2Produits a1 v1 a2 v2 (l-n0*a0))
  \end{lstlisting}
\end{ques}

  \begin{ques}
    \begin{lstlisting}
let coutRuban a v l =
  let n = Array.length a in
  let m = Array.make (l+1) nan in
  m.(0) <- 0.;
  for x = 1 to l do
    m.(x) <- max_int 0 n
      (fun i -> if a.(i) > x then 0. else m.(x-a.(i)) +. v.(i));
  done;
  m.(l)    
    \end{lstlisting}
On sait que la fonction \lstinline{Array.length} s'exécute en temps
$O(1)$, le calcul de \lstinline{Array.make (l+1)} en temps
$O(\ell)$. Chaque appel à la fonction anonyme à l'intérieur de la boucle
s'effectue en temps $O(1)$ donc chaque appel à \lstinline{max_int}
s'effectue en temps $O(n)$ où $n$ est le nombre de types de
produits. Or la boucle effectue $\ell$ tours, d'où une complexité
temporelle en $O(n\ell)$ pour la fonction.
  \end{ques}

\medskip
  \begin{ques}
    \begin{sloppypar}
    Nous introduisons tout d'abord une fonction
    \lstinline{imax_int} de type
    \lstinline{int -> int -> (int -> float) -> int * float}
    telle que \lstinline{imax_int $a$ $b$ $f$}, sous la précondition
    $a < b$, retourne un couple $(i, v)$ tel que $v$ est le maximum de
    la fonction $f$ sur l'intervalle d'entiers \ii{a,b} et $i$ est un
    entier de l'intervalle où ce maximum est atteint.
    \end{sloppypar}
    \begin{lstlisting}
      let rec imax_int a b f =
        let va = f a in
        if b - a = 1 then a, va
        else let i, v = imax_int (a+1) b f in
             if v < va then i, v else a, va
    \end{lstlisting}
    Il suffit alors d'adapter la fonction précédente pour calculer à
    la fois le prix maximal et le découpage correspondant comme suit:
    \begin{lstlisting}
let decoupageRuban a v l =
  let n = Array.length a in
  let m = Array.make (l+1) nan in
  let d = Array.make (n+1) (-2) in
  m.(0) <- 0.;
  d.(0) <- -1;
  (* on calcule m et d *)
  for x = 1 to l do
    let i, vi = imax_int 0 n
      (fun i -> if a.(i) > x then 0. else m.(x-a.(i)) +. v.(i)) in
    m.(x) <- vi;
    d.(x) <- if vi > 0. then i else -1;
  done;
  (* affiche : int -> unit
     (affiche k) affiche le découpage à effectuer à partir d'un
     morceau de longueur k, pour k <= l : *)
  let rec affiche k =
    let i = d.(k) in
    (* si i = -1, plus aucun découpage n'est possible *)
    if i <> -1 then
      let () = print_string "Découper un morceau de type " in
      let () = print_int i in
      let () = print_string " qui est de longueur " in
      let () = print_int a.(i) in
      let () = print_newline () in
      affiche (k-a.(i))
  in
  affiche l
    \end{lstlisting}
Comme pour la fonction précédente, la boucle \lstinline{for} s'exécute
en temps $O(n\ell)$ où $n$ est le nombre de types de produits. Quant à
l'appel \lstinline{affiche l}, son temps d'exécution est dominé par le
nombre d'appels à la fonction \lstinline{affiche} car celle-ci, en
dehors de l'éventuel appel récursif, s'exécute en temps constant. Or
\lstinline{affiche} est appelée au plus $\ell+1$ fois. D'où une
complexité pour l'exécution de \lstinline{decoupageRuban a v l} en
$O(n\ell)$ où $n$ est le nombre de types de produits.
  \end{ques}

\end{partie}

\begin{partie}{Coupe d'un ruban sans r\'ep\'etition}

Dans cette partie, le tailleur ne peut fabriquer plus d'une fois le produit $i$ car la 
demande n'exc\`ede pas ce nombre.

\medskip
  \begin{ques}
Pour toute longueur $x$, on a évidemment $M(0, x) = 0$
et pour tout $i > 0$, on a de plus
\begin{equation*}
  M(i,x) =
  \begin{cases}
    \max (M(i-1, x-a_{i-1})+v_i, M(i-1, x))&\text{si $x\geq
      a_{i-1}$}\\
    M(i-1, x) & \text{sinon}
  \end{cases}
\end{equation*}
  \end{ques}

\begin{sloppypar}
  \begin{ques}
    \begin{lstlisting}
let coutRubanSansRepetition a v l =
  let n = Array.length a in
  let m = Array.make_matrix (n+1) (l+1) 0. in
  (* la première ligne de la matrice est déjà initialisée à 0,
     on passe au calcul des suivantes en utilisant la formule de
     récurrence : *)
  for i = 1 to n do
    for x = 0 to l do
      m.(i).(x) <- if x >= a.(i-1) then 
                     max (m.(i-1).(x-a.(i-1)) +. v.(i)) m.(i-1).(x)
                   else m.(i-1).(x)
    done
  done;
  m.(n).(l)
    \end{lstlisting}
La complexité de la création de la matrice est en $O(n\ell)$ où $n$
est le nombre de types de produits, celle des deux boucles
imbriquées aussi, d'où la complexité de la fonction: $O(n\ell)$ où $n$
est le nombre de types de produits.
  \end{ques}
\end{sloppypar}

\begin{ques}
  \begin{lstinline}
let coutRubanSansRepetition a v l =
  let n = Array.length a in
  (* on stockera m.(i-1) et m.(i) dans ces variables : *)
  let mim1 = ref (Array.make (l+1) 0.) in 
  let mi = ref (Array.make  (l+1) nan) in
  (* !mim1 est initialisé à 0 *)
  for i = 1 to n do
    for x = 0 to l do
      !mi.(x) <- if x >= a.(i-1) then
                   max (!mim1.(x-a.(i-1)) +. v.(i)) !mim1.(x)
                 else !mim1.(x)
    done;
    mim1 := !mi;
  done;
  !mim1.(l)
  \end{lstinline}

  Récrire cette fonction en ne se servant que de deux tableaux de
  $\ell{}+1$ \'el\'ements, r\'eduisant la taille m\'emoire
  utilis\'ee \`a $\grandO{2(\ell{}+1)}$.
\end{ques}
\end{partie}
