\begin{ques}
  \begin{lstlisting}
let rec init l = match l with
  | [] -> []
  | x::r -> [x] :: init r
  \end{lstlisting}
\end{ques}

\begin{ques}
  À chaque appel de \inl{init}, on effectue uniquement des opérations
  en temps constant (filtrage, cons), en dehors de l'éventuel appel
  récursif appel à \inl{init}. Or l'exécution de \inl{init $l$}, où
  $l$ est une liste à $n$ éléments, conduit à évaluer $n+1$ appels de
  \inl{init} (appel initial compris). \textbf{La complexité temporelle
    de \inl{init} est donc $O(n)$.}
\end{ques}

\begin{ques}
  \begin{lstlisting}
let rec fusion2 l1 l2 = match l1, l2 with
  | [], l | l, [] -> l
  | x1::r1, x2::r2 -> if x1 <= x2
                      then x1 :: fusion2 r1 l2
                      else x2 :: fusion2 l1 r2
  \end{lstlisting}
\end{ques}

\begin{ques}
  À chaque appel récursif de la fonction \inl{fusion2}, la somme des
  longueurs des arguments de la fonction décroît de $1$, et cette
  somme est évidemment toujours positive ou nulle. Le nombre
  total d'appels à \inl{fusion2} déclenchés par l'exécution de
  \inl{fusion2 $l_1$ $l_2$} est donc au plus $n_1+n_2+1$, où $n_1$ et
  $n_2$ sont les longueurs respectives de $l_1$ et $l_2$.

  En outre, à chaque appel de \inl{fusion2}, on n'effectue que des
  opérations en temps constant (filtrage, comparaison, cons), en
  dehors de l'éventuel appel récursif.

  \textbf{La complexité temporelle de l'exécution de la fonction
    \inl{fusion2} lorsqu'elle est appliquée à deux listes $l_1$ et
    $l_2$ de longueurs $n_1$ et $n_2$ est donc $O(n_1+n_2)$.}
\end{ques}

\begin{ques}
  \begin{lstlisting}
let rec fusion l = match l with
| [] -> []
| [x] -> [x]
| x::y::r -> fusion2 x y :: fusion r
  \end{lstlisting}
\end{ques}

\begin{ques}
  Lorsqu'on applique la fonction fusion à la liste de listes
  $[l_1;\ldots; l_p]$, on est conduit à effectuer au total
  $\ceil{p/2}+1$ appels à \inl{fusion}. À chacun de ces appels, on
  effectue uniquement des opérations de temps constant (filtrage,
  cons), en dehors de l'éventuel appel récursif et de l'éventuel appel
  à \inl{fusion2}.

  D'après la question précédente, le temps total mis pour effectuer
  tous les appels à \inl{fusion2} est $O(n_1+n_2 + n_3 + n_4 + \ldots
  + n_k)$, où $k = 2\floor{p/2}$ et où $n_1$, \ldots, $n_p$ sont les
  longueurs respectives des listes $l_1$, \ldots, $l_p$.

  Le temps total mis pour calculer \inl{fusion}, en dehors des appels
  à \inl{fusion2} est $\floor{p/2} + 1$.

  Si on suppose que chacune des listes $l_1$, \ldots, $l_p$ est
  non-vide, alors $n_1 + \ldots + n_p \geq p$ et
  $O(n_1+\ldots+n_{2\floor{p/2}}) + O(p) = O(n_1+\ldots + n_p)$.

  La complexité en temps de l'exécution de la fonction \inl{fusion}
  lorsqu'elle est appliquée à une liste de listes non vides
  $[l_1;\ldots; l_p]$ dont la somme des longueurs vaut $n$ est donc
  $O(n)$.
\end{ques}

\begin{ques}
  \begin{lstlisting}
(* tri_aux : 'a list list -> 'a list
   (tri_aux $l$) itère la fonction fusion sur la liste $l$ jusqu'à ce
   qu'il ne reste plus qu'un seul élément dans $l$, qu'elle renvoie
   alors. Si $l$ est initialement vide, elle renvoie une liste vide *)
let rec tri_aux l = match l with
  | [] -> []
  | [x] -> x
  | _ -> tri_aux (fusion l)

let trie l = tri_aux (init l) 
  \end{lstlisting}
\end{ques}
NB: Nous avons bien pris garde à ce que la fonction \inl{tri}
fonctionne sur la liste vide (en faisant en sorte que \inl{tri_aux}
renvoie une réponse raisonnable dans le cas de la liste vide).

\begin{ques}
  Soit $l'$ une liste de listes ayant au plus $2^k$ éléments, avec
  $k>0$.  Alors \inl{fusion $l'$} possède au plus $2^{k-1}$ élements.

  Ainsi, si $l'$ est une liste de listes ayant au plus $2^k$ éléments,
  en itérant au plus $k$ fois \inl{fusion} sur $l$, on obtient une
  liste à un seul élément.

  L'appel de \inl{tri} sur une liste $l=[x_1;\ldots;x_n]$ à $n$
  éléments conduit à 
  un appel à \inl{init}, qui renvoie en temps $O(n)$ une liste $l'$
  constituée des listes $[x_1]$, \ldots{}, $[x_n]$.

  \textbf{L'appel de \inl{tri_aux} sur cette liste $l'$ à $n$ éléments conduit
  donc au plus à $k$ applications de \inl{fusion},
  où $k=\ceil{\log_2 n}$.}

  Chacune des autres opérations effectuées à
  chaque appel de \inl{tri_aux} s'effectuant en temps constant, le
  temps total de calcul de \inl{tri_aux} sur la liste $l'$ est donc
  dominé par le temps de calcul de ces $k$ fusions. Or la somme des
  longueurs des listes de $l'$ vaut $n$ et cette valeur est conservée
  par la fonction \inl{fusion} au cours des itérations. De plus les
  listes de $l'$ sont toutes non vides. Le temps de calcul de chaque
  appel à \inl{fusion} est donc $O(n)$ et le temps total d'exécution
  de \inl{tri_aux} est donc $O(kn)=O(n\log_2 n) = O(n\log n)$.

  \textbf{Le temps total de calcul de \inl{trie} appliqué à une
    liste $l$ de taille $n$ est donc $O(n\log n)$ dans le cas le pire.}
\end{ques}

\begin{ques}[Question bonus]
  \begin{lstlisting}
let rec init2 l = match l with
| [] -> []
| x::r ->
   let l' = init2 r in
   match l' with
   | [] -> [[x]]
   | (y::a)::r -> if x <= y then (x::y::a) :: r
		  else [x] :: l'
   | []::_ ->
      failwith
	"init2 devrait renvoyer une liste de listes non vides."
  \end{lstlisting}
\end{ques}

\begin{ques}[Question bonus]
  À chaque appel de \inl{init2}, on n'effectue que des opérations de
  temps constant (filtrage, comparaison, cons) en dehors de l'éventuel
  appel récursif.

  De plus, le nombre total d'appel à \inl{init2} déclenché par
  l'application de \inl{init2} sur une liste de longueur $n$ est
  $n+1$.

  La complexité en temps de la fonction \inl{init2}, lorsqu'elle est
  appliquée à une liste de longueur $n$ est donc, dans le cas le pire,
  $O(n)$.

  La liste $l'$ renvoyée par \inl{init2} ne comporte que des listes non
  vides, dont la somme des longueurs est $n$. Donc $l'$ possède au
  plus $n$ éléments, ce qui donne lieu à $O(\log_2 n)$ appels à
  \inl{fusion} lors de l'exécution de \inl{tri_aux} sur $l'$. Le temps
  d'exécution de \inl{tri_aux} est donc, dans le cas le pire, $O(n\log n)$.
  
  \textbf{La complexité
  en temps de la fonction \inl{trie} lorsqu'elle est appliquée à une
  liste de longueur $n$ est donc, dans le cas le pire, $O(n\log n)$.}

\emph{NB: Cette majoration asymptotique est serrée, le cas $n\log n$ se
produisant lorsque la liste $l$ est triée par ordre décroissant.}
\end{ques}

\begin{ques}[Question bonus]
  lorsqu'elle est appliquée à une
  liste $l$ de longueur $n$ presque triée, c'est-à-dire constituée de
  $n-1$ éléments triés dans laquelle on a inséré, à une position
  arbitraire, un élément $x$ qui n'est pas à sa place, la liste $l'$
  renvoyée par \inl{init2} comporte alors au plus trois listes dont la
  somme des longueurs vaut $n$.

  L'appel à \inl{tri_aux} exécute alors au plus trois fois la fonction
  \inl{fusion}, pour un temps d'exécution total $O(3n) = O(n)$.

  \textbf{Le temps de calcul total de la fonction \inl{trie} sur $l$
    est donc $O(n)$.}
\end{ques}
