
Le but de ce problème est le suivant: Étant donné une liste de points
dans le plan donnés par leurs 
coordonnées cartésiennes dans un repère orthonormé, on cherche une
façon efficace de trouver les deux points les plus proches l'un de
l'autre.

\section{Préliminaires}

\begin{ques}
  Écrire une fonction
  \inl{minimise}, de type \inl{'a list -> ('a -> float) -> 'a},
  prenant en argument une liste $l=[x_{1}; \ldots; x_{n}]$, une
  fonction $f$ et retournant $x_{i}$ tel que $f(x_{i})$ soit minimal.
\end{ques}

\begin{ques}
  Écrire une fonction \inl{filtre}, de type
  \inl{'a list -> ('a -> bool) -> 'a list},
  prenant en argument une liste $l$ et une
  fonction $f$ et retournant la liste des éléments $x$ de $l$ tels que
  l'appel \inl{($f$ $x$)} retourne le booléen \inl{true}. Ces
  éléments doivent être retournés dans la liste résultat dans le même
  ordre que dans la liste donnée en argument.  Cette fonction existe
  déjà en OCaml et s'appelle \inl{List.filter}.
\end{ques}

\begin{ques}
  Écrire une fonction \inl{nth}, de type \inl{'a list -> int -> 'a}
  prenant en argument une liste $\ell$ et un entier $i$ et retournant
  l'élément d'indice $i$ de la liste (on suppose que la liste possède
  au moins $i+1$ éléments). Cette fonction existe déjà en OCaml et
  s'appelle \inl{List.nth}.
\end{ques}

\begin{ques}
  Écrire une fonction \inl{first}, de type
  \inl{'a list -> int -> 'a list},
  prenant en argument une liste $\ell$ et un entier $i$ et retournant
  les $i$ premiers éléments de $\ell$ si $\ell$ est de longueur au
  moins $i$ et tous les éléments de $\ell$ sinon.

  Vous ferez en sorte que le temps d'exécution de
  \inl{(first $i$ $\ell$)} soit en $O(i)$, \textbf{indépendamment
    de la longueur de $\ell$}.
\end{ques}

\section{Méthode naïve}

On identifie les points du plan et leurs coordonnées cartésiennes et
on introduit le type \inl{point} pour les représenter:
\begin{lstlisting}
type point = float * float
\end{lstlisting}

Étant donné deux points $P$ et $Q$, on note $d(P,Q)$ la distance
euclidienne de $P$ à $Q$.

\begin{ques}
  Écrire une fonction \inl{dist}, de type \inl{point -> point -> float}
  retournant la distance euclidienne entre deux points donnés.
\end{ques}

Étant donné un point $P$ et un ensemble de points $E$, on dira qu'un
élément $P'$ de $E$ est un plus proche voisin de $P$ si
\begin{equation}
  \label{eq:1}
  d(P,P') = \min_{Q\in E} d(P,Q)
\end{equation}
$P$ peut avoir plusieurs plus proches voisins si plusieurs points de
$E$ ont une distance à $P$ égale à $\min_{Q\in E} d(P,Q)$.

\begin{ques}
  Écrire une fonction
  \inl{plus_proche}, de type \inl{point -> point list -> point},
  prenant en argument un point $p$ et une liste de points $l$
  retournant un plus proche voisin de $p$ parmi les éléments de $l$.
  Indication: utiliser \lstinline+minimise+ peut être une bonne idée.
\end{ques}

\begin{ques}
  Quelle est la complexité de votre fonction \lstinline+plus_proche+?
\end{ques}

\begin{ques}
  Écrire une fonction
  \inl{liste_plus_proches}, de type
  \inl{point list -> (point*point) list}
  prenant en argument une liste de points
  $l = [p_{0}; \ldots; p_{n-1}]$ et retournant une liste
  $[(p_{0},p'_{0});\ldots;(p_{n-2}, p'_{n-2})]$ telle que pour tout
  $i\in\ii{0,n-1}$, $p'_{i}$ soit un
  plus proche voisin de $p_{i}$ parmi $[p_{i+1}; \ldots; p_{n-1}]$.
  Si la liste $l$ comporte un élément ou moins,
  \lstinline+liste_plus_proches+ retourne la liste vide.
\end{ques}
\begin{ques}
  Quelle est la complexité de votre fonction \lstinline+liste_plus_proches+?
\end{ques}

Étant donné un ensemble $E$ de points comportant au moins $2$ points,
on appelle plus proches voisins tout couple $(M,M')\in E^{2}$
vérifiant
\begin{equation*}
  d(M,M') = \min_{(P,Q)\in E^{2}, P\neq Q}d(P,Q)
\end{equation*}

\begin{ques}
  En utilisant la question précédente, écrire une fonction
  \lstinline+plus_proches_voisins_naif+ de type
  \lstinline+point list -> point*point+
  prenant en argument une liste $l$ ayant au moins
  deux éléments et retournant un couple de plus proches voisins de la
  liste $l$. On supposera que la liste donnée ne comporte pas deux
  fois le même point.
\end{ques}

\begin{ques}
  Quelle est la complexité de votre fonction
  \lstinline+plus_proches_voisins_naif+?  
\end{ques}

\section{Diviser pour régner}

\subsection{Modélisation du problème}
Dans la suite, on représentera le nuage de points qui nous intéresse
par un enregistrement possédant deux champs:

\begin{lstlisting}
type nuage = {
  par_x_puis_y : point list;
  par_y : point list;
}
\end{lstlisting}

Dans tout ce qui suit, on veut que tout objet $n$ de type
\lstinline+nuage+ qu'on manipule vérifie les trois propriétés
suivantes:
\begin{enumerate}
\item Les deux champs $n$\lstinline+.par_x_puis_y+ et $n$\lstinline+.par_y+
  représentent un même ensemble $E$ de points: chaque point de $E$
  apparaît une et une seule fois dans chacune de ces deux listes;
\item La liste $n$\lstinline+.par_x_puis_y+ est triée
  par ordre lexicographique;
\item La liste $n$\lstinline+.par_y+ est triée par
  ordonnées croissantes.
\end{enumerate}

On définit la \emph{taille} d'un nuage de points comme le nombre de
points qu'il contient.

On va écrire deux fonctions permettant de créer un objet de type \lstinline+nuage+:
\begin{ques}
  Écrire une fonction \inl{cree_nuage}, de type \inl{point list -> nuage},
  créant, à partir d'une liste de points tous distincts,
  un nuage de points vérifiant les trois propriétés recherchées.

  Vous ferez en sorte que la complexité temporelle de cette fonction
  soit un $O(N\log N)$ où $N$ est la taille du nuage et vous
  justifierez la complexité de votre fonction.

  Vous pouvez notamment utiliser la fonction \lstinline+sort+ du
  module \lstinline+List+ (cette fonction utilise une variante du tri
  fusion) et la fonction \inl{compare} (documentation dans la partie
  \textit{The core library} du manuel d'OCaml, module
  \inl{Pervasives})
\end{ques}

\begin{ques}
  Écrire une fonction
  \inl{filtre_nuage}, de type \inl{nuage -> (point -> bool) -> nuage},
  prenant en argument un nuage $n$, une fonction $f$ et retournant le
  nuage formé des points $M$ de $n$ tels que l'appel
  \inl{($f$ $M$)} retourne \inl{true}. Le nuage ainsi créé doit bien
  entendu vérifier les trois propriétés énoncées plus haut.

  Si $f$ s'exécute en $O(1)$, \inl{filtre_nuage} doit s'exécuter
  en temps linéaire en la taille du nuage.
\end{ques}

Par la suite, lorsqu'on voudra créer des objets de type
\lstinline+nuage+, on s'astreindra à utiliser ces deux fonctions, de
façon à être certain que les nuages créés vérifient les trois
propriétés énoncées précédemment.

\subsection{Découpage en sous-problèmes}

Pour résoudre ce problème, l'idée est de couper en deux le nuage de
points $n$ sur lequel on veut trouver les plus proches voisins, de
chercher récursivement les plus proches voisins des deux demi-nuages
$n_{g}$ et $n_{d}$ obtenus et enfin d'utiliser ces plus proches
voisins des demi-nuages pour trouver les plus proches voisins de $n$.

\begin{ques}
  Écrire une fonction
  \inl{coupe}, de type \inl{nuage -> nuage * point * nuage},
  prenant en argument un nuage $n$ de taille $k$, avec $k\geq 1$ et
  retournant deux
  nuages $n_{g}$ et $n_{d}$ respectivement de tailles $\floor{k/2}$ et
  $\ceil{k/2}$ et un point $P$ tel que
  \begin{enumerate}
  \item $n_{g}$ contient tous les éléments de $n$ strictement inférieurs
    à $P$ pour l'ordre lexicographique;
  \item $n_{d}$ contient tous les éléments de $n$ supérieurs ou égaux
    à $P$ pour l'ordre lexicographique (y compris $P$).
  \end{enumerate}
  La fonction \lstinline+coupe+ devra s'exécuter en temps linéaire en
  $k$.
\end{ques}


\subsection{Combinaison des solutions des sous-problèmes}

Dans cette partie, on considère la situation suivante: on dispose d'un
ensemble $E$ de cardinal $k$. On note $P$ un point de $E$, $E_{g}$ l'ensemble des
points $P'$ tels que $P'<P$ pour l'ordre lexicographique, $E_{d}$
l'ensemble des points $P'$ tels que $P'\geq P$ pour l'ordre
lexicographique.

On note $(x_{P},y_{P})$ les coordonnées de $P$.
On suppose que $E_{g}$ et $E_{d}$ comportent au moins deux points, de
telle sorte qu'ils possèdent chacun au moins un couple de plus proches
voisins.

On note $\delta_{g}$ (resp $\delta_{d}$) la distance minimale entre
deux plus proches voisins dans $E_{g}$ (resp. $E_{d}$) et on pose
$\delta = \min(\delta_{g},\delta_{d})$.

On note $V$ la bande du plan constituée des points de coordonnées
$(x,y)$ vérifiant $\abs{x-x_{P}}\leq \delta$ et $V_{B}$ l'ensemble
des points de $E$ appartenant à $V$.

On note $\delta'$ la distance entre deux plus proches voisins de
$E$.

\begin{ques}
  Justifiez brièvement qu'on a $\delta'\leq \delta$.
\end{ques}

% \begin{ques}
%   Montrez que si $\delta' = \delta$, alors il existe un couple de plus
%   proches voisins $(M,M')$ de $n$ appartenant tous deux à $n_{g}$ ou
%   tous deux à $n_{d}$.
% \end{ques}

Jusqu'à la fin de cette partie, on suppose qu'on a $\delta'<\delta$.
\begin{ques}
  Faire un dessin.
\end{ques}

\begin{ques}
  Montrez que tout couple de plus proches voisins $(M,M')$ de
  $E$ appartient à $V_{B}$.
\end{ques}

On note $k'= \card V_{B}$ et $(x_{0},y_{0})$, \ldots{}, $(x_{k'-1},y_{k'-1})$ les
éléments de $V_{B}$ triés par ordonnées croissantes et
on considère un couple $(M,M')$ de plus proches voisins de $E$. $M$ et
$M'$
s'écrivent respectivement $(x_{i},y_{i})$ et $(x_{j},y_{j})$. Quitte à
échanger $M$ et $M'$, on a $i<j$.

\begin{ques}
  Montrer qu'au plus quatre points de $E_{g}\cap V_{B}$ et quatre
  points de $E_{d}\cap V_{B}$ peuvent avoir des
  ordonnées appartenant à $[y_{i},y_{j}]$. \textbf{On fera de nouveau un
    dessin}. On pourra utiliser le principe des
  tiroirs\footnote{Ce principe est aussi appelé principe
    de Dirichlet ou \textit{pigeonhole principle}.} qui s'énonce comme
  suit: si $n+1$ chaussettes sont rangées dans $n$ tiroirs (avec $n\in
  \N^{*}$), alors au moins un tiroir contient au moins deux
  chaussettes.
\end{ques}

\begin{ques}
  En déduire $j \leq i+7$.
\end{ques}

\begin{ques}
  Écrire une fonction
  \inl{liste_plus_proches_7}, de type \inl{point list -> (point*point) list},
  prenant en argument une liste de points
  $l = [p_{0}; \ldots; p_{n-1}]$ et retournant une liste
  $[(p_{0},p'_{0});\ldots;(p_{n-2}, p'_{n-2})]$ telle que pour tout
  $i\in\ii{0,n-1}$, $p'_{i}$ soit un
  plus proche voisin de $p$ parmi $[p_{i+1}; \ldots; p_{\min(n-1,i+7)}]$.
  %
  On retournera la liste vide si $n\leq 1$.

  Vous ferez en sorte que cette fonction soit de complexité linéaire
  en $n$ et vous justifierez sa complexité.
\end{ques}

\begin{ques}
  Écrire une fonction
  \inl{plus_proches_7}, de type \inl{point list -> (point * point)},
  prenant en argument une liste de points $l = [p_{0}; \ldots;
  p_{n-1}]$ et retournant un couple $(p_{i}, p_{j})$ minimisant
  $d(p_{i},p_{j})$ sous la contrainte $i< j \leq i+7$.

  Vous ferez en sorte que cette fonction soit de complexité linéaire
  en $n$ et vous justifierez sa complexité.
\end{ques}

\subsection{Résolution récursive}

\begin{ques}
  Donnez, en français, un algorithme récursif de recherche des plus
  proches voisins. Vous détaillerez en particulier les conditions
  d'arrêt et vous justifierez rigoureusement la correction et la
  terminaison de votre algorithme.
\end{ques}

\begin{ques}
  Écrire une fonction
  \inl{pp_voisins_rec}, de type \inl{nuage -> point * point},
  prenant en argument un nuage de points comportant au moins deux
  points et retournant les plus proches voisins.
\end{ques}

On note $T(n)$ le temps de calcul de \lstinline+pp_voisins_rec+ dans
le cas le pire pour un nuage de points de taille $n$ et on
suppose par la suite que $T$ est une fonction croissante.

%TODO:
% \begin{ques}
%   Justifier brièvement que $T$ est croissante.
% \end{ques}

\begin{ques}
  Montrer que pour tout $n\in \N$, on a
  \begin{equation*}
    T(n) \leq 2 T(\ceil{n/2}) + \mathop{O}\limits_{n\to+\infty}(n)
  \end{equation*}
\end{ques}

Pour $k\in \N$, on pose $u_{k} = T(2^{k})$.
\begin{ques}
  Montrer qu'on a nécessairement
  \begin{equation*}
   u_{k+1}  - 2u_{k} \leq \mathop{O}\limits_{k\to+\infty}(2^{k}) 
  \end{equation*}
\end{ques}

\begin{ques}
  On pose $v_k = u_k / 2^k$, pour $k\in \N$. Montrez qu'on a
  $v_k \leq \mathop{O}\limits_{k\to+\infty}(k)$ et en déduire $u_{k}
  = \mathop{O}\limits_{k\to+\infty}(2^{k}\times k)$.
\end{ques}

\begin{ques}
  En déduire $T(n)=\mathop{O}\limits_{k\to+\infty}(n\log n)$.
\end{ques}

\begin{ques}
  Écrire une fonction
  \inl{pp_voisins}, de type \inl{point list -> point * point},
  prenant en argument une liste de points tous distincts comportant au moins deux
  points et retournant les plus proches voisins. Pour cela, vous
  construirez un nuage de points à partir de la liste et utiliserez la
  fonction \lstinline+pp_voisins_rec+.
\end{ques}

\begin{ques}
  Quelle est, en fonction de la longueur $n$ de la liste de points, la
  complexité de l'exécution de cette fonction? Justifiez.
\end{ques}

\begin{ques}
  Écrire une fonctions \lstinline+pp_voisins_ext+ généralisant
  \lstinline+pp_voisins+: elle doit avoir le même type que
  \lstinline+pp_voisins+ mais la liste de points qu'elle prend en
  argument peut éventuellement comporter des doublons. Lorsqu'un point
  est présent plusieurs fois, il est pris en compte une seule fois
  pour le calcul des plus proches voisins. Vous ferez en sorte que la
  complexité asymptotique reste la même que celle de
  \lstinline+pp_voisins+ et justifierez que c'est bien le cas.
\end{ques}
