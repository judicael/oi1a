(*
\theoremstyle{exostyle}
\newtheorem{rep}{Question}
\section{Préliminaires}

\begin{rep}
 *)
let rec minimise l f = match l with
  | [] -> failwith "Liste vide"
  | [x] -> x
  | x::y::r -> let z = minimise l f in
	       if f x < f z
	       then x
	       else z
;;
(*
\end{rep}

\begin{rep}
 *)
let rec filtre l p = match l with
  | [] -> []
  | x::r -> if p x then x :: filtre l p else filtre l p
;;
(*
\end{rep}

\begin{rep}
*)
let rec nth l n = match l with
  | [] -> failwith "Liste vide"
  | x::r -> if n = 0 then x else nth r (n-1)
;;
(*
\end{rep}

\begin{rep}
 *)
let rec first l n = match l, n with
  | _, 0 | [], _ -> []
  | x::r, _ -> x :: first r (n-1)
;;
(*
Une récurrence immédiate permet de montrer que pour tout entier $i$ et
toute liste $l$, l'appel [(first l i)] conduit au plus à $i$ appels
récursifs (en plus de l'appel initial).

Or lors de chacun des appels, on effectue au plus une opération de
filtrage, une soustraction et un cons, qui sont des opérations de
temps constant.


\begin{framed}
  Le temps d'exécution de [(first l i)] est donc un
  $O(i)$, indépendamment de la longueur de [l].  
\end{framed}
\end{rep}

\section{Méthode naïve}

 *)
type point = float * float;;
(*

\begin{rep}
On peut représenter les vecteurs du plan par des couples de points:
 *)
type vecteur = float * float;;
(*
On définit alors naturellement la fonction calculant la norme
euclidienne:

[norme : vecteur -> float]
 *)
let norme (x, y) = sqrt (x *. x +. y *. y);;
(*
[vect : point -> point -> vecteur]
[vect P Q] retourne le vecteur $\overrightarrow{PQ}$
*)
let vect (x, y) (x', y') = (y' -. y, x' -. x);;
(*
On peut alors définir la fonction [dist : point -> point -> float]
demandée :
 *)
let dist p q = norme (vect p q);;
(*
\end{rep}

\begin{rep}
 *)
let plus_proche p l = minimise l (fun q -> dist p q);;
(*
\end{rep}

\begin{rep}
Notons $f$ la fonction [fun q -> dist p q] et remarquons que $f$
s'exécute sur tout point [p] en temps constant.
L'exécution de [plus_proche p l] conduit à exécuter [minimise l f],
ce qui conduit au total à $O(|l|)$ appels de [minimise], où $|l|$
désigne la taille de $l$. Or, à chaque
appel, la fonction [minimise] n'effectue que des opérations en temps
constant, en dehors de l'éventuel appel récursif. Son
temps d'exécution est donc dominé par son nombre d'appels.

\begin{framed}
  L'exécution de [plus_proche p l] demande donc un temps $O(n)$, où $n$
  est la longueur de [l].
\end{framed}
\end{rep}

\begin{rep}
 *)
let rec liste_plus_proches l = match l with
  | [] -> []
  | [p] -> []
  | p::r -> 
     (p, plus_proche p r) :: liste_plus_proches r
;;
(*
\end{rep}
\begin{rep}
Considérons l'exécution de [liste_plus_proche l] dans le cas le pire
pour une liste [l] de longueur [n].
Pour $n\geq 1$, cette exécution conduit à $n$ appels de
[liste_plus_proche] (appel initial inclus). À chaque appel, on
exécute des opérations de temps constant (filtrage, cons), plus un
éventuel appel à [plus_proche] sur une sous-liste de $l$, ce qui
demande donc un temps $O(n)$, plus un éventuel appel récursif.

\begin{framed}
  Le temps d'exécution de [liste_plus_proche l] est donc $O(n^2)$.  
\end{framed}
\end{rep}

\begin{rep}
 *)
let plus_proches_voisins_naif l =
  let l1 = liste_plus_proches l in
  minimise l1 (fun (p, q) -> dist p q)
;;
(*
\end{rep}

\begin{rep}
Pour une liste $l$ de longueur $n$, l'appel à [liste_plus_proches l]
s'exécute en temps $O(n^2)$ et construit une liste [l1] de longueur
$n-1$. Le calcule de [minimise l1 (fun (p, q) -> dist p q)] demande
alors un temps $O(n-1)$, le temps de calcul d'un appel à [dist] ne
dépendant pas de ses arguments.

\begin{framed}
  Le temps total de calcul de [liste_plus_proches l] est donc
  $O(n^2)+O(n-1) = O(n^2)$.
\end{framed}

\emph{NB : Il s'agit bien d'une somme et non d'un produit.}
\end{rep}

\section{Diviser pour régner}

\subsection{Modélisation du problème}

 *)
type nuage = {
  par_x_puis_y : point list;
  par_y : point list;
};;
(*
\begin{rep}
Nous définissons une fonction [lexico : point -> point -> int]
telle que [lexico p q] retourne un entier strictement négatif si [p]
est strictement inférieur à [q] dans l'ordre lexicographique, 0 s'ils
sont égaux et un entier strictement positif sinon.
 *)
let lexico (x1, x2) (y1, y2) =
  match compare x1 x2 with
  | 0 -> compare y1 y2
  | c -> c

(* Nous pouvons maintenant écrire la fonction demandée:
 *)

let cree_nuage l =
  let parx = List.sort lexico l in
  let pary =
    List.sort
      (fun (x1, x2) (y1, y2) -> compare x2 y2)
      l
  in
  { par_x_puis_y = parx; par_y = pary }
;;
(*
Soit [l] une liste de longueur $N$.
La fonction [List.sort] utilise une variante du tri fusion et
s'exécute donc sur [l] en temps $O(N\log N)$ lorsque les fonctions
de comparaison utilisées s'exécutent en temps constant.

\begin{framed}
  Le calcul de [parx] et de [pary] dans l'exécution de
  [cree_nuage l] s'effectue donc en temps $O(N\log N)$.
\end{framed}
\end{rep}

\begin{rep}
Nous proposons le code suivant pour [filtre_nuage]:
 *)
let filtre_nuage n f =
  let parx = filtre n.par_x_puis_y f in
  let pary = filtre n.par_y f in
  { par_x_puis_y = parx; par_y = pary }

(*
Il reste à montrer d'une part sa correction (le fait qu'il retourne
un nuage vérifiant les propriétés demandées) et d'autre part que sa
complexité est linéaire en la taille du nuage si la fonction [f]
s'exécute en temps constant.

Notons $n$ un nuage de taille $N$ et $f$ une fonction s'exécutant en
temps constant.
Alors [n.par_x_puis_y] est une liste de longueur $N$,
donc le calcul de [parx] dans l'exécution de [filtre_nuage n f]
s'effectue en temps $O(N)$ comme on l'a vu précédemment.
De même, celui de [pary] s'effectue en temps $O(N)$.

\begin{framed}
  Donc, lorsque [f] s'exécute en temps constant
  et que [n] est un nuage de taille $N$,
  le calcul de [filtre_nuage n f] s'effectue en temps $O(N)$.
\end{framed}

De plus, puisque $n$ est un nuage, [n.par_x_puis_y] et [n.par_y]
contiennent le même ensemble de points.
Donc les listes [parx] et [pary] calculées
par l'exécution de [filtre_nuage n f]
contiennent également les mêmes ensembles de points.

Il est clair que si une liste [l] est triée pour un certain ordre,
alors pour toute fonction [f], [filtre l f] est également triée
pour ce même ordre.
Donc la liste [parx] et [pary] est triée par ordre lexicographique
et la liste [pary] est triée par ordonnées croissantes.

\begin{framed}
  Le nuage construit par [filtre_nuage n f] respecte donc les
  trois conditions requises.
\end{framed}
\end{rep}

\subsection{Découpage en sous-problèmes}

\begin{rep}
Considérons un nuage $n$ de taille $k$,
dont la liste des points trié par ordre lexicographique est
$P_0$, \ldots, $P_{k-1}$. Alors, en posant $i=\floor{k/2}$,
la liste $P_{0}$, \ldots, $P_{i-1}$ est de longueur $i$.
Cette liste est exactement la liste des points
précédant strictement $P_i$ dans l'ordre lexicographique
(les points étant tous distincts). D'où le code suivant:
 *)

let coupe n =
  let k = List.length n.par_x_puis_y in
  let i = k / 2 in
  let p = nth n.par_x_puis_y i in
  let ng =
    filtre_nuage n (fun q -> lexico q p < 0) in
  let nd = 
    filtre_nuage n (fun q -> lexico q p >= 0) in
  ng, p, nd
;;
(*
L'exécution de [coupe n], où $n$ est un nuage de taille $N$,
demande le calcul de la longueur de [n.par_x_puis_y],
ce qui se fait en temps $O(N)$,
le calcul d'une division euclidienne qui se fait en temps constant,
deux applications de [filtre_nuage].
Les fonctions données en argument à [filtre_nuage]
s'exécutent en temps constant
et les listes données en arguments
sont de tailles $\floor{N/2}=O(N)$ et
$N-\floor{N/2}=\ceil{N/2}=O(k)=O(N)$.
\end{rep}


\subsection{Combinaison des solutions des sous-problèmes}

\begin{rep}
  On a $E_g\subset E$, donc $E_g^2 \subset E^2$, donc
  $\Set{d(x,y)|(x,y)\in E_g^2} \subset
   \Set{d(x,y)|(x,y)\in E^2}$.
  Le minimum $\delta_g$ du premier ensemble est donc
  supérieur ou égal au minimum $\delta'$ du second.
  De même $\delta_d\geq \delta'$.

  Donc \fbox{$\delta'\leq \min (\delta_g,\delta_d) = \delta$}.
\end{rep}

\begin{rep}
%  TODO
DESSIN
\end{rep}

\begin{rep}
  Soit $(M, M')$ un couple de plus proches voisins.
  Puisque $\delta'<\delta$,
  on ne peut avoir $(M,M') \in E_g$ ni $(M,M')\in E_d$.
  Donc l'un des points appartient à $E_g$, l'autre à $E_d$.
  Sans perte de généralité, on peut supposer $M\in E_g$ et
  $M'\in E_d$.
  Notons $(x,y)$ les coordonnées de $M$
  et $(x',y')$ celles de $M'$.

  On a $x\leq x_P\leq x'$.
  En particulier, $0\leq x'-x_P \leq x'-x$
  et $0\leq x_P - x \leq x' -x$ et $0\leq x'-x$.

  Or $d(M, M') = \sqrt{(x'-x)^2+(y'-y)^2} \geq \abs{x'-x}$, d'où $0\leq x'-x \leq \delta'$.

  Donc $0\leq x' -x_P\leq \delta'$ et $0\leq x_P - x \leq \delta'$.

  Donc $M\in V_B$ et $M'\in V_B$.
\end{rep}
*)
(*l
\begin{rep}
  DESSIN
  %TODO
 
  Supposons par l'absurde qu'au moins cinq points de $E_g\cap V_B$
  ont des ordonnées appartenant à $[y_i,y_j]$.
  On dispose alors de cinq points $M_{1}$, \ldots{}, $M_{5}$
  appartenant au rectangle $[x_P-\delta', x_P+\delta']\times  [y_i,y_j]$.

  Ce rectangle est la réunion
  des quatre rectangles $R_1$, $R_2$, $R_3$ et $R_4$ définis par
  \begin{align*}
    R_{1} &= [x_{P}-\delta', x_{P}-\delta'/2]\times [y_{i}, (y_{i}+y_{j})/2]\\
    R_{2} &= [x_{P}-\delta'/2, x_{P}]\times [y_{i}, (y_{i}+y_{j})/2]\\
    R_{3} &= [x_{P}-\delta', x_{P}-\delta'/2]\times [(y_{i}+y_{j})/2, y_{j}]\\
    R_{4} &= [x_{P}-\delta'/2, x_{P}]\times [(y_{i}+y_{j})/2, y_{j}]
  \end{align*}
  Chacun des points $M_{1}$, \ldots{}, $M_{5}$ appartient au moins à
  l'un de ces rectangles, donc d'après le principe de Dirichlet, au
  moins deux points distincts $M_{a}$ et $M_{b}$ appartiennent au même
  rectangle $R_{i}$. 
  Or le diamètre de chacun des rectangles vaut
  \begin{equation*}
  d = \sqrt{\delta'^{2}/4+(y_{j}-y_{i})^{2}/4} \leq
  \delta'/\sqrt{2} <\delta'
  \end{equation*}
  Donc $d(M_{a},M_{b})\leq d < \delta'$.

  C'est absurde, donc au plus quatre points de $E_{g}\cap V_{B}$ ont
  des ordonnées appartenant à $[y_{i},y_{j}]$.

  De la même façon, au plus quatre points de $E_{d}\cap V_{B}$ ont des
  ordonnées appartenant à $[y_{i},y_{j}]$.
\end{rep}
*)
(*
\begin{rep}
  D'après ce qui précède, au plus $8$ point de $V_{B}$
  ont des ordonnées comprises entre $y_{i}$ et $y_{j}$.

  Par l'absurde, supposons $j > i+7$, c'est-à-dire $j\geq i+8$.
  Alors comme
  la liste $(x_{0}, y_{0})$, \ldots{}, $(x_{k'-1},y_{k'-1})$
  est triée par ordonnées croissantes,
  les points $M_{i}, \ldots, M_{j}$ constituent au moins $9$ points
  de $V_{B}$ d'ordonnées appartenant à $[y_{i}, y_{j}]$.

  C'est absurde, donc on a $j\leq i+7$.
\end{rep}

\begin{rep}
Cette fonction est très similaire à [liste_plus_proches] écrite plus
haut.
La différence est qu'au lieu d'appeler [plus_proche] sur la queue
de la liste,
on l'appelle sur la sous-liste constituée des $7$ premiers éléments
de cette queue.
*)
let rec liste_plus_proches_7 l = match l with
  | [] | [_] -> []
  | p::r -> let pp7 = plus_proche p (first r 7) in
            (p, pp7) :: liste_plus_proches_7 r
;;
(*
L'exécution de [liste_plus_proches_7 l] pour une liste de longueur [n]
(avec $n\geq 1$) conduit au total à $n$ appels à
[liste_plus_proches_7] (appel initial inclus), sur des listes toutes
de longueur au plus $n$.

À chacun de ces appels, on effectue, en dehors de l'éventuel appel
récursif, uniquement des opérations en temps constant. En effet, le calcul de
[first r 7] sur une liste [r] quelconque se fait en temps constant, le
temps de calcul de [first] étant linéaire en son deuxième argument. De
plus ce calcul retourne une liste de longueur constante, donc l'appel
de [plus_proche] s'effectue également en temps constant. Enfin,
filtrage et cons s'exécutent également en temps constant.

Le temps de calcul de [liste_plus_proches_7 l] est donc dominé par le
nombre d'appel, lui-même linéaire en $|l|$.

\begin{framed}
  Le temps de calcul de [liste_plus_proches_7 l] sur une liste de
  longueur [n] est donc $O(n)$.
\end{framed}
\end{rep}

\begin{rep}
 *)
let rec plus_proches_7 l =
  let lpp7 = liste_plus_proches_7 l in 
  minimise lpp7 (fun (p, q) -> dist p q) ;;
(*
On a vu que pour une liste [l] de longueur $n$,
[liste_plus_proche_7 l] prenait un temps $O(n)$.
De plus, elle retourne une liste $l'$ de longueur $n-1$.
Or le temps de calcul de [minimise f r] est en $O(N)$
pour toute liste [r] de longueur [N]
et toute fonction [f] travaillant en temps constant.

\begin{framed}
  Donc, pour toute liste [l] de longueur $n$: le temps d'exécution de
  [plus_proche_7 l] est $O(n)$.
\end{framed}
\end{rep}

\subsection{Résolution récursive}

\begin{rep}
  On peut rechercher les plus proches voisins dans un nuage $n$ de taille
  $k\geq 2$ comme suit:
  \begin{itemize}
  \item Si $k< 4$, on utilise l'algorithme naïf.
  \item Sinon, on coupe ce nuage en deux, comme programmé
    précedemment: on obtient des sous-nuages $n_{g}$ et $n_{d}$
    respectivement de tailles $\floor{k/2}$ et $\ceil{k/2}$ ainsi
    qu'un point $P$ tel que $n_{g}$ contient tous les points de $n$
    strictement inférieurs à $P$ dans l'ordre lexicographique et
    $n_{d}$ tous les autres points.
  \item Chacun de ces sous-nuages comporte au moins deux points. On
    peut alors appliquer récursivement l'algorithme sur ces deux
    sous-nuages. On obtient alors des couples de plus proches voisins
    $(M_{g},M_{g}')$ et $(M_{d},M_{d'})$ respectivement pour $n_{g}$
    et $n_{d}$.
  \item On calcule $\delta = \min(d(M_{g},M_{g'}),d(M_{d},M_{d'}))$.
  \item On calcule la liste $L$ des points de $n$ appartenant à
    $V_{B}$ (i.e. d'abscisses dans $[x_{P}-\delta, x_{P}+\delta]$),
    triée par ordre croissant d'ordonnées.
  \item Si cette liste possède au moins deux éléments, on calcule le
    couple $(M,M')$ des points les plus proches sous la contrainte que
    leur différence d'indice dans $L$ est au plus $7$
    et on retourne, parmi les trois couples $(M,M')$,
    $(M_{g},M_{g}')$ et $(M_{d},M_{d}')$ celui dont les deux points sont
    les plus proches.
  \item Sinon, on retourne parmi les deux couples $(M_{g},M_{g}')$ et
    $(M_{d},M_{d}')$ celui dont les deux points sont les plus proches.
  \end{itemize}

  Chaque appel récursif se fait sur des nuages de tailles strictement
  plus petites car $\floor{k/2} < k$ pour tout $k\geq 2$. Donc l'algorithme
  termine.

  Par ailleurs, il est clair que l'algorithme est correct pour $k=2$
  et pour $k=3$. En outre, si l'on suppose par l'absurde qu'il existe
  des nuages de taille supérieure ou égale à $2$ pour lesquels il
  donne une réponse incorrecte, considérons un tel nuage $n$ de taille
  $k$ minimale. 

  Alors, $k\geq 4$ puisque l'algorithme est correct pour $k=2$ et
  $k=3$. Donc l'algorithme est appellé récursivement sur les
  sous-nuages $n_{g}$ et $n_{d}$, de taille strictement inférieure à
  $k$ et pour lesquels on obtient donc le bon résultat.

  Alors, en notant $\delta'$ la distance minimale entre deux points de
  $n$:
  \begin{itemize}
  \item ou bien $\delta'<\delta$ et dans ce cas il existe deux points
    $M$ et $M'$ dans $L$, de distance $\delta$, de différence
    d'indices au plus $7$ et ce couple est bien le couple retourné par
    l'algorithme.
  \item ou bien $\delta' = \delta$ et dans ce cas, les couples de
    points de $L$ sont tous de distances supérieures ou égales à
    $\delta$ entre eux et l'algorithme retournera bien un couple de
    points à distance $\delta$.
  \end{itemize}

  Donc dans tous les cas, le résultat correct sera retourné, ce qui
  est absurde.

  Donc l'algorithme est correct.
\end{rep}

\begin{rep}
 *)
let taille_nuage n = List.length n.par_y
  
let rec pp_voisins_rec n =
  let k = taille_nuage n in
  if k <= 3
  then plus_proches_voisins_naif n.par_y
  else
    let ng, (xp, yp), nd = coupe n in
    let mg, mg' = pp_voisins_rec ng in
    let md, md' = pp_voisins_rec nd in
    let dg = dist mg mg' in
    let dd = dist md md' in
    let delta = min dg dd in
    let f (x,_) = 
      x >= xp -. delta && x <= xp +. delta in
    let vb = filtre n.par_y f in
    (* points potentiellement les plus proches: *)
    let c = match vb with
      | [] | [_] -> [(mg, mg'); (md, md')]
      | _ -> let m, m' = plus_proches_7 vb in
	     [(m, m'); (mg, mg'); (md, md')]
    in
    minimise c (fun (p, q) -> dist p q)
;;
(*
\end{rep}

\begin{rep}
Pour $k$ au voisinage de $+\infty$, le temps de calcul de 
pour un nuage de taille $n$ de taille $k'\leq k$ est ou bien majoré par une constante
si ce nuage est de taille au plus $3$, ou bien le temps cumulé des exécutions de
\begin{itemize}
\item [coupe n], qui est en $O(k')$.
\item [pp_voisins_rec ng], en $T(\floor{k'/2})$
\item [pp_voisins_rec nd], en $T(\ceil{k'/2})$
\item [dist mg mg'], [dist md md'], [min dg dd] en $O(1)$,
\item [filtre f n.par_y] en temps $O(k')$,
\item  éventuellement [plus_proches_7 vb], en temps $O(k'')$ avec
  $k''\leq k$.
\item [minimise c (fun p q -> dist p q)] en temps constant, $c$ étant
  de longueur au plus $3$.
\end{itemize}
Pour tout $x\leq y$, on a $T(x)\leq T(y)$ d'après la
définition de $T$, donc $T(\floor{k'/2})\leq T(\ceil{k'/2})\leq T(\ceil{k/2})$.

On en déduit:
\begin{equation*}
  \boxed{T(k)\leq_{k\to +\infty} 2 T(\ceil{k/2})+ O(k)}
\end{equation*}
\end{rep}

\begin{rep}
  On a alors
  \begin{align*}
    u_{k+1} - 2 u_{k} &= T(2^{k+1}) - 2 T(2^{k})\\
    &\leq 2 T(2^{k}) + O(2^{k+1}) + 2 T(2^{k})\\
    &\leq O(2^{k})
  \end{align*}
\end{rep}

\begin{rep}
  On en déduit
  \begin{equation*}
    u_{k+1} 2^{-k-1} - u_{k}/2^{-k} \leq O(1)
  \end{equation*}
  Autrement dit, la suite $\p{u_{k+1}2^{-k-1}-u_{k}2^{-k}}$ est
  majorée par une constante $M$
  D'où, par somme télescopique:
  \begin{equation*}
    u_{k}/2^{k} - u_{0}/2^{0} \leq M\times k = O(k)
  \end{equation*}
  On en déduit $0\leq u_{k} \leq O(k \cdot 2^{k})$, donc
  \begin{equation*}
    \boxed{u_{k} = O(2^{k} \cdot k)}
  \end{equation*}
\end{rep}

\begin{rep}
  On en déduit successivement:
  \begin{align*}
    T(n)&\leq T(2^{\ceil{\log_{2} n}})\\
    &\leq O(2^{\ceil{\log_{2} n}}\cdot \ceil{\log_{2} n})\\
    &leq O(n \cdot \log_{2} n)
  \end{align*}
  Et comme $0\leq T(n)$, on en déduit
  \begin{equation*}
    \boxed{T(n)=O(n\log n)}
  \end{equation*}
\end{rep}

\begin{rep}
*)
let pp_voisins l =
  let n = cree_nuage l in
  pp_voisins_rec n
;;
(*
\end{rep}

\begin{rep}
  On sait que la complexité de l'appel [cree_nuage l] est $O(n \log
  n)$ pour une liste [l] de taille $n$. Le nuage alors créé est de
  taille [n] également et la complexité de [pp_voisins_rec] appliqué à
  ce nuage est $O(n\log n)$.
  
  \begin{framed}
    On en déduit que la complexité de l'exécution de [pp_voisins
      l] est $O(n\log n)$ où $n$ est la taille de [l].
  \end{framed}
\end{rep}

\begin{rep}
Il suffit de trier la liste pour en retirer les doublons avant d'appliquer [pp_voisins].

[retire_doublons : 'a list -> 'a list] retire les doublons d'une liste déjà triée.
*)
let rec retire_doublons l = match l with
  | [] | [_] -> []
  | x::y::r -> 
     if x = y then retire_doublons (y::r)
     else x :: retire_doublons(y::r)
;;

let pp_voisins_ext l =
  let l' = retire_doublons (List.sort lexico l) in
  pp_voisins l'
             
(*
Pour une liste [l] de longueur [n], le tri de la liste [l] se fait en
$O(n\log n)$. La fonction [retire_doublons] travaille en temps
linéaire en la taille de liste qui lui est passée en argument, donc
ici en $O(n)$. Elle retourne une liste [l'] de taille inférieure ou égale à
$n$, donc l'exécution de [pp_voisins l'] se fait en $O(n \log n)$.

\begin{framed}
  Le temps total d'exécution de [pp_voisins_ext l] est donc un
    $O(n\log n)$, où $n$ est la taille de [l].
\end{framed}
\end{rep}

*)
