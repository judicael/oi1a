
#

targets+=$d/ds01.pdf $d/ds01-cor.pdf $d/ds01-bareme.pdf \
	$d/ds02.pdf $d/ds02-cor.pdf

$d/ds01.pdf: exercices/listes/listes-01.tex \
	exercices/listes/listes-02.tex \
	exercices/listes/listes-03.tex \
	exercices/arbres/arbres-01.tex

$d/ds01-cor.pdf: exercices/listes/listes-01-cor.tex \
	exercices/listes/listes-02-cor.tex \
	exercices/listes/listes-03-cor.tex \
	exercices/arbres/arbres-01-cor.tex

e=exercices/prog-dyn/x-2007-psi-pt
f=exercices/eq-complexite/eq-comp

$d/ds02.pdf: $e.tex $e-img.pdf $f-01.tex $f-02.tex $f-03.tex $f-04.tex $f-05.tex

$d/ds02-cor.pdf: $e-cor.tex $e-img.pdf \
  $f-01-cor.tex $f-02-cor.tex $f-03-cor.tex $f-04-cor.tex $f-05-cor.tex


