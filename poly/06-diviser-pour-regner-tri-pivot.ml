(*
\begin{exo}
  On rappelle qu'il existe une fonction [filter : ('a list -> bool) -> 'a list -> 'a list]
  dans le module [List] : 
   [filter p l] retourne la sous-liste des éléments [x] de [l] tels que
   [p x] soit le booléen true.

   Quelle est sa complexité temporelle et spatiale?
\end{exo}

*)

(*
\titrepartie{Fonction de partionnement d'une liste}

On introduit la fonction suivante : *)
(* [partition : 'a * 'a list -> 'a list * 'a list] *)
(* [partition x l] retourne deux listes [u] et [v] telles que [u@v] et
   [l] possèdent les mêmes éléments (avec la même multiplicité) et
   tous les éléments de [u] sont inférieurs ou égaux à [x] et
   tous les éléménts de [v] sont strictement supérieurs à [x]
*)

let partition x l =
  let u = filter (fun y -> y <= x) l in
  let v = filter (fun y -> y > x) l in
  (u, v)
;;
(*l
\begin{exo}
  Quelle est la complexité de la fonction \ocwlowerid{partition} en
  espace et en temps ?
\end{exo}
*)

(* [quicksort : 'a list -> 'a list] *)
(* [quicksort l] retourne une liste [u] triée ayant les mêmes éléments
   que [l] avec la même multiplicité.
*)
let rec quicksort l =
  match l with
  | [] -> []
  | x :: r -> let (u1, u2) = partition x r in
	      let v1 = quicksort u1 in
	      let v2 = quicksort u2 in
	      v1 @ (x::v2)
;;

