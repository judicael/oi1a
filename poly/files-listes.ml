type 'a file = { entree : 'a list; sortie : 'a list };;


let file_vide = { entree = []; sortie = []; };;

let enfile f x = { entree = x :: f.entree; sortie = f.sortie };;

let enfile { entree = e; sortie = s } x = { entree = x::e; sortie = s };;

let enfile { entree; sortie } x = { entree = x :: entree; sortie };;

let rec defile f = match f with
  | { entree = []; sortie = [] } -> failwith "File vide"
  | { entree = e; sortie = x :: r } -> x, { entree = e; sortie = r }
  | { entree = e; sortie = [] } ->
    let s = List.rev e in defile { entree = []; sortie = s }
;;
  
let est_vide f = match f with
  | { entree = []; sortie = [] } -> true
  | _ -> false
