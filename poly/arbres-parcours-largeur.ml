type 'a arbre =
| Vide
| Noeud of 'a arbre * 'a * 'a arbre
;;

type 'a file == 'a list;;
let enfile x f = f@[x];;
let defile f = match f with
  | x::r -> x, r
  | [] -> failwith "file vide !";;
let file_vide = [];;
let est_vide f =  f = [];;


let a0 = Noeud(Vide, 3.2, Vide);;
let a1 = Noeud(Vide, 2.718, Vide);;
let a2 = Noeud(a0,7.4, a1);;
let a3 = Noeud(Vide, 5.2, a1);;
let a4 = Noeud(a2, 1.23, a3);;
let a = Noeud(a3, 2.75, a4);;

ilet parcours_largeur a = 
  let f1 = enfile a file_vide in
  let rec boucle f =
    if not (est_vide f) then
      match defile f with
      | Vide, f' -> boucle f'
      | Noeud(fg, y, fd), f' ->
	begin
	  print_float y;
	  print_string " ";
	  boucle (enfile fd (enfile fg f'))
	end
  in
  boucle f1;;

