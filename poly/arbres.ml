
type 'a arbre = 
| Arbre_vide
| Noeud of 'a arbre * 'a * 'a arbre
;;

type direction =
| Gauche
| Droite
;;

type chemin == direction list;;

(* designe_par : chemin -> 'a arbre -> 'a arbre
   designe_par c a  retourne le sous-arbre de a désigné par le chemin c
*)
let rec designe_par c a =
  match c, a with
  | [], _ -> a
  | Gauche :: c2, Noeud(fg, _, _) -> designe_par c2 fg
  | Droite :: c2, Noeud(_, _, fd) -> designe_par c2 fd
  | _ :: _, Arbre_vide -> failwith "Ceci n'est pas un chemin dans l'arbre"
;;

let rec taille a =
  match a with
  | Arbre_vide -> 0
  | Noeud(fg, _, fd) -> taille fg + taille fd + 1
;;

let rec hauteur a = match a with
  | Arbre_vide -> 0
  | Noeud(fg, _, fd) -> 1 + max (hauteur fg) (hauteur fd)
;;

let rec parcours_prefixe a f = match a with
  | Arbre_vide -> ()
  | Noeud(fg, x, fd) -> 
    begin
      let () = f x in
      let () = parcours_prefixe fg f in
      let () = parcours_prefixe fd f in
      ()
    end
;;

let rec parcours_postfixe a f = match a with
  | Arbre_vide -> ()
  | Noeud(fg, x, fd) -> 
    begin
      let () = parcours_postfixe fg f in
      let () = parcours_postfixe fd f in
      let () = f x in
      ()
    end
;;
let rec parcours_infixe a f = match a with
  | Arbre_vide -> ()
  | Noeud(fg, x, fd) -> 
    begin
      let () = parcours_infixe fg f in
      let () = f x in
      let () = parcours_infixe fd f in
      ()
    end
;;
