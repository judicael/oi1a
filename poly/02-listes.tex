Les \emph{listes} sont une structure de données couramment employée en
informatique.

\titrepartie{Avertissement}

L'usage du terme «listes» en Python est contraire à ce
qui se fait dans tout le reste de l'informatique: \textbf{les
  soi-disant «listes Python» ne sont pas des listes}. Elles constituent une
autre structure de données, en général appelée \emph{tableaux
dynamiques}.

\titrepartie{Définition}

\begin{df}[Listes]
  Une \emph{liste} ou \emph{liste chaînée} est une structure de
  données immuable.

  On obtient une liste à partir des opérations de construction (ou
  \emph{constructeurs}) suivants:
  \begin{itemize}
  \item La liste vide, communément appelée \textbf{nil}.
  \item L'ajout d'un élément $x$ en tête d'une liste $r$, communément appelée
    \textbf{cons}, et dont le résultat est parfois noté
    $\textbf{cons}(x, r)$ ou $(\textbf{cons}\ x\ r)$.
  \end{itemize}

  Lorsqu'une liste est de la forme $\textbf{cons}(x, r)$ (c'est-à-dire
  lorsqu'elle est non-vide), on dit qu'elle possède une tête: $x$, et
  une \emph{queue} (ou un \emph{reste}): $r$.

  Pour obtenir les éléments d'une liste, on dispose des trois
  opérations d'accès (ou \emph{accesseurs}) suivants:
  \begin{itemize}
  \item Une opération de test, disant si la liste est vide ou non.
  \item Une opération permettant d'obtenir la tête d'une liste
    non-vide.
  \item Une opération permettant d'obtenir la queue d'une liste
    non-vide.
  \end{itemize}
\end{df}

\titrepartie{Représentation en mémoire}
\sousparties
\titrepartie{Rappels}
\begin{df}[Octet, mot-mémoire]
  On appelle octet un regroupement de $8$ bits.

  Les ordinateurs actuels travaillent sur des regroupements de $n$
  bits, appelés des \emph{mots}, ou \emph{mots-mémoire}, où $n$ est,
  pour un ordinateur donné, une constante dépendant de l'architecture
  de l'ordinateur. Un mot mémoire est donc un regroupement de $n/8$
  octets.
\end{df}

  Typiquement, les ordinateurs des années $80$ avaient des
  architectures $8$ bits ou $16$ bits (le Personnal Computer d'IBM
  était au départ $16$ bits et la plupart des
  \og{}micro-ordinateurs\fg{} avaient des architectures $8$ bits ou
  hybrides $8$ bits / $16$ bits), les PC travaillant sur $32$ bits
  sont apparus au début des années $90$, et les premières
  architectures $64$ bits sont apparues au milieu des années $90$
  pour des stations de travail
  (architecture Alpha, de DEC, puis Itanium d'Intel), avant de devenir
  le standard au milieu
  des années $2000$ pour ce qui est des ordinateurs de bureau
  (architecture x86-64 d'AMD, ensuite adoptée par Intel).

  Sur les ordinateurs actuels, on peut donc considérer qu'un mot est
  un regroupement de $64$ bits, c'est-à-dire de $8$ octets.

\titrepartie{Organisation de la mémoire}
La \emph{mémoire vive} d'un ordinateur peut être modélisée comme un
tableau de mots. Chaque case de cette mémoire vive est numérotée par
un entier.

On pourrait s'attendre à ce que cette numérotation soit
faite avec des entiers naturels consécutifs mais ce n'est pas le cas:
en fait, pour des raisons historiques, ce sont les octets qui sont
numérotés avec des entiers naturels consécutifs. Sur architecture $64$
bits, où les mots sont constitués de $8$ octets, le premier
mot-mémoire est donc numéroté $0$ et ses $8$ octets sont numérotés
$0$, \ldots, $7$; le second est numéroté $8$ et ses $8$ octets sont
numérotés $8$, \ldots, $15$; etc. Les mots-mémoire ont donc pour
numéro un multiple de $8$.

Un programme en cours d'exécution utilise trois zones de la mémoire,
chacune étant une suite de mots consécutifs:
\begin{description}
\item[Le code exécutable] qui est la zone où est stocké le programme;
  le contenu de cette zone ne change pas au cours de l'exécution du
  programme.
\item[Le tas] qui contient les données du programme.
\item[La pile] contenant:
  \begin{itemize}
  \item les \emph{adresses de retour} des fonctions en cours
    d'exécution (permettant au programme de savoir où continuer
    lorsqu'il termine l'exécution d'une fonction)
  \item et les \emph{variables locales} des
  fonctions (chaque variable locale contient une seule information:
  l'adresse dans le tas de la valeur dénotée par la variable).
  \end{itemize}
\end{description}

Les données dans le tas sont organisées sous forme de
\emph{blocs-mémoire}. Un bloc-mémoire est une zone de mots consécutifs
dans le tas respectant certaines conventions.

En Caml, l'organisation d'un bloc-mémoire suit le schéma présenté
figure~\ref{fig:bloc-memoire}, à savoir:
\begin{enumerate}
\item L'\emph{entête}, premier mot qui sert à donner à la fois la taille du bloc, la
  couleur du bloc, et l'étiquette du bloc:
  \begin{itemize}
  \item La taille du bloc, c'est-à-dire le nombre de mots qu'il
    occupe, est représentée sur les 54 premiers 
    bits du bloc (sur une architecture 64 bits).
  \item La couleur du bloc est codée sur les 2 bits suivants. Cette
    couleur est utilisée par un mécanisme de Caml appelé le
    gestionnaire de mémoire ou GC (pour \textit{Garbage Collector})
    pour savoir si un bloc mémoire est utilisé ou peut être réutilisé.
  \item L'étiquette du bloc est codée sur 8 bits. On verra plus loin à
    quoi sert cette information.
  \end{itemize}
\item Une suite de $n$ mots (avec $n$ possiblement nul) contenant soit
  des données «brutes» (par exemple les caractères contenus dans une
  chaîne de caractère), soit les adresses dans le tas de $n$ données
  supplémentaires.
\end{enumerate}
\begin{figure}
  \centering
\begin{Verbatim}
+------------------------+---------+----------+----------+----------+----
| size of block in words |  color  | tag byte | value[0] | value[1] | ...
+------------------------+---------+----------+----------+----------+----
 <------- 54 bits ------> <-2 bit-> <--8 bit--><-64 bits-><-64 bits->
 <------------------ 64 bits ----------------->
\end{Verbatim}
  \caption{Bloc-mémoire Caml, architecture 64 bits (adapté d'après
    Real World OCaml).}
  \label{fig:bloc-memoire}
\end{figure}

\titrepartie{Représentation des listes en mémoire}

Une liste Caml est soit de la forme $\textbf{nil}$, soit de la forme
$\textbf{cons}(x, r)$. Dans le premier cas, elle est représentée en
mémoire par
un bloc mémoire d'un seul mot, avec une étiquette entière dont la
valeur nous importe peu (on pourra supposer qu'il s'agit de
$0$). Nous la dessinerons comme sur la
figure~\ref{fig:nilcell}.
Dans le second cas, elle est représentée par un bloc mémoire de trois
mots: l'entête, comportant une étiquette différente de l'étiquette de
la liste vide et dont la valeur nous importe peu (on pourra supposer
qu'il s'agit de la valeur $1$), puis deux mots, le
premier contenant l'adresse la valeur de $x$, c'est-à-dire son adresse
en mémoire et la seconde la valeur de $r$, c'est-à-dire son adresse en
mémoire. Nous la dessinerons comme sur la figure~\ref{fig:conscell}.

% \begin{tikzpicture}[node distance=3cm, auto]

% %    \node[head, label=below:toto] (head) {};
% %    \node[data, right of=head]    (A) {\cons};
%     \node[data]    (A) {\cons};
%     \node[data, right of=A]       (B) {\cons};
%     \node[data, right of=B]       (C) {\cons};
%     \node[data, right of=C]       (D) {\cons};
%     \node[data, right of=D]       (E) {\cons};
%     \node[nilcell, right of=E]       (last) {\nilcell};

% %    \draw[fill] (head.center)   circle (0.05);

% %    \path[ptr]  (head.center) --++(right:7.5mm)  |- (A);
% %    \draw[fill] ($(A.south)!0.5!(A.text split)$) circle (0.05);
% %    \draw[fill] ($(A.three split)!0.5!(A.east)$) circle (0.05);
%     \draw[fill] ($(A.three south)!0.5!(A.three north)$) circle (0.05);
% %    \draw[ptr]  ($(A.south)!0.5!(A.text split)$) --++(right:10mm) |- (B);
%     \draw[ptr]  ($(A.three south)!0.5!(A.three north)$) --++(right:10mm) |- (B);
%     \draw[fill] ($(B.three south)!0.5!(B.three north)$) circle (0.05);
%     \draw[ptr]  ($(B.three south)!0.5!(B.three north)$) --++(right:10mm) |- (C);
%     \draw[fill] ($(C.south)!0.5!(C.text split)$) circle (0.05);
%     \draw[ptr]  ($(C.south)!0.5!(C.text split)$) --++(right:10mm) |- (D);
%     \draw[fill] ($(D.south)!0.5!(D.text split)$) circle (0.05);
%     \draw[ptr]  ($(D.south)!0.5!(D.text split)$) --++(right:10mm) |- (E);
%     \draw[fill] ($(E.south)!0.5!(E.text split)$) circle (0.05);
%     \draw[ptr]  ($(E.south)!0.5!(E.text split)$) --++(right:10mm) |- (last);

% \end{tikzpicture}

\begin{figure}
  \centering
% \begin{tikzpicture}[node distance=3cm, auto]

% %    \node[head, label=below:toto] (head) {};
% %    \node[data, right of=head]    (A) {\cons};
%     \node[nilcell]       (nil) {\nilcell};
% \end{tikzpicture}
  \includegraphics{02-listes/nilcell.pdf}
  \caption{Représentation de la liste vide en mémoire}
  \label{fig:nilcell}
\end{figure}
\begin{figure}
  \centering
% \begin{tikzpicture}[node distance=3cm, auto]

% %    \node[head, label=below:toto] (head) {};
% %    \node[data, right of=head]    (A) {\cons};
%     \node[data]       (h) {\cons};
%     \node[rectangle, draw,below left of=h] (x) {bloc mémoire de $x$};
%     \node[rectangle, draw,below right of=h] (r) {bloc mémoire de $r$};
%     \draw[ptr] ($(h.two south)!0.5!(h.two north)$) -> (x);
% \end{tikzpicture}
  \includegraphics{02-listes/conscell.pdf}
  \caption{Représentation de $\textbf{cons}(x, r)$ en mémoire}
  \label{fig:conscell}
\end{figure}

Ainsi la liste $\textbf{cons}(3.14, \textbf{cons}(2.718,
\textbf{nil}))$ sera représentée 
$3.14$ sera dessinée de la façon donnée figure~\ref{fig:liste2flottants}.
\begin{figure}
  \centering
  \includegraphics{02-listes/liste2flottants.pdf}
  \caption{Représentation de $\textbf{cons}(3.14, \textbf{cons}(2.718,
\textbf{nil}))$ en mémoire}
  \label{fig:liste2flottants}
\end{figure}
Enfin, on donne figure~\ref{fig:longueliste} une représentation d'une
liste de cinq éléments.
\begin{figure}
  \centering
  \includegraphics{02-listes/longueliste.pdf}
  \caption{Représentation de la liste $1.1$, $2.2$, $3.3$, $4.4$ et
    $5.5$ en mémoire.}
  \label{fig:longueliste}
\end{figure}

Conséquences sur l'accès aux listes: chercher à utiliser une fonction
d'accès au $n$-ième élément n'est presque jamais une bonne idée.


\finsousparties

\titrepartie{La construction de listes en Caml}

En Caml, la liste vide se note \texttt{[]}.

Et $\textbf{cons}(x,r)$ se note $x\texttt{::}r$. Attention:
l'opérateur \texttt{::} associe à droite, ce qui signifie que
\texttt{x::y::z} signifie la même chose que \texttt{x::(y::z)}.

\begin{Verbatim}
# [];;
- : 'a list = []
# fun x y -> x::y ;;
- : 'a -> 'a list -> 'a list = <fun>
# let l1 = 5.5::[];;
val l1 : float list = [5.5]
# let l2 = 4.5::l1;;
val l2 : float list = [4.5; 5.5]
\end{Verbatim}
Remarquez qu'au lieu d'afficher \texttt{5.5::[]} ou
\texttt{4.5::5.5::[]}, Caml utilise une notation plus agréable:
respectivement \texttt{[5.5]} ou \texttt{[4.5; 5.5]}. Cette notation
est aussi acceptée par Caml en entrée:
\begin{Verbatim}
# let l3 = [4.5; 5.5];;
val l3 : float list = [4.5; 5.5]
# l2 = l3;;
- : bool = true
\end{Verbatim}
Cette notation \texttt{[4.5; 5.5]} plus agréable conduit cependant
Caml à construire la liste exactement de la même façon.\footnote{On
  dit que cette notation est du \emph{sucre syntaxique} pour signifier
  qu'il s'agit juste d'une sucrerie: quelque chose qui est agréable
  mais qui ne manquerait pas vraiment s'il n'était pas là.}

On peut évidemment construire des listes d'autre chose que des
flottants:
\begin{Verbatim}
# [true; false; false; true];;
- : bool list = [true; false; false; true]
\end{Verbatim}

Remarquez que la construction de \texttt{l2} n'a pas modifié
\texttt{l1}. Nous donnons figure~\ref{fig:l123} les représentations
des valeurs des trois variables \texttt{l1}, \texttt{l2},
\texttt{l3} en mémoire.
\begin{figure}
  \centering
  \includegraphics{02-listes/listes123.pdf}
  \caption{Représentation de \texttt{l1}, \texttt{l2} et \texttt{l3} en mémoire.}
  \label{fig:l123}
\end{figure}


\begin{rmq}[Homogénéité des listes]
Les listes doivent être homogènes:

\begin{Verbatim}
# [1; true; "toto"];;
Characters 4-8:
  [1; true; "toto"];;
      ^^^^
Error: This expression has type bool but an expression was expected of type
         int.
\end{Verbatim}
\end{rmq}

\titrepartie{Opération d'accès: le filtrage}

Pour accéder à une liste, on utilise un outil très puissant appelé
\emph{filtrage} (en anglais \emph{pattern-matching}).

Exemple pour obtenir la tête d'une liste:

\begin{Verbatim}
let tete l = match l with
  | [] -> failwith "Erreur : liste vide"
  | x :: r -> x
;;

tete [11; 22; 33];;
\end{Verbatim}

L'expression \ocwkw{match} \ldots{} \ocwkw{with} \ldots{} est appelée \emph{filtrage}. Les
lignes de la forme \ocwkw{|} \ldots{} \ocwkw{->} \ldots{} sont appelés des \emph{cas de
filtrage}. Les expressions à gauche de la flèche \ocwkw{->} sont
appelés des \emph{motifs}

%\pageref{grammaireCaml}
Dans la grammaire de Caml, un \emph{filtrage} est un cas particulier
d'expression: un \emph{filtrage} s'exprime sous la forme
\begin{equation*}
  \mkw{match} \nonterm{expression} \mkw{with} \nonterm{cas de
    filtrage} \ldots
\end{equation*}
Un \emph{cas de filtrage} est de la forme
\begin{equation*}
  \mkw{|} \nonterm{motif} \mkw{->} \nonterm{expression}
\end{equation*}
Un \emph{motif} est l'une des formes suivantes\footnote{Pour
  l'instant, du moins. On verra plus tard qu'il y a encore bien
  d'autres motifs possibles.}:
\begin{gather*}
  \mkw{\_}\\
  \nonterm{variable}\\
  \mkw{[]}\\
  \nonterm{motif} \mkw{::} \nonterm{motif}\\
  \nonterm{constante flottante, entière, booléenne}
\end{gather*}

\begin{exo}
  \texttt{\_:: 0 :: 1 :: r} est-il un motif?
\end{exo}

\begin{rmq}
  OCaml refuse tout motif dans lequel une même variable apparaît
  plusieurs fois.
\end{rmq}

\begin{df}
On dit qu'un motif \emph{filtre} une valeur $v$ si, en remplaçant toutes
les variables du motif et chaque apparition de \texttt{\_} par des
valeurs bien choisies, on obtient la 
valeur $v$.
\end{df}

Un filtrage est évalué de la façon suivante: on évalue d'abord
l'expression située entre \texttt{match} et \texttt{with} pour obtenir
une certaine valeur. On cherche alors le premier motif qui \emph{filtre}
cette valeur.

(dans la suite, on regardera à chaque fois comment Caml fonctionne
pour effectuer ces filtrages).

\begin{exo}
  Écrire une fonction testant si une liste est non-vide.
\end{exo}

\begin{exo}
  Écrire une fonction testant si une liste a exactement deux éléments.
\end{exo}

\begin{exo}
  Écrire une fonction testant si le premier élément d'une liste de
  booléens vaut \texttt{true}.
\end{exo}
\begin{exo}
  Écrire une fonction testant si le premier élément d'une liste de
  booléens vaut \texttt{true}
et le deuxième vaut \texttt{false}.
\end{exo}
\begin{exo}
  Écrire une fonction testant si une liste possède deux
  éléments ou moins.
\end{exo}
\begin{exo}
  Écrire une fonction testant si une liste possède $0$ éléments ou au moins $3$.
\end{exo}
\begin{exo}
  Écrire une fonction testant si le premier élément d'une liste
  d'entiers vaut \texttt{42}.
\end{exo}

\begin{exo}
  Écrire une fonction prenant en argument une liste $l$ et un élément
  $x$ et testant si le premier élément de $l$ est $x$.
  Testez votre fonction avec différentes valeurs de $l$ et $x$.
\end{exo}

\begin{exo}
  Écrire une fonction calculant la longueur d'une liste
(\texttt{list\_length}).
\end{exo}

\begin{exo}
  Écrire une fonction \texttt{member} testant l'appartenance d'un
  élément à une liste.
\end{exo}

\begin{exo}
  Écrire une fonction sommant les éléments (entiers) d'une liste.
\end{exo}

\begin{exo}
  Écrire une fonction \texttt{f} calculant la liste des carrés des
  éléments d'une liste. Par exemple, si \texttt{l} vaut \texttt{[10;
    11; 42; 17]}, \texttt{f l} devra retourner la liste \texttt{[100;
    121; 1764; 289 ]}.
\end{exo}

\begin{exo}
  Écrire une fonction \texttt{append} calculant la concaténation de
  deux listes. Indication: c'est évident si la première liste est
  vide.
\end{exo}

\begin{exo}
  Écrire une fonction \texttt{filter} prenant deux arguments:
  \texttt{f : 'a -> bool} et \texttt{l : 'a list} et retournant la
  liste des éléments \texttt{x} de \texttt{l} tels que \texttt{f x}
  soit vrai.
\end{exo}

\begin{exo}
  Écrire une fonction prenant en argument une liste de valeurs et la
  retournant triée par ordre croissant.

  Montrez que votre fonction termine. Quelle est sa complexité dans le
  cas le pire? Dans le cas le meilleur?
\end{exo}

% Construction de longues listes aléatoires (\texttt{random\_\_float :
%   float -> float}).
% Tri pivot/fusion avec des listes (split : sous-spécifié donc
% intéressant).
