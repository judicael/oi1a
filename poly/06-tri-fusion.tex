\titrepartie{Partage de listes}
\sousparties
\titrepartie{Code}
\begin{lstlisting}
(* split : 'a list -> 'a list * 'a list
  (split l) partage la liste l en deux listes de mêmes longueur : si
  l est de longueur n, (split l) retourne un couple (l1, l2) tel que
  l1@l2 a les mêmes éléments que l, avec même multiplicité et
  l1 et l2 sont de longueurs respectives n' et n'' où n' vaut n/2,
  arrondi par excès et n'' vaut n/2, arrondi par défaut.
 *)
let rec split l = match l with
  | [] -> [], []
  | x::r -> let l1, l2 = split r in x::l2, l1
;;
\end{lstlisting}

\titrepartie{Correction}
On peut montrer que pour toute liste $l$ de longueur $n$, l'appel
\texttt{split $l$} termine en effectuant $n$ appels récursifs
supplémentaires par récurrence sur $n$. On montre en même temps que
cet appel retourne deux listes $l_1$ et $l_2$ de longueurs respectives
$\ceil{n}$ et $\floor{n}$.

\titrepartie{Complexité}
De plus, à chaque appel de \texttt{split}, on effectue uniquement un
nombre borné d'opérations qui sont toutes de temps constant, sauf
l'éventuel appel récursif. On en déduit que l'exécution de
\texttt{split $l$} prend un temps $n+1 \times O(n) = O(n)$.
\finsousparties

\titrepartie{Fusion de listes triées}
\sousparties
\titrepartie{Code}
\begin{lstlisting}
  
(* merge : 'a list -> 'a list * 'a list
   (merge l1 l2) retourne une liste contenant les mêmes éléments que l, 
   avec même multiplicité.
   De plus, si l1 et l2 sont des listes triées par ordre croissant,
   alors (merge l1 l2) est une liste triée.
 *)
let rec merge l1 l2 = match l1, l2 with
  | [], l -> l
  | l, [] -> l
  | x1::r1, x2::r2 -> if x1 <= x2 then x1 :: merge r1 l2
		      else x2 :: merge l1 r2
;;
\end{lstlisting}

\titrepartie{Correction}
Montrons que pour toutes listes $l_1$ et $l_2$ triées par ordre
croissant, l'appel \texttt{fusion $l_1$ $l_2$} termine et retourne
une liste $l$ triée par ordre croissant dont les éléments sont les
mêmes que ceux de \texttt{$l_1$@$l_2$} (avec même multiplicité).

Pour cela, notons pour $n\in \N$, $P(n)$ l'assertion «pour toutes
listes $l_1$ et $l_2$ triées par ordre croissant dont la somme des longueurs vaut $n$, la
fonction \texttt{fusion $l_1$ $l_2$} termine et retourne une liste $l$
triée par ordre croissant dont les éléments sont les mêmes que ceux de
$l$».

\begin{itemize}
\item Montrons $P(0)$. Deux listes dont la somme des longueurs vaut $0$ sont
  nécessairement vides. Or \texttt{fusion [] []} retourne \texttt{[]}
  qui possède les mêmes éléments que \texttt{[]@[]} et qui est
  triée. On a donc $P(0)$.
\item Montrons que $P$ est héréditaire. Pour cela, considérons
  $n\in\N$ quelconque vérifiant $P(n)$ et montrons
  $P(n+1)$. Considérons donc deux listes $l_1$ et $l_2$ triées par
  ordre croissant dont la somme des longueurs vaut $n+1$. On peut alors
  distinguer trois cas:
  \begin{itemize}
  \item Si $l_1$ est
    vide, \texttt{fusion $l_1$ $l_2$} retourne $l_2$, qui est triée par
    ordre croissant et possède les mêmes éléments que
    \texttt{$l_1$@$l_2$}.
  \item Si $l_2$ est vide, on a le même résultat.
  \item Si ni $l_1$, ni $l_2$ ne sont vides. Alors, il y a deux
    sous-cas:
    \begin{itemize}
    \item Si $x_1\leq x_2$, alors \texttt{fusion $l_1$ $l_2$} retourne le
      résultat de l'évaluation de \texttt{$x_1$ :: fusion $r_1$ $l_2$}. Or
      $r_1$ et $l_2$ sont triées et la somme des longueurs de $r_1$ et
      $l_2$ vaut $n$. Comme $P(n)$ est vrai, \texttt{fusion $r_1$
        $l_2$} retourne une liste $l$ triée ayant les mêmes éléments
      que \texttt{$r_1$@$l_2$}. \texttt{$x_1$ :: fusion $r_1$ $l_2$}
      retourne donc une liste ayant les mêmes éléments que
      \texttt{$x_1$::$r_1$@$l_2$}, donc que \texttt{$l_1$@$l_2$}. De
      plus, $l_1$ et $l_2$ sont triées donc $x_1$ minore tous les
      éléments de $r_1$ et $x_2$ tous ceux de $l_2$. Comme de plus
      $x_1\leq x_2$, $x_1$ minore tous les éléments de
      \texttt{$r_1$@$l_2$}, donc de $l$, qui est triée. Donc
      \texttt{$_1x$::$l$} est triée. L'appel \texttt{fusion $l_1$
        $l_2$} termine et retourne une liste triée contenant les mêmes
      éléments que \texttt{$l_1$@$l_2$}.
    \item Si $x_1 > x_2$, on montre de même que l'appel \texttt{fusion $l_1$
        $l_2$} termine et retourne une liste triée contenant les mêmes
      éléments que \texttt{$l_1$@$l_2$}.
    \end{itemize}
  \end{itemize}
  $P$ est donc héréditaire.
\end{itemize}
On en déduit que $P(n)$ est vrai pour tout entier $n$.

Soit $l_1$ et $l_2$ deux listes triées. Notons $n$ la somme de leur
longueur. On déduit du fait qu'on a $P(n)$, que l'appel 
\texttt{fusion $l_1$ $l_2$} termine et retourne une liste triée
contenant les mêmes éléments que \texttt{$l_1$@$l_2$}.

\titrepartie{Complexité}
On peut noter qu'à chaque appel de \texttt{fusion}, on
effectue uniquement un nombre borné d'opérations, qui sont toutes de
temps constant, sauf l'appel récursif. On en déduit que la complexité
temporelle de l'exécution de \texttt{fusion $l_1$ $l_2$} est un $O(n)$
où $n$ est la somme des longueurs de $l_1$ et $l_2$.
\finsousparties

\titrepartie{Tri fusion}
\sousparties
\titrepartie{Code}
\begin{lstlisting}
(* tri_fusion : 'a list -> 'a list
   (tri_fusion l) trie la liste l par ordre croissant, i.e. retourne
   une liste triée par ordre croissant ayant les mêmes éléments que l,
   avec même multiplicité.
 *)
let rec tri_fusion l = match l with
  | [] -> []
  | [_] -> l
  | _ -> let l1, l2 = split l in merge (tri_fusion l1) (tri_fusion l2)
;;
\end{lstlisting}

\titrepartie{Correction}

Par l'absurde, supposons que le code de \texttt{tri\_fusion} ne
respecte pas sa spécification, c'est-à-dire qu'il existe au moins une liste $l$
telle que \texttt{tri\_fusion $l$} ne termine pas ou ne retourne pas
une liste triée par ordre croissant ayant les mêmes éléments que $l$
et choisissons $l$ de longueur minimale.

On a alors trois cas:
\begin{itemize}
\item $l$ est vide et \texttt{tri\_fusion $l$} retourne $[]$ qui est
  une liste triée ayant les mêmes éléments que $l$. C'est absurde.
\item $l$ contient un unique élément, et \texttt{tri\_fusion $l$}
  retourne $l$ qui est triée et possède les mêmes éléments
  qu'elle-même. C'est absurde.
\item $l$ contient au moins deux éléments. Notons $n$ sa
  longueur. Alors notons $l_1$ et $l_2$ les listes retournées par
  l'évaluation de \texttt{split $l$}. $l_1$ et $l_2$ sont
  respectivement de longueurs $\ceil{n/2}$ et $\floor{n/2}$. Comme
  $n\geq 2$, $n - \ceil{n/2} = \floor{n/2} \geq 1$, donc $n >
  \ceil{n/2}\geq\floor{n/2}$. $l_1$ et $l_2$ sont donc de longueurs
  strictement inférieures à celle de $l$. Comme $l$ est un
  contre-exemple de longueur minimale à la spécification de
  \texttt{tri\_fusion}, \texttt{tri\_fusion $l_i$}, pour $i=1, 2$,
  retourne donc une liste $l'_i$ triée ayant même éléments que $l_i$.
  Or \texttt{tri\_fusion $l$} retourne une liste qui est obtenue
  par fusion de $l'_1$ et $l'_2$. Cette liste possède donc les mêmes
  éléments que \texttt{$l'_1$@$l'_2$}, donc que \texttt{$l_1$@$l_2$},
  et de plus elle est triée, ce qui est absurde.
\end{itemize}
Donc dans tous les cas, on obtient une absurdité.

On en déduit que le code de \texttt{tri\_fusion} respecte sa
spécification: pour toute liste $l$, \texttt{tri\_fusion $l$} termine
et retourne une liste triée par ordre croissant ayant les mêmes
éléments que $l$.

\titrepartie{Complexité}
\sousparties
\titrepartie{Équation à résoudre}

Notons, pour $n\in\N$, $C_{TF}(n)$ le temps de calcul mis dans le cas
le pire par \texttt{tri\_fusion $l$} pour une liste $l$ de longueur
$n$.

Les opérations effectuées par \texttt{tri\_fusion} sur une liste $l$ de
longueur $n\geq 2$ sont un filtrage (en temps constant), un appel à
\texttt{split $l$} en temps $O(n)$, un appel à \texttt{fusion} sur
deux listes $l'_1$ et $l'_2$ de longueurs respectives $\ceil{n/2}$ et
$\floor{n/2}$, ce qui demande un temps $O(\ceil{n/2} + \floor{n/2}) =
O(n)$, et deux appels récursifs à \texttt{tri\_fusion} sur des listes
de longueurs $\ceil{n/2}$ et $\floor{n/2}$, dont les temps de calcul
sont donc au plus respectivement $C_{TF}(\ceil{n/2})$ et
$C_{TF}(\floor{n/2})$.

Le temps de calcul de \texttt{tri\_fusion $l$} est donc au plus $O(n)
+ O(1) + O(n) + C_{TF}(\ceil{n/2})+C_{TF}(\floor{n/2})$.

Cette inégalité étant vérifiée pour toute liste de longueur $l$, le
temps de calcul de \texttt{tri\_fusion} sur une liste de longueur $n$
dans le cas le pire vérifie:

\begin{equation*}
  C_{TF}(n) \leq C\p{\floor{\frac{n}{2}}} + C\p{\ceil{\frac{n}{2}}} + O(n)
\end{equation*}

\titrepartie{Résolution}

Ou bien on suppose que $C_{TF}$ est croissante, ou bien on introduit
plutôt $C'_{TF}(n)$, le temps de calcul de \texttt{tri\_fusion} dans
le cas le pire pour une liste de longueur \emph{au plus} $n$.

On a, pour tout $n\in \N$, $C_{TF}(n) \leq C'_{TF}(n) \leq C'_{TF}(n+1)$
(pour toute liste de longueur $n$, \texttt{tri\_fusion} met un temps
au plus égal à $C'_{TF}(n)$ et pour toute liste de longueur au plus
$n$, \texttt{tri\_fusion} met un temps au plus $C'_{TF}(n+1)$).

Donc $C'_{TF}$ majore $C_{TF}$ et est croissante.

De plus, on a:
\begin{equation*}
    C'_{TF}(n) \leq C'\p{\floor{\frac{n}{2}}} + C'\p{\ceil{\frac{n}{2}}} + O(n)
\end{equation*}

Il suffit de reprendre la démonstration précédente pour se convaincre
que c'est le cas.\footnote{Une
  autre possibilité est de remarquer que pour tout $n\in\N$,
  $C'_{TF}(n) = \max_{i\in\cii{0,n}}(C_{TF}(i))$ et d'effectuer un
  travail (pénible) sur les inégalités.}

On peut maintenant essayer de calculer $C'_{TF}$ pour des valeurs
particulières. Pour $k\in\N$, posons $u_k = C'_{TF}(2^k)$.

Alors, à partir d'un certain rang $u_k \leq 2 u_{k-1} + C 2^k$ où $C$
est une constante.

Une solution particulière de l'équation homogène $\forall k\in\N\quad
u_k = 2 u_{k-1}$ est la suite $\p{2^k}_{k\in \N}$. Posons donc
$\lambda_k = u_k 2^{-k}$ pour $k\in \N$.

On a alors:
\begin{align*}
  \lambda_k 2^{k} &\leq 2^k \lambda_{k-1} + C 2^k & \text{APCR}\\
  \lambda_k &\leq \lambda_{k-1} + C &\text{APCR}\\
  \lambda_k - \lambda_{k-1} &\leq C &\text{APCR $k_0$}\\
  \lambda_k - \lambda_{k_0} & \leq (k- k_0) C &\text{pour $k\geq k_0$}\\
  \lambda_k & \leq (k- k_0) C + \lambda_{k_0} &\text{pour $k\geq k_0$}\\
  \lambda_k &= O(k)\\
  u_k &= O(k 2^k)\\
\end{align*}
Or pour tout $n$, on a $n \leq 2^{\ceil{\log_2 n}}$ et $C'_{TF}$ est
croissante. Donc, on a:
\begin{equation*}
  C_{TF}(n) \leq C'_{TF}(n)\leq C'_{TF}\p{2^{\ceil{\log_2 n}}} =
  u_{\ceil{\log_2 n}} = O\p{\ceil{\log_2 n} 2^{\ceil{\log_2 n}}}
\end{equation*}
Or $O\p{\ceil{\log_2 n} 2^{\ceil{\log_2 n}}} = O(n \log_2 n)$ et
$C_{TF}(n) \geq 0$.

Donc, finalement:
\begin{equation*}
  C_{TF}(n) = O(n \log n)
\end{equation*}

\finsousparties
\finsousparties

\titrepartie{Conclusion sur le tri fusion}

Le tri fusion est un exemple ultra-classique du paradigme «diviser
pour régner». Il est moins naturel que le tri par insertion que nous
avons étudié précédemment, et est donc un peu plus délicat à
programmer. Cette difficulté supplémentaire valait-elle le coup?

Le tri par insertion possède une complexité temporelle en $O(n^2)$
pour une liste de longueur $n$. Le tri fusion possède quant à lui une
complexité $O(n\log n)$. On peut donc dire qu'on a gagné un facteur
$n/\log n$ mais ce n'est pas très parlant.

Essayons de quantifier ce gain en regardant le temps de calcul de
programmes qui demanderaient respectivement $n$, $n\log_2 n$, $n^2$, $2^n$ et $n!$
instructions pour un même problème de taille $n$, en supposant que le
temps de calcul d'une instruction est d'une nano-seconde. Le
tableau~\ref{tab:tempsexec} donne ces temps de calcul pour différentes
valeurs de $n$. On voit que les complexités linéarithmique ($O(n\log
n)$) et quadratique ($O(n^2)$) diffèrent vite.

% TODO : Refaire la table en la calculant automatiquement. Tikz ?
\begin{table*}
  \centering
  \begin{tabular}{rrrrrr}\toprule
    $n$       & $n$ \si{ns} & $n\log_2 n$ \si{ns} & $n^2$ \si{ns}      & $2^n$\si{ns}      & $n!$ \si{ns}\\ \midrule
    \num{10}  & \SI{10}{ns} & \SI{30}{ns}         & \SI{e2}{ns}        & \SI{1}{\micro s} &\SI{3}{ms}\\
    \num{20} & \SI{20}{ns} & \SI{90}{ns}          & \SI{4e2}{ns}       & \SI{1e1}{ms}     &\SI{8e1}{ans}\\
    \num{30} & \SI{30}{ns} & \SI{e2}{ns}          & \SI{9e2}{ns}       & \SI{1}{s}        &\SI{8e15}{ans}\\
    \num{40} & \SI{40}{ns} & \SI{2e2}{ns}         & \SI{2}{\micro s}  & \SI{2e1}{\minute}& \\
    \num{50} & \SI{50}{ns} & \SI{3e2}{ns}         & \SI{3}{\micro s} & \SI{10}{jours}& \\
    \num{60} & \SI{60}{ns} & \SI{4e2}{ns}         & \SI{4}{\micro s} & \SI{40}{ans} & \\
    \num{70} & \SI{70}{ns} & \SI{4e2}{ns}         & \SI{5}{\micro s} & \SI{4e4}{ans}& \\
    \num{80} & \SI{80}{ns} & \SI{5e2}{ns}         & \SI{6}{\micro s} & \SI{4e7}{ans}& \\
    \num{90} & \SI{90}{ns} & \SI{6e2}{ns}         & \SI{8}{\micro s} & \SI{4e10}{ans}& \\
    \num{e2} & \SI{e2}{ns} & \SI{7e2}{ns} & \SI{10}{\micro s}\\
    \num{e3} & \SI{1}{\micro s} & \SI{10}{\micro s} & \SI{1}{\milli
                                                      s}\\
    \num{e4} & \SI{10}{\micro s} & \SI{1.3e2}{\micro s} & \SI{100}{\milli
                                                      s}\\
    \num{e5} & \SI{e2}{\micro s} & \SI{1.7}{\milli s} & \SI{10}{s}\\
    \num{e6} & \SI{1}{\milli s} & \SI{2.0e1}{\milli s} & \SI{1.7e1}{\minute}\\
    \num{e7} & \SI{10}{\milli s} & \SI{2.3e2}{\milli s} & \SI{28}{\hour}\\
    \num{e8} & \SI{100}{\milli s} & \SI{2.7}{s} & \SI{1e2}{jours}\\
    \num{e9} & \SI{1}{s} & \SI{3e1}{s} & \SI{3e1}{ans}\\
        \bottomrule
  \end{tabular}
  \caption{Temps d'exécution pour quelques complexités classiques}
  \label{tab:tempsexec}
\end{table*}

