
(* arbres binaires-entiers *)
type ('a, 'b) abe =
  | Noeud_abe of ('a, 'b) abe * 'a * ('a, 'b) abe
  | Feuille_abe of 'b
;;

let rec hauteur_abe a = match a with
  | Feuille_abe _ -> 0
  | Noeud_abe (fg, _, fd) -> 1 + max (hauteur_abe fg) (hauteur_abe fd)
;;

(* la taille d'un arbre binaire-entier est définie comme
   nombre de ses nœuds internes : *)

let rec taille_abe a = match a with
  | Feuille_abe _ -> 0
  | Noeud_abe (a1, _, a2) -> 1 + taille_abe a1 + taille_abe a2
;;

(* arbres binaires-unaires *)
type 'a arbre =
  | Vide
  | Noeud of 'a arbre * 'a * 'a arbre
;;

let a5 = Noeud(Noeud(Vide, 22, Vide), 19, Vide);;
let a2 = Noeud(Noeud(Vide, 7, Vide), 12, Noeud(a5, 73, Vide));;
let a3 = Noeud(Noeud(Vide, 10, Vide), 42, Noeud(Noeud(Vide, 5, Vide), 33, Vide));;
let a4 = Noeud(Vide, 17, Noeud(Vide, 6, Vide));;
let a1 = Noeud(a3, 3, a4);;
let a = Noeud(a1, 4, a2);;

let rec parcours_prefixe a = match a with
  | Vide -> []
  | Noeud(a1, i, a2) -> i :: parcours_prefixe a1 @ parcours_prefixe a2
;;
parcours_prefixe a;;
let rec peigne_gauche n = if n = 0 then Vide else Noeud(peigne_gauche (n-1), 42, Vide);;

let rec parcours_aux a l = match a with
  | Vide -> l
  | Noeud(a1, i, a2) ->
    let l1 = parcours_aux a2 l in
    let l2 = parcours_aux a1 l1 in
    i :: l2
;;

let parcours_prefixe a = parcours_aux a [];;

let l = parcours_prefixe (peigne_gauche 10000);;

let rec parcours_postfixe a = match a with
  | Vide -> []
  | Noeud(a1, i, a2) -> parcours_postfixe a1 @ parcours_postfixe a2 @ [i] 
;;
parcours_postfixe a;;
let rec peigne_gauche n = if n = 0 then Vide else Noeud(peigne_gache (n-1), 42, Vide);;

let rec parcours_aux a l = match a with
  | Vide -> l
  | Noeud(a1, i, a2) ->
    let l0 = i :: l in
    let l1 = parcours_aux a2 l0 in
    parcours_aux a1 l1
;;

let parcours_infixe a = parcours_aux a [];;

let rec parcours_infixe a = match a with
  | Vide -> []
  | Noeud(a1, i, a2) -> parcours_infixe a1 @ [i] @ parcours_infixe a2
;;
parcours_infixe a;;
let rec peigne_gauche n = if n = 0 then Vide else Noeud(peigne_gache (n-1), 42, Vide);;

let rec parcours_aux a l = match a with
  | Vide -> l
  | Noeud(a1, i, a2) ->
    let l1 = parcours_aux a2 l in
    parcours_aux a1 (i::l1)
;;

let parcours_infixe a = parcours_aux a [];;

let rec hauteur a = match a with
  | Vide -> 0
  | Noeud (fg, _, fd) -> 1 + max (hauteur fg) (hauteur fd)
;;

(* la taille d'un arbre binaire-unaire est définie comme
   nombre de ses nœuds internes : *)
let rec taille a = match a with
  | Vide -> 0
  | Noeud (a1, _, a2) -> 1 + taille a1 + taille a2
;;

(* Liens entre arbres binaires entiers et binaires-unaires *)

(* On peut représenter tout ('a, 'a) abe par un 'a arbre : *)
let rec arbre_de_abe a = match a with
  | Feuille_abe x -> Noeud(Vide, x, Vide)
  | Noeud_abe (a1, x, a2) -> Noeud(arbre_de_abe a1, x, arbre_de_abe a2)
;;

(* Si on enlève les feuilles d'un arbre binaire entier, on obtient un
   arbre binaire-unaire *)
let rec squelette a = match a with
  | Feuille_abe _ -> Vide
  | Noeud_abe (a1, x, a2) -> Noeud(squelette a1, x, squelette a2)
;;

(* En ajoutant des feuilles arbitraires contenant la valeur v
   à un arbre binaire entier a, on obtient un arbre binaire-unaire a'
   dont le squelette est a :
*)
let rec ajoute_feuille a v = match a with
  | Vide -> Feuille_abe v
  | Noeud(a1, x, a2) ->
    Noeud_abe(ajoute_feuille a1 v, x, ajoute_feuille a2 v)
;;

(* On désigne un nœud ou un sous-arbre d'un arbre a donné, en
   indiquant le chemin à suivre depuis la racine.
*)
type direction = | Gauche | Droite;;
type chemin == direction list;;

(* sous_arbre : 'a arbre -> chemin -> 'a arbre *)
let rec sous_arbre a c = match a, c with
  | _, [] -> a
  | Noeud(a', _, _), Gauche :: c' -> sous_arbre a' c'
  | Noeud(_, _, a'), Droite :: c' -> sous_arbre a' c'
  | Vide, _ :: _ -> failwith "Chemin invalide"
;;

type op = 
| Plus
| Moins
| Mult
| Div
;;

let rec eval a = match a with
  | Feuille_abe n -> n
  | Noeud_abe(e1, Plus, e2) -> eval e1 + eval e2
  | Noeud_abe(e1, Moins, e2) -> eval e1 - eval e2
  | Noeud_abe(e1, Mult, e2) -> eval e1 * eval e2
  | Noeud_abe(e1, Div, e2) -> eval e1 / eval e2
;;

(* type des expressions arithmétiques *)
type expr =
| ECte of int
| EPlus of expr * expr
| EMoins of expr * expr
| EMult of expr * expr
| EDiv of expr * expr
| EOpp of expr (* opposé *)
;;

let rec eval e = match e with
| ECte c -> c
| EPlus (e1, e2) -> eval e1 + eval e2
| EMoins (e1, e2) -> eval e1 - eval e2
| EMult (e1, e2) -> eval e1 * eval e2
| EDiv (e1, e2) -> eval e1 / eval e2
| EOpp e' -> - eval e'
;;  

eval (EOpp(EPlus(EMoins(ECte 25, ECte 4),EMult(ECte 3, ECte 7))));;

(* On représente des files par des listes caml :
   - le premier élément à traiter est en tête de la liste
   - on ajoute les nouveaux éléments à la fin de la liste
*)
type 'a file == 'a list

let est_file_vide q = match q with [] -> true | _ -> false;;
let file_vide = [];;
let enfile q x = q@[x];;
let defile q = match q with
  | [] -> failwith "File vide !"
  | x :: r -> x, r
;;

(* parcours_largeur_aux : 'a arbre file -> 'a list *)
let rec parcours_largeur_aux q =
  if est_file_vide q then []else let a, q' = defile q in
       match a with
       | Vide -> parcours_largeur_aux q'
       | Noeud(a1, x, a2) ->
	 let q1 = enfile q' a1 in
	 let q2 = enfile q1 a2 in
	 x::parcours_largeur_aux q2
;;

let parcours_largeur a =
  let q = enfile file_vide a in
  parcours_largeur_aux q
;;

let rec pow x n = if n = 0 then 1 else
    if n mod 2 = 0 then pow (x*x) (n/2)
    else x * pow (x*x) (n/2)
;;

(* est_parfait : 'a arbre -> bool
   dit si l'arbre a est parfait
*)
let est_parfait a =
  let h = hauteur a in
  let n = taille a in
  pow 2 h - 1 = n
;;

type classification =
| CParfait
| CQuasiCompletNonParfait
| CNonQuasiComplet
;;

(* classifie : 'a arbre -> int * classification 
   retourne la hauteur de l'arbre et son "type"
*)
let rec classifie a = match a with
  | Vide -> 0, CParfait
  | Noeud(a1, x, a2) ->
    let h1, c1 = classifie a1 in
    let h2, c2 = classifie a2 in
    let h = 1 + max h1 h2 in
    let c = match c1, c2 with
      | CParfait, CParfait when h1 = h2 -> CParfait
      | CParfait, CParfait when h1 = h2 + 1 -> CQuasiCompletNonParfait
      | CParfait, CQuasiCompletNonParfait when h1 = h2 -> CQuasiCompletNonParfait
      | CQuasiCompletNonParfait, CParfait when h1 = h2 + 1 -> CQuasiCompletNonParfait
      | _, _ -> CNonQuasiComplet
(*      | CParfait, CParfait -> CNonQuasiComplet
      | CParfait, CQuasiCompletNonParfait -> CNonQuasiComplet
      | CQuasiCompletNonParfait, CParfait -> CNonQuasiComplet
      | CQuasiCompletNonParfait, CQuasiCompletNonParfait -> CNonQuasiComplet
      | CNonQuasiComplet, _ -> CNonQuasiComplet
      | _, CNonQuasiComplet -> CNonQuasiComplet
*)
    in h, c
;;
