\titrepartie{Une brève histoire d'OCaml}

À la fin du XIX\up{e} siècle et le début du XX\up{e}, les
mathématiciens se sont mis en quête des fondements des
mathématiques. La plupart des mathématiciens considèrent généralement
que cette quête commence avec l'introduction par Cantor de la notion
d'ensemble, se développe avec les défis du programme de Hilbert, et
s'arrête brutalement avec les réponses inattendues données par Gödel.

Mais cette quête a conduit certains mathématiciens à s'intéresser à
une notion: celle d'algorithme et de fonction calculable par un
algorithme (on peut citer notamment Haskell Curry, inventeur de la
logique combinatoire, Alonzo Church, inventeur du $\lambda$-calcul, et
Alan Turing, inventeur de la machine qui porte son nom).

Grosso modo, on peut dire qu'un ordinateur actuel est la traduction
Un ordinateur peut être vu comme la réalisation concrète des idées
d'une machine de Turing.

Les langages de programmation ont énormément évolué depuis les
premiers ordinateurs des années 40. Donnons quelques jalons:
\begin{description}
\item[1940's] On programme directement en binaire
\item[1950's] On programme en assembleur (représentation symbolique du
  code binaire)
\item[1954] invention de FORTRAN (premier compilateur en 1957); encore
  utilisé aujourd'hui par les physiciens.
\item[1958] invention de LISP (premier compilateur en 1962); largement
  influencé par le $\lambda$-calcul, LISP est le langage de
  l'intelligence artificielle, et est encore à ce jour un langage bien
  vivant avec beaucoup de descendants.
\item[1973] invention de ML (Robin Milner) au départ comme langage
  spécialisé pour construire automatiquement des démonstrations;
  inspiré par LISP et le $\lambda$-calcul;
\item[1985] invention de CAML (fondé sur la CAM inventée par Guy
  Cousineau à partir des travaux de Pierre-Louis Curien sur les
  catégories), au sein du projet INRIA Formel, dirigé par Gérard Huet;
  première implantation en 1987 (par Ascander Suarez).
\item[1990] Caml-Light: nouvelle implantation de Caml par Xavier
  Leroy (GC par Damien Doligez);
\item[1995] Caml Special Light: implantation plus ambitieuse par Xavier
  Leroy, apportant un puissant système de modules paramétrés.
\item[1996] Évolution de Caml Special Light en Objective OCaml (à
  partir des travaux de Didier Rémy puis Jérôme Vouillon).
\item[2000] Extension avec les fonctions à arguments nommés, à
  arguments optionnels, les méthodes polymorphes et les variantes
  polymorphes (à partir des travaux de Jacques Garrigue).
\item[2011] Objective Caml est renommé en OCaml.
\end{description}

Cette évolution dans les langages de programmation est motivée par des
critères esthétiques car il est plus agréable d'utiliser un langage
élégant. Et choisir un langage esthétique peut avoir de fortes
répercussions économiques: la productivité d'un programmeur, mesurée
en nombre de lignes de code écrites par jour, dépend assez peu du
langage employé. En revanche, le nombre de lignes de codes nécessaires
pour écrire un même programme peut varier fortement en fonction du
langage employé. Ainsi un article de 1994 de Paul Hudak et Mark Jones,
intitulé \emph{Haskell vs. Ada vs. C++ vs Awk vs. \ldots An Experiment
  in Software Prototyping Productivity} a montré que sur un petit
exemple, la longueur d'un programme réalisant une tâche donnée pouvait
varier d'un facteur 13 et la durée de sa réalisation d'un facteur 18
selon les langages utilisés.

Or les langages fondés sur le $\lambda$-calcul, appelés
\emph{langages fonctionnels}, ont une grande puissance d'expression et
permettent donc d'écrire des programmes de façon concise. 

De plus, il est relativement plus facile de raisonner sur des
programmes fonctionnels que sur des programmes impératifs
classiques. Cela permet d'avoir des programmes corrects à moindre
coût.

OCaml est relativement peu connu du grand public mais est utilisé dans
de nombreux domaines et très apprécié de ceux qui
l'utilisent.\footnote{On peut trouver à l'adresse
  \url{https://ocaml.org/learn/companies.html} une liste d'entreprises
  utilisant OCaml.}

\titrepartie{Caractéristiques d'OCaml}

\sousparties
\titrepartie{Interactif ou compilé}
OCaml peut être utilisé de façon interactive: de même qu'on peut en
Python, exécuter des instructions au fur et à mesure qu'on les tape
dans une console Python, OCaml peut être utilisé via un \emph{système
  interactif} (appelé \emph{toplevel}, voire improprement appelé
\emph{interpréteur interactif}). Un programme OCaml peut aussi être
compilé vers en un programme binaire indépendant (qu'on peut faire
tourner sur un ordinateur ou OCaml n'est pas installé).

\titrepartie{Langage fonctionnel}
OCaml est un langage \emph{fonctionnel}, ou \emph{applicatif}.

Dans beaucoup de langages, dits «impératifs», un programme est une
suite d'instructions. Dans ces langages, on distingue les instructions
des expressions. Par exemple, en Python, «\texttt{x+3*y}» est une
expression, tandis que «\texttt{y = 2 * y + 1}» est une
instruction\footnote{C'est une affectation, dont le membre droit est
  une expression.}

Dans les langages fonctionnels, l'accent est mis sur la notion
d'expression plutôt que d'instruction: en OCaml, cette notion
d'instruction n'existe pas vraiment, on ne travaille qu'avec des
expressions.

\titrepartie{Langage typé statiquement}

En Python, les expressions ont des types (les entiers, les booléens,
les chaînes de caractères, etc.), qui sont déterminés automatiquement
à l'exécution du programme. Python peut, lors de l'exécution d'une
fonction se rendre compte d'un problème de typage, par exemple se
rendre compte qu'on est en train de tenter d'additionner un entier et
une chaîne de caractères, auquel cas, il va déclencher une erreur. On
dit que Python est \emph{typé dynamiquement}.

En OCaml, les expressions ont également des types mais ceux-ci sont
déterminés à la compilation du programme, avant même que la moindre
exécution ait lieu.
\finsousparties

\titrepartie{Premiers pas en OCaml}
\sousparties
\titrepartie{Les types de base}

Démo:
\begin{itemize}
\item Entiers (constantes, les quatre opérations)
\item Flottants (constantes, les quatre opérations, la puissance, les fonctions
  trigo, etc.)
\item Booléens (constantes, conjonction, disjonction, négation, comparaison)
\item Chaînes de caractères (constantes, concaténation).
\end{itemize}

\begin{rmq}
  On note \texttt{f x} l'application d'une fonction \texttt{f} à un
  argument \texttt{x}, de même qu'en mathématiques on note $\sin x$
  l'application de la fonction sinus à $x$. Les parenthèses autour de
  l'argument ne sont utilisées que pour lever une ambiguïté: de même
  qu'on écrira $\sin (x+y)$, on notera \texttt{f (x + y)}.
\end{rmq}


\titrepartie{Parenthésage}

Comme en mathématiques, on peut parenthéser les expressions pour
forcer la priorité d'un opérateur sur un autre.

En l'absence de parenthèses, l'application des opérateurs usuels suit
les règles usuelles de priorité.

Entre deux opérateurs de même priorité, tout se passe comme
si on avait parenthésé à gauche (ainsi \texttt{x - y + z} signifie la
même chose que \texttt{(x - y) + z}.

La remarque suivante est à \textbf{connaître par cœur}:
\begin{rmq}[Priorité de l'application]
  L'application est prioritaire sur les opérateurs infixes.
\end{rmq}
Ainsi \texttt{f x + y} signifie la même chose que \texttt{(f x) + y}
et non \texttt{f (x+y)}.

\titrepartie{Variables globales}

Exemple:
\begin{Verbatim}
# let x = 1 + 2;;
val x : int = 3
\end{Verbatim}

Lorsqu'il lit une phrase \texttt{let $x$ = $e$;;} où $x$ est un nom de variable
et $e$ une expression, OCaml:
\begin{enumerate}
\item évalue l'expression $e$, obtenant ainsi une valeur $v$;
\item ajoute dans l'environnement des variables l'association entre
  $x$ et $v$.
\end{enumerate}

\titrepartie{Expressions fonctionnelles}

Dans de nombreux langages, les fonctions n'ont pas le même statut que
les autres 
En OCaml, les fonctions ont un statut de première classe, c'est-à-dire
qu'elles ont le même statut que les autres objets: il n'y a pas d'un
côté les expressions et les valeurs entières, flottantes, les
booléens, les chaînes de caractères, etc.\ et d'un autre les
fonctions.

Les expressions fonctionnelles sont des expressions comme les autres,
et ont une valeur comme toute expression.

Les valeurs fonctionnelles sont de la forme
\texttt{fun $v$ -> $e$} où $v$ est le nom d'une variable et $e$ est
une expression.

Exemple:
\begin{Verbatim}
# fun x -> x + x + x;;
- : int -> int = <fun>
# fun x -> x ^ x ^ x;;
- : string -> string = <fun>
\end{Verbatim}

Remarquons:
\begin{itemize}
\item L'évaluation d'une fonction donne une valeur de type
  fonctionnel, i.e.\ une fonction (ici de type
  \texttt{int -> int} ou \texttt{string -> string}) que la boucle d'interaction
  OCaml ne sait pas afficher (elle se contente d'afficher \verb+<fun>+).
\item OCaml devine tout seul le type de la fonction sans même
  l'exécuter (d'ailleurs on ne lui a pas dit sur quel argument l'exécuter).
\end{itemize}

\begin{exo}
  Que se passe-t-il à votre avis si on donne à OCaml la phrase
  suivante?
  \begin{Verbatim}
  fun x -> x + 3.14;;
  \end{Verbatim}
\end{exo}

À quoi servent les expressions fonctionnelles? Ici, à peu de choses
tant qu'on n'applique pas la fonction à un argument.
De manière générale, l'application d'une expression fonctionnelle
$e_1$ à un argument $e_2$ s'écrit tout simplement $e_1\ e_2$.
Ici, cela donne par exemple:

\begin{Verbatim}
#( fun x -> x ^ x ^ x) "bla";;
- : string = "blablabla"
\end{Verbatim}

Remarquez que ça peut être intéressant si on veut appliquer la
fonction à un mot assez long, par exemple, pour cette célèbre réplique
de Bernard Blier dans \textit{Le grand blond avec une chaussure
  noire}, on peut économiser 23 caractères\footnote{Ça n'a aucun
  intérêt, c'est juste de la fénéantise.}:
\small
\begin{Verbatim}
# (fun x -> x ^ x ^ x) "On tourne en rond, m ! ";;
- : string = "On tourne en rond, m ! On tourne en rond, m ! On tourne en rond, m ! "
\end{Verbatim}
\normalsize

Cela dit, la notion de fonction est surtout intéressante lorsqu'on la
combine avec la notion de variable globale:
\begin{Verbatim}
# let triple = fun x -> x ^ x ^ x;;
val triple : string -> string = <fun>
#triple "bla";;
- : string = "blablabla"
#let bb = triple "On tourne en rond, m ! ";;
val bb : string =
 "On tourne en rond, m ! On tourne en rond, m ! On tourne en rond, m ! "
\end{Verbatim}

NB: \texttt{triple} est une expression fonctionnelle, dont la valeur (fonctionnelle)
est \verb+fun x -> x ^ x ^ x+

\titrepartie{Fonctions à plusieurs variables}

Comment faire pour écrire une fonction à plusieurs variables?
En fait le mot-clé \texttt{fun} introduit une expression qui peut être
plus complexe que l'expression \texttt{fun $v$ -> $e$}: on peut en
fait écrire \texttt{fun $p_1$ \ldots $p_n$ -> $e$} où $e$ est une
expression et $p_1$, \ldots, $p_n$ sont des \emph{motifs
  irréfutables}. Les noms de variables sont des motifs irréfutables
(et pour l'instant, seuls les noms de variables sont des motifs irréfutables).

Exemple:
\begin{Verbatim}
# fun x y -> 2*x + 3*y;;
- : int -> int -> int = <fun>
\end{Verbatim}

\begin{exo}
  Quel est le type inféré par OCaml pour l'expression suivante?
\begin{Verbatim}
fun x y -> float_of_int x +. 3.15*y
\end{Verbatim}
(\verb+float_of_int+ est une fonction de type \verb+int -> float+, qui
associe à tout entier $n$ le flottant représentant $n$)
\end{exo}

\titrepartie{Expressions conditionnelles}

Une expression conditionnelle est de la forme
\texttt{if $e_1$ then $e_2$ else $e_3$}, où $e_1$ est une expression
booléenne. Elle évalue $e_1$, qui doit être une expression
booléenne. Si celle-ci vaut \texttt{true}, l'expression $e_2$ est
évaluée et sa valeur est retournée. Sinon, c'est l'expression $e_3$
qui est évaluée et dont la valeur est retournée.

Exemple d'utilisation:
\begin{Verbatim}
(* Retourne le nombre de jours de l'année annee dans le calendrier
   julien *)
let nb_jours annee =
  if annee mod 4 = 0 then 365 else 366
;;
\end{Verbatim}

\begin{exo}
  Le code suivant est-il accepté par OCaml?
  \begin{Verbatim}
    let nb_jours annee =
      365 + (if annee mod 4 = 0 then 0 else 1);;
  \end{Verbatim}
\end{exo}

% \titrepartie{Fonctionnelles et fonctions à plusieurs variables}
% Exemple avec la suite des $(x\mapsto e^{-nx}+x)$, pour $n\in\N$.

\titrepartie{Produit cartésien}

\begin{Verbatim}
#( 3, "toto");;
- : int * string = 3, "toto"
# ((3, "titi"), "toto");;
- : (int * string) * string = (3, "titi"), "toto"
# (3, "titi", "toto");;
- : int * string * string = 3, "titi", "toto"
\end{Verbatim}

\begin{exo}
  Que répond OCaml si on lui demande d'évaluer:
  \texttt{((3, "titi"), "toto") = (3, "titi", "toto")}?
\end{exo}

Motifs irréfutables liés au produit cartésien: si $p_1$, $p_2$,
\ldots, $p_{n-1}$ et $p_n$ sont des motifs irréfutables, alors $(p_1,
\ldots, p_n)$ est un motif irréfutable. Exemple d'utilisation:
\begin{Verbatim}
# fun (x, y) (z, t, u) -> x - 3*t, y + 2*z + t - u;;
- : int * int -> int * int * int -> int * int = <fun>
\end{Verbatim}

Il y a donc deux moyens d'écrire une façon à deux variables, par
exemple, celle qui à $x$ et $y$ associe $x - 3*y$:
\begin{itemize}
\item Celui qu'on a vu plus haut, qu'on appelle la version
  \emph{curryfiée} de la fonction (en hommage à Haskell Curry):
\begin{Verbatim}
fun x y -> x - 3*y
\end{Verbatim}
\item Celui qu'on vient de voir, en utilisant un motif irréfutable de
  couple:
\begin{Verbatim}
fun (x, y) -> x - 3*y
\end{Verbatim}
\end{itemize}
Sauf très bonne raison de faire autrement, on préférera
systématiquement la version curryfiée.

\titrepartie{Les fonctions récursives}
\sousparties
\titrepartie{Recherche par dichotomie}
On veut écrire une fonction \texttt{zero\_sin : float -> float -> float ->
  float} telle que, sous la précondition $a<b$ et $\sin a$ et $\sin b$
de signes opposés, \texttt{zero\_sin a b e} retourne une approximation
d'un zéro de la fonction sinus compris entre $a$ et $b$,
l'approximation devant être correcte à une erreur $e$ près.

\begin{Verbatim}
let rec zero_sin a b e =
  let m = 0.5 *. (a +. b) in
  if b -. a < 2. *. e then m
  else if sin m *. sin a <= 0. then zero_sin a m e
  else zero_sin m b e
;;  
\end{Verbatim}
L'exécution de \texttt{zero\_sin 3. 4. 0.0001} donne la valeur
\texttt{3.141540\ldots}.

Que s'est-il passé ?
Deux points de vue:
\begin{enumerate}
\item conceptuel (expansion de l'expression);
\item opérationnel (notion de pile).
\end{enumerate}

Question de la terminaison.

\titrepartie{Méthode de la sécante}
On cherche encore un zéro de la fonction sinus, mais cette fois-ci en
utilisant une autre méthode, appelée méthode de la sécante. Celle-ci
consiste, pour chercher un zéro d'une fonction $f$, à calculer à
une suite $(u_n)$ d'approximations définie par:
\begin{equation*}
  \left\{
    \begin{aligned}
      u_0 &= \alpha\\
      u_1 &= \beta\\
      \forall n \geq 2\quad u_{n} & = \frac{u_{n-2}f(u_{n-1})-u_{n-1}f(u_{n-2})}{f(u_{n-1})-f(u_{n-2})}
    \end{aligned}
  \right.
\end{equation*}
où $\alpha$ et $\beta$ sont des approximations initiales du zéro
considéré. Sous certaines conditions sur $f$ et sur ces approximations
initiales, la suite $u$ converge très rapidement vers ce zéro de $f$.

\begin{exo}
  Pourquoi cette méthode porte-t-elle le nom de méthode de la sécante?
  À quoi correspond la formule de calcul de $u_n$?
\end{exo}

On veut définir une fonction \texttt{zero2 : float -> float -> int ->
  float} telle que \texttt{zero2 $\alpha$ $\beta$ n} retourne la
valeur de $u_n$ associée à la fonction sinus et les approximations
initiales $\alpha$ et $\beta$.
\begin{Verbatim}
let rec zero2 a b n =
  if n = 0 then a
  else if n = 1 then b
  else
    let un1 = zero2 a b (n-1) in
    let un2 = zero2 a b (n-2) in
    let fun1 = sin un1 in
    let fun2 = sin un2 in
    (un2*.fun1 -. un1*.fun2) /. (fun1 -. fun2)
;;
\end{Verbatim}

Autre version:

\begin{Verbatim}
let rec zero3 a b n =
  if n = 0 then a
  else if n = 1 then b
  else
    let fa = sin a in
    let fb = sin b in
    let c = (b*.fa -. a*.fb) /. (fa -. fb) in
    zero3 b c (n-1)
;;
\end{Verbatim}

\titrepartie{Les fonctions sont de première classe}

En OCaml les fonctions sont de première classe, ce qui veut dire
qu'une fonction peut prendre en argument une autre fonction:
\begin{Verbatim}
let rec secante f a b n =
  if n = 0 then a
  else if n = 1 then b
  else
    let fa = f a in
    let fb = f b in
    let c = (b*.fa -. a*.fb) /. (fa -. fb) in
    secante f b c (n-1)
;;
\end{Verbatim}

\titrepartie{Filtrage}

Dans la fonction précédent, on distingue trois cas, pour lesquels on
calcule de trois façons différentes la valeur à rendre. Un point
intéressant est que les tests effectués pour distinguer ces trois
cas portent tous les deux sur la valeur de $n$.
Il existe en OCaml une construction très puissante permettant de
calculer des résultats différents en fonction d'une valeur donnée: le
\emph{filtrage}. Dans le cas très simple de la fonction précédente,
elle prend la forme suivante:
\begin{Verbatim}
let rec secante f a b n =
  match n with
  | 0 -> a
  | 1 -> b
  | _ -> let fa = f a in
         let fb = f b in
         let c = (b*.fa -. a*.fb) /. (fa -. fb) in
         secante f b c (n-1)
;;
\end{Verbatim}
$n$ est la valeur à filtrer. Chaque
cas de filtrage est formé d'un \emph{motif}, suivi d'une flèche, suivi d'une
expression. On a ici trois cas de filtrage. On considère leurs motifs
un par un, jusqu'à en trouver un qui \emph{filtre} la valeur $n$. On
exécute alors l'expression à droite de la flèche correspondante et on
renvoie sa valeur.

Le premier cas de filtrage a pour motif \texttt{0}. Si \texttt{n} vaut
$0$, on dit que ce motif filtre $n$, auquel cas on renvoie la valeur
de \texttt{a}.

Le second cas a pour motif \texttt{1}. Si \texttt{n} vaut $1$, on dit
que ce motif filtre $n$, auquel cas on renvoie la valeur de \texttt{b}.

Le troisième cas a pour motif \texttt{\_}. Ce motif filtre toute
valeur. Si $n$ n'a été pas été filtré par un des deux motifs
précédents, il est filtré par celui-ci. On exécute alors l'expression
à droite de la flèche, et on renvoie la valeur ainsi calculée.

\begin{exo}[Factorielle]
  Écrire une fonction \texttt{fact : int -> int} calculant la
  factorielle de son argument.

  Que donne \texttt{fact 64} ? Expliquer.
\end{exo}

\begin{exo}
  Écrire une fonction \texttt{fib : int -> int} telle que \texttt{fib
    n} calcule $F_n$ où $(F_n)$ est la suite de Fibonacci. On rappelle
  qu'on a:
  \begin{equation*}
    \left\{
      \begin{aligned}
        F_0 &= 0\\
        F_1 &= 1\\
        \forall n\in \N \quad F_{n+2} = F_{n+1} + F_{n}
      \end{aligned}
    \right.
  \end{equation*}
  Combien votre fonction met-elle de temps pour calculer $F_{30}$?
  $F_{40}$? $F_{50}$?

  Pouvez-vous l'écrire de telle façon qu'elle calcule instantanément
  $F_{80}$?
\end{exo}
% Démonstration de la correction.

% Calcul de la complexité temporelle.

% Calcul de la complexité spatiale (espace de pile).

% Autre exemple: suite de syracuse. Calculer le $n$-ème terme de cette
% suite $s$. Calculer $n$ tel que $s_n = 1$:
% \begin{Verbatim}
% (* suite de syracuse *)
% let suivant n = if  n mod 2 = 0 then n/2 else 3*n+1;;

% (* valeur du nieme terme de la suite de Syracuse commençant avec a : *)
% let rec syracuse n a = if n = 0 then a else syracuse (n-1) (suivant a);;
% \end{Verbatim}

% Calcul de la complexité temporelle.

% Calcul de la complexité spatiale: notion de fonction récursive
% terminale. (Voir les deux versions : récursive terminale et non
% récursive terminale.)

\finsousparties

% \titrepartie{Comment se passer des boucles}

% Opérateur d'itération défini (sur des exemples d'abord). Utilisation pour calculer le $n$-eme
% terme d'une suite $u$ définie par $u_{n+1} = f(u_n)$.

% % \begin{Verbatim}
% % let rec iter f n a = if n=0 then a else iter f (n-1) (f a);;

% % (* nouvelle version de syracuse : *)
% % let syracuse n a = iter suivant n a;;

% % iter sin 10 2.;;

% % iter (fun s -> s ^ " beaucoup") 100 1.;;
% % \end{Verbatim}

% Opérateur indéfini: Trouver le nombre d'itérations nécessaires pour
% obtenir 1 avec la suite de Syracuse. Faire écrire en Python.

% \begin{Verbatim}
% let rec nb_it_syracuse a = if a=1 then 0 else 1 + nb_it_syracuse (suivant a);;
% \end{Verbatim}

% Méthode de Newton pour le calcul de $\sqrt{a}$ ($u_{n+1} =
% \frac{1}{2}\p{u_n+\frac{a}{u_n}}$). Faire écrire en Python.


% TODO : test (naïf) de primalité ?

\finsousparties