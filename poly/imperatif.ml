let rec filter p l = match l with
  | [] -> []
  | a::r -> 
    if p a then a::filter p r
    else filter p r
;;

let f l =
  let y = filter (fun x -> x < 42) l in
  42
;;

let rec partition x l =
  let u = List.filter (fun y -> y <= x) l in
  let v = List.filter (fun y -> y > x) l in
  u, v
;;

let rec quicksort l = match l with
  | [] -> []
  | x::r -> let u1, u2 = partition x r in
	    let v1 = quicksort u1 in
	    let v2 = quicksort u2 in
	    v1 @ (x::v2)
;;

let rec range n = match n with
  | 0 -> []
  | _ -> n::range (n-1)
;;

let rec list n = match n with
  | 0 -> []
  | _ -> Random.int 1000000 :: list (n-1)
;;

let l = range 100000;;

let r = List.rev l;;

let f x =
  let immininent_danger = x in
  if immininent_danger then print_string "R2, why did you have to be so brave?"
;;

(if 0 = 0 then 42 else 17) + 3;;

(if 0 = 0 then 42 else ()) + 3;;

let x = Array.make 17 3;;

x.(4);;

x.(4) <- 42;;

for i=0 to 5 do
  print_string "toto\n"
done;;

let x = ref 0;;

(* split : 'a list -> 'a list * 'a list *)
(* split l retourne un couple l1, l2 de listes contenant les mêmes
   éléments que l, telles que l1 et l2 soient de même longueur ou que
   celle de l1 soit celle de l2 plus un *)
let rec split l = match l with
  | [] -> [], []
  | [x] -> [x], []
  | x::y::r -> let l1, l2 = split r in
	    x::l1, y::l2
;;

(* split : 'a array -> 'a array * 'a array
   Précondition : le tableau est de taille non nulle
*)
let split a =
  let n = Array.length a in
  let n2 = n / 2 in
  let a1 = Array.make n2 a.(0) in
  let a2 = Array.make (n-n2) a.(0) in
  for i = 0 to n2-1 do a1.(i) <- a.(i) done;
  for i = 0 to n-n2-1 do a2.(i) <- a.(n2+i) done;
  a1, a2
;;

(* merge : 'a list -> 'a list -> 'a list *)
(* merge l1 l2 fusionne les deux listes supposées triées l1 et l2 en
   une unique liste *)
let rec merge l1 l2 = match l1, l2 with
  | [], l | l, [] -> l
  | x1::r1, x2::r2 ->
    if x1 <= x2 then x1::merge r1 l2
    else x2::merge l1 r2
;;

(* on suppose que ni a1 ni a2 n'est vide *)
let merge a1 a2 =
  let n1 = Array.length a1 in
  let n2 = Array.length a2 in
  let n = n1 + n2 in
  let a = Array.make n a1.(0) in
  let i1 = ref 0 in
  let i2 = ref 0 in
  for i = 0 to n-1 do
    (* les i plus petits éléments de a1 et a2 sont dans a, triés
       et ce sont les !i1 premiers éléments de a1 et les !i2 premiers éléments de a2.
    *)
    if !i2 >= n2 || (!i1 < n1 && a1.(!i1) <= a2.(!i2))
    then
      let () = a.(i) <- a1.(!i1) in
      i1 := !i1 + 1
    else
      let () = a.(i) <- a2.(!i2) in
      i2 := !i2 + 1
  done;
  a
;;

let rec tri a =
  if Array.length a <= 1 then a
  else
    let a1, a2 = split a in
    merge (tri a1) (tri a2)
;;

(* tri : 'a list -> 'a list *)
(* tri l retourne une liste triée contenant les mêmes éléments que l *)
let rec tri l = match l with
  | [] | [_] -> l
  | _ -> let l1, l2 = split l in
	 merge (tri l1) (tri l2)
;;
