let nmax = 1000;;
let t = Array.make (nmax+1) (-1);;

let rec fib n = match n with
  | 0 -> 0
  | 1 -> 1
  | _ -> fib_memo (n-1) + fib_memo (n-2)

and fib_memo n =
  if t.(n) <> -1 then t.(n)
  else
    let r = fib n in
    let () = t.(n) <- r in
    r
;;
