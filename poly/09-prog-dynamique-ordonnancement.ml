(*
\titrepartie{Le problème}

On considère des tâches à effectuer.
Chaque tâche possède:
\begin{enumerate}
\item Une étendue dans le temps représentée sous la forme d'un
  intervalle $[s, f[$ où $s$ et $f$ sont respectivement la date de
  début et la date de fin de la tâche. Cet intervalle sera toujours
  supposé non vide ($s<f$).
\item Un poids.
\end{enumerate}

*)

type date = float;;
type task = { start : date; finish : date; weight : float; };;

(* Par la suite, étant donnée une tâche $t$, on notera $w(t)$, $s(t)$ et $f(t)$
respectivement le poids de cette tâche, sa date de début et sa date de fin.
*)
let w t = t.weight;;
let s t = t.start;;
let f t = t.finish;;

(* Quelques outils de conversion utiles *)

let s_float = string_of_float;;
let s_int = string_of_int;;
let s_bool = string_of_bool;;
let s_task t =
  let s = s_float t.start in
  let f = s_float t.finish in
  let w = s_float t.weight in
  "{ start = "^s^"; finish = "^f^"; weight = "^w^"; }"
;;

(*
On dit que deux tâches $t_{1}$ et $t_{2}$ sont compatibles si les
intervalles $[s_{1}, f_{1}[$ et $[s_{2}, f_{2}[$ sont disjoints:
*)

let compatibles t1 t2 =
  t1.finish <= t2.start || t2.finish <= t1.start
;;

(* par la suite on notera $C(t_{1},t_{2})$ le fait que ces deux tâches
soient compatibles.

Étant donné un ensemble $E$ de tâches, on notera $W(E)$ le poids
total de ses éléments:
\begin{equation}
  W(E) = \sum_{t\in E}w(t)
\end{equation}

Étant donné un ensemble fini de tâches $T$ possédant $n$ éléments
$t_{0}$, \ldots{}, $t_{n-1}$,
le problème est de trouver le poids maximum que peut avoir un
sous-ensemble $E$ de tâches de $T$ deux à deux compatibles ainsi que
d'exhiber un tel sous-ensemble réalisant ce maximum.
*)

type taskset = task list;;

(* [s_list s l] convertit la liste [l : 'a list] en chaîne en
   utilisant la fonction [s : 'a -> string] pour convertir les
   éléments de [l]
*)

let s_list s l =
  let sl = List.map s l in
  "[ " ^ String.concat "; " sl ^ "]"
;;
let s_vect s v =
  let sv = Array.map s v in
  "[| " ^ String.concat "; " (Array.to_list sv) ^ "|]"
;;

(* Compare les dates de fin de [t1] et [t2] selon leur date de fin.
*)
let leq_task t1 t2 = compare t1.finish t2.finish;;

(* on affiche les listes de tâches triées par ordre croissant. pour cet ordre 
  de date de fin *)
let s_taskset ts = s_list s_task (List.sort leq_task ts);;

let emptyset = [];;

let rec poids_total ts = match ts with
  | [] -> 0.
  | x::r -> x +. poids_total r
;;

(*
\titrepartie{Jeu de tests}
*)

let t1 = { start = 0. ; finish = 10.; weight = 100.; };;
let t2 = { start = 1. ; finish = 2.; weight = 40.; };;
let t3 = { start = 2. ; finish = 3.; weight = 40.; };;
let t4 = { start = 2.5 ; finish = 4.5; weight = 50.; };;
let t5 = { start = 3.2 ; finish = 5.; weight = 40.; };;

(* un ensemble de tâches : *)
let j1 = [ t5; t4; t3; t1; t2 ];;

let j1_sorted = List.sort leq_task j1;;
let j1_vect = Array.of_list j1_sorted;;

(* poids maximum pour cet ensemble de tâches *)
let pm1 = 120.;;

(* solution correspondante *)
let s1 = [ t5; t3; t2 ];;

let assertEq msg v exp =
  if v <> exp
  then
    let s = "Echec : " ^ msg
      ^ " Valeur calculée :" ^ v 
      ^ " Valeur attendue : " ^ exp in
    failwith s
;;

let assertEqConv f msg x1 x2 = assertEq msg (f x1) (f x2);;
let assertEqFloat = assertEqConv s_float;;
let assertEqInt = assertEqConv s_int;;
let assertEqBool = assertEqConv s_bool;;
let assertEqTaskset = assertEqConv s_taskset;;

let test_compatible () =
  begin
    assertEqBool "C(t1, t2)" (compatibles t1 t2) false;
    assertEqBool "C(t2, t3)" (compatibles t2 t3) true;
    assertEqBool "C(t3, t4)" (compatibles t3 t4) false;
    assertEqBool "C(t3, t5)" (compatibles t3 t5) true;
  end
;;
(*
\titrepartie{Algorithme par recherche exhaustive}
\sousparties
\titrepartie{Version initiale}
On peut procéder comme suit:

\begin{enumerate}
\item On calcule l'ensemble de toutes les parties de $T$.
\item On garde uniquement celles qui sont constituées de tâches deux à
  deux compatibles.
\item On calcule le poids total de chacune.
\item Il ne reste plus qu'à garder celle de poids maximal.
\end{enumerate}

Il s'agit d'un algorithme par recherche exhaustive, aussi qualifié de
\textit{brute-force} en anglais, pour la raison suivante : 
L'ensemble des parties de $T$ possède $2^{n}$ éléments.

Il   va donc   falloir   un  espace   mémoire
$\Omega(2^{n})$ pour les représenter\footnote{À moins d'avoir une idée
  de représentation de cet ensemble particulièrement astucieuse.} donc
un temps $\Omega(2^{n})$.

\titrepartie{Une petite amélioration}
On peut améliorer un peu les choses en calculant directement
l'ensemble des parties de $T$ ne contenant que des tâches deux à deux
compatibles.

Pour $i=0, \ldots, n$, on pose
$T_{i} = \Set{t_{k}| 0\leq k < i}$ ($T_{0}=\emptyset$ et
$T_{n}=T$). On note alors $E_{i}$ l'ensemble des parties de $T_{i}$ ne
contenant que des tâches deux à deux compatibles et $M_{i}$ le poids
maximum d'un ensemble de tâches appartenant à $E_{i}$:
\begin{equation}
  M_{i} = \max_{S \in E_{i}} W(S)
\end{equation}
$E_i$ est donc un ensemble d'ensembles de tâches ($E_i \in \cP(\cP(T_i))$)
(on a $E_0=\Set{\emptyset}$ et $M_0=0$).

Pour tout $i\in[0,n]$, on notera $S_{i}$ un élément de $E_{i}$ tel que
$W(S_{i})=M_{i}$.

Alors, pour $i\in[0,n[$, on a 
$E_{i+1} = E_{i}\cup F_{i}$, où $F_{i}$ est l'ensemble des $\cT
\cup\Set{t_{i}}$ pour tout $\cT \in E_{i}$ tel que $t_{i}$ est
compatible avec tous les éléments de $\cT$:
\begin{equation}\label{eq:Fi-naif}
  F_{i} = \Set{ \cT \cup \Set{t_{i}} \in E_{i}|\cT\in E_i \ \forall  t\in \cT\ C(t_{i},t)}
\end{equation}

Cependant cette amélioration ne change pas la borne inférieure théorique dans le cas
le pire: on reste avec $2^{n}$ parties par exemple dans le cas où
toutes les tâches sont deux à deux compatibles.

De manière générale, dans tous les cas s'il y a au moins $k$ tâches
deux à deux compatibles, on a encore au moins $2^{k}$ ensembles à
construire.

\finsousparties
\titrepartie{Une remarque géniale}

Le calcul de $F_{i}$ tel que donné par l'équation~(\ref{eq:Fi-naif})
est un peu pénible à effectuer. La raison en est qu'il faut chercher
dans $E_i$ toutes les tâches compatibles avec $t_{i}$.
Mais cela devient bien plus facile si les tâches  $t_{0}$,
\ldots{}, $t_{n-1}$ sont ordonnées par date de fin croissantes. 

Supposons que ce soit le cas et notons $d_{0}$, \ldots{}, $d_{n-1}$ et
$f_{0}$, \ldots{},
$f_{n-1}$ les dates respectives de début et de fin des tâches $t_{0}$,
\ldots{}, $t_{n-1}$. Soit alors $i\in [0,n[$. L'ensemble des tâches
$t_{k}$ incompatibles avec $t_{i}$ avec $k\leq i$ est l'ensemble des
tâches vérifiant $f_{k}> d_{i}$. En notant $p(i)$ le plus petit entier de
$[0, i]$ tel que $f_{p(i)}>d_{i}$, les tâches $t_{k}$ compatibles avec
$t_i$, pour $k\leq i$, sont exactement les $t_{k}$ pour $k<p(i)$
(on a $p(i) = i$ si les tâches
$t_{k}$ pour $k<i$ sont toutes compatibles avec $t_{i}$).

Remarquons que le calcul de $p(i)$ se fait très facilement:
*)

(*
[p t i] retourne le plus petit entier $j$ tel que $t_j$ et $t_i$
soient incompatibles. [t] est supposé trié par ordre croissant de dates de fin.
*)
let p t i =
  let j = ref 0 in
  while compatibles t.(i) t.(!j) do incr j done;
  !j
;;

assertEqBool "j1_vect.(1) et ..(2) ne devraient pas être compatibles"
	       (compatibles j1_vect.(1) j1_vect.(2)) false;;

let test_p () =
  let t = j1_vect in
  begin
    assertEqInt "p(0)" (p t 0) 0;
    assertEqInt "p(1)" (p t 1) 1;
    assertEqInt "p(2)" (p t 2) 1;
    assertEqInt "p(3)" (p t 3) 2;
    assertEqInt "p(4)" (p t 4) 0;
  end
;;

(* Cet algorithme est en $O(i)$. Ce qui veut dire qu'on peut calculer
   les $p_{i}$ pour toutes les valeurs de $i$ dans $[0,n]$ en
   $O(n^{2})$.

   Mais on peut en fait faire mieux: en triant les tâches par début de
   tâche croissant, on peut ensuite calculer le tableau des valeurs de $p$ en
   $O(n)$.
*)

let tabule_p t =
  let n = Array.length t in
  (* la liste des tâches représentées par leur indice dans t *)
  let ts = Array.to_list (Array.init n (fun i -> i)) in
  (* on les trie par ordre croissant de date de début *)
  let v = Array.of_list
    (List.sort (fun i j -> compare t.(i).start t.(j).start) ts) in
  let j = ref 0 in
  let p = Array.make n 0 in
  for i = 0 to n-1 do
    while compatibles t.(v.(i)) t.(!j) do incr j done;
    p.(v.(i)) <- !j
  done;
  p
;;

let test_tabule_p () =
  let t = j1_vect in
  let p = tabule_p t in
  assertEqConv (s_vect s_int) "Tabulation de p" p [| 0; 1; 1; 2; 0|]
;;

(* Exerice:
   \begin{itemize}
   \item justifier la correction de cet algorithme ;
   \item justifier qu'il s'agit bien d'un algorithme en $O(n)$.
   \end{itemize}
*)

(*
Revenons au calcul de $F_i$. On a vu que les sous-ensembles de
$T_{i+1}$ ne contenant que des tâches compatibles avec $t_{i}$ sont
inclus dans $T_{p(i)}$. Donc on a alors :

\begin{equation}
  \label{eq:Fi-pasnaif}
  F_{i} = \Set{ \cT \cup \Set{t_{i}} \in E_{p(i)} | t\in \cT}
\end{equation}

D'où
\begin{equation}
  \max_{S\in F_{i}} W(S) = w(t_{i}) + \max_{S\in E_{p(i)}}
\end{equation}
et
\begin{align}
  \max_{S\in E_{i+1}} W(S) &= \max(\max_{S\in E_{i}} W(S), \max_{S \in
    F_{i}}W(S))\\
M_{i+1} &= \max (M_{i}, w(t_{i})+M_{p(i)})
\end{align}

Et bien évidemment $M_0 = 0$, d'où le programme très simple suivant:
*)

(* [poids_max : taskset -> float]. *)
let poids_max t =
   let sorted = List.sort leq_task t in
   let v = Array.of_list sorted in
   let p = tabule_p v in
   (* [poids_max_aux : int -> float] calcule $M_i$. *)
   let rec poids_max_aux i =
     if i = 0 then 0.
     else max (poids_max_aux (i-1)) (v.(i-1).weight +. poids_max_aux (p.(i-1)))
   in
   poids_max_aux (List.length t)
;;

(*
Nous avons donc une expression très simple pour le calcul du maximum. Elle
ne calcule que $O(n)$ valeurs. Reste à voir si on peut la calculer en
un temps raisonnable.

Pour l'instant, pour  cette expression naïve, ce n'est  pas le cas. En
effet, si  toutes tâches  sont compatibles, on  a, pour  tout $i\in[0,
n[$,  $p(i) =  i$ et  donc l'appel  à [poids_max_aux  n]  conduit deux
appels  de [poids_max_aux (n-1)]  conduisant chacun  à deux  appels de
[poids_max_aux  (n-2)],  etc.   On  a  donc encore  un  algorithme  en
$\Omega(2^{n})$ dans le cas le pire.

Un petit test sur notre programme :
*)
let test1 () =
  assertEqFloat "Test 1" (poids_max j1) 120.
;;

(*
\titrepartie{Calcul efficace du maximum}

Il suffit  de mémoïser le  calcul de [poids_max_aux] pour  obtenir une
solution  efficace.  En  effet,  on  calcule  [poids_max_aux  i]  pour
$i\in[0,n]$  et ce  calcul, en  dehors des  deux appels  récursifs, ne
demande que le calcul de $i-1$,  une lecture dans le tableau $p$ et un
calcul de [max].  Un calcul par mémoïsation demanderait  donc un temps
$O(n)$.

Attention: cette  complexité est celle dans  le cas où  le tableau des
tâches est déjà  trié. Sinon, il convient de le trier  ce qui, avec un
tri par comparaison\footnote{On
  peut faire mieux si au lieu d'utiliser un tri par comparaison, on
  exploite le fait que le tri porte sur des valeurs numériques, par
  exemple le radix sort ou flashsort.} (non-naïf) demande du $\Omega(n\log n)$.

Dans ce cas très simple, on peut même en fait tabuler les valeurs de
[poids_max_aux]. Nous donnons ici une version de cette fonction de
tabulation prenant en argument un vecteur [v] de tâches supposées
triées par ordre croissant de date de fin ainsi que la fonction [p]
tabulée pour ce vecteur et retournant le tableau [m] des valeurs
de $M_{i}$ pour $i\in[0,n+1[$, où $n$ est la longueur de $t$.
*)

let tabule_poids_max v p =
  let n = Array.length v in
  if n <> Array.length p then invalid_arg "Longueur de v et p différentes";
  let m = Array.make (n+1) 0. in
  for i = 0 to n-1 do
    m.(i+1) <- max m.(i) (v.(i).weight +. m.(p.(i)))
  done;
  m
;;

(*
\titrepartie{Calcul d'une solution}
\sousparties
On peut construire une solution d'au moins deux façons:

\begin{description}
\item[Par calcul direct] En modifiant [tabule_poids_max] de façon à ne
  pas seulement stocker la valeur des maximums calculés dans le tableau mais de
  façon à stocker des couples $(M_i, E_{i})$.
\item[Par reconstruction] En reconstruisant la solution après coup.
\end{description}

\titrepartie{Calcul direct}

Comme précédemment, on suppose ici que le tableau [v] des tâches est
trié par ordre croissant et que la fonction [p] a été tabulée:
*)

let tabule_solution v p =
  let n = Array.length v in
  if n <> Array.length p then invalid_arg "Longueur de v et p différentes";
  let m = Array.make (n+1) 0. in
  let e = Array.make (n+1) emptyset in
  for i = 0 to n-1 do
    if m.(i) <= v.(i).weight +. m.(p.(i)) then begin
      m.(i+1) <- v.(i).weight +. m.(p.(i));
      e.(i+1) <- v.(i) :: e.(p.(i));
    end else begin
      m.(i+1) <- m.(i);
      e.(i+1) <- e.(i)
    end;
  done;
  e
;;

let test2 () =
  let n = Array.length j1_vect in
  let p = tabule_p j1_vect in
  let s = tabule_solution j1_vect p in
  assertEqTaskset "Test 2" s.(n) [t5; t3; t2]
;;

(*
\titrepartie{Reconstruction}

On écrit un algorithme semblable à l'algorithme naïf mais, comme on a
tabulé poids maximums, on n'a besoin que d'un appel récursif à chaque
étape:
*)

let reconstruction_solution t =
  let sorted = List.sort leq_task t in
  let v = Array.of_list sorted in
  let p = tabule_p v in
  let m = tabule_poids_max v p in
  let rec solution i =
    if i = 0 then emptyset
    else
    if m.(i-1) <= v.(i-1).weight +. m.(p.(i-1)) then
      v.(i-1) :: solution (p.(i-1))
    else solution (i-1)
  in
  solution (Array.length v)
;;

(* Intérêt pratique de l'algorithme de reconstruction: l'espace
   mémoire utilisé est plus petit, bien qu'on soit toujours sur la même
   complexité asymptotique dans le cas le pire (du $\Theta(n)$), on
   stocke uniquement les tables des $p(i)$ et des $M_{i}$ et au lieu de
   stocker une table contenant toutes les valeurs de $S_{i}$, on se
   contente de calculer récursivement les $S_{i}$ pour $i\in[0,n-1[$ tel
   que $t_{i}$ appartient à $S_{n}$.

   De manière générale, on arrive parfois en programmation dynamique dans
   une situation où une table donnant les coûts optimaux tient en mémoire
   et la table des solutions elle-même ne tient pas. Dans ce cas, cette
   technique de reconstruction peut s'avérer utile.

*)

let test3 () =
  let s = reconstruction_solution j1 in
  assertEqTaskset "Test 3" s [t5; t3; t2]
;;
(*
\finsousparties
 *)
