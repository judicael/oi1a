%TODO : revoir ce qu'il faut les inviter à lire.

\begin{partie}{Introduction}
La programmation dynamique est un ensemble de méthodes pour résoudre
des problèmes d'optimisation. Pour en montrer l'intérêt, nous
introduisons au~\ref{ex:rechnaivecheminmin} un exemple que nous allons
résoudre de façon naïve. Nous verrons que la résolution naïve ne
permet pas d'aborder des problèmes de taille conséquente.

Au~\ref{partie:memoisation}, nous verrons la technique de
mémoïsation, qui est la première méthode de programmation dynamique
à employer pour résoudre le problème. Au~\ref{partie:complprogdyn},
nous nous intéresserons aux complexités temporelle et spatiale de
cette solution pour montrer une autre méthode de programmation
dynamique permettant de réduire la complexité spatiale.

Nous nous intéresserons enfin à un autre exemple d'application des
méthodes de programmation dynamique, celui de l'ordonnancement de
tâches pondérées, au~\ref{partie:ordtachespond}.
\end{partie}

\titrepartie{Recherche naïve d'un chemin de poids minimal}
\sousparties
\label{ex:rechnaivecheminmin}

\begin{partie}{Exemple étudié}
\label{ex:progdyn}
Nous prendrons l'exemple suivant, tiré du projet Euler, problème~81 (\url{http://projecteuler.net/problem=81}):
étant donné une matrice $n\times n$ contenant des entiers, on cherche
le chemin de somme minimale allant du coin supérieur gauche au coin
inférieur droit, étant entendu qu'un chemin est une succession de
cases de la matrice et allant uniquement vers le bas ou vers la
droite.

Par exemple, sur la matrice $5\times 5$ suivante, le chemin de somme minimale
est représenté en gras. Sa somme vaut $2427$:
\begin{equation}
  \label{eq:matrice81ex}
\begin{pmatrix}
\textbf{131} & 673 & 234 & 103 & 18\\
\textbf{201} & \textbf{96} & \textbf{342} & 965 & 150\\
630 & 803 & \textbf{746} & \textbf{422} & 111\\
537 & 699 & 497 & \textbf{121} & 956\\
805 & 732 & 524 & \textbf{37} & \textbf{331}
\end{pmatrix}
\end{equation}

On cherche à écrire une fonction calculant la somme d'un chemin
minimal.

On représentera la matrice en Caml sous forme d'un tableau de
tableaux.
\end{partie}

\titrepartie{Modélisation mathématique}
\sousparties
Étant donné une matrice $M\in\cM_{p,q}(\R)$ de coefficients
$\p{m_{ij}}_{(i,j)\in \ii{0,p}\times \ii{0,q}}$, notons $c(i,j)$,
pour $0\leq i<p$ et $0\leq j<q$, la somme minimale d'un chemin allant de
la case $(0,0)$ à la case $(i,j)$ en se déplaçant uniquement vers le
bas ou vers la droite.

\emph{Une propriété essentielle du problème est que si un chemin de $(0,0)$
à $(i,j)$ est de somme minimale, tout sous-chemin de ce chemin est
aussi de somme minimale.}

Cette propriété assure qu'on a les égalités suivantes pour $0<i <p$ et
$0<j<q$:
\begin{align}
  c(0,0) &= m_{00}\\
  c(0,j)&= c(0, j-1) + m_{ij} \\
  c(i,0)&= c(i-1, 0) + m_{ij}\\
  c(i,j) &= \min(c(i-1,j), c(i, j-1)) + m_{ij}
\end{align}

Remarquons que:
\begin{itemize}
\item la première égalité vient du fait que l'unique chemin de $(0,
  0)$ à lui-même est le chemin trivial contenant juste la case
  $(0,0)$.
\item la seconde du fait que tout chemin de $(0, 0)$ à $(0, j)$ ne
  peut que rester dans la colonne $0$ puisque les déplacements ne
  peuvent pas se faire vers la gauche.
\item de même, la troisième vient du fait que les déplacements ne
  peuvent se faire vers le haut.
\item la quatrième du fait que l'avant-dernière case d'un chemin de
  de $(0,0)$ à $(i,j)$ est nécessairement $(i-1, j)$ ou $(i, j-1)$ et
  que si le chemin de $(0,0)$ à $(i, j)$ est de poids minimal, alors
  ce chemin privé de cette dernière case $(i,j)$ est aussi un chemin
  de poids minimal.
\end{itemize}
\finsousparties

\input{09-prog-dynamique-euler.ml.tex}

\titrepartie{Ordonnancement de tâches pondérées}
\label{partie:ordtachespond}
\sousparties
\input{09-prog-dynamique-ordonnancement.ml.tex}
\finsousparties

\titrepartie{Autres exemples}

Voir TP.
%\titrepartie{Mémoïsation dans une table de hachage}
%Exemple du mur de briques.

%TODO:
%Temps de vol pour la suite de syracuse (calcul du temps de vol maximum
%pour le premier million d'entiers).

%Nombre de façon de casser une tablette de chocolat.


\titrepartie{Étymologie}
\label{partie:etymologietabuler}

Le Littré à l'adjectif tabulaire indique:
  \begin{quote}
    Qui tient à l'emploi des tableaux ; qui emploie des tableaux. Logarithmes tabulaires, ceux des tables.
  \end{quote}
  et signale l'étymologie latine \textit{tabula} qui signifie table.

Le Centre National de Ressources Textuelles et Lexicales signale que
selon le Larousse encyclopédique de 1964 le verbe tabuler signifie
«Commander un espacement automatique sur une machine à écrire.» ainsi
que «passer des cartes perforées à la tabulatrice».

Qu'est-ce qu'une tabulatrice? C'est une très bonne question à laquelle
répond ce même Centre:
\begin{quote}
  Machine mécanographique pour le traitement des cartes perforées
  permettant d'effectuer des calculs et d'imprimer les données sous
  forme de listes, de tableaux, d'états en continu. Pour la
  totalisation des quantités par catégories, intervient une troisième
  machine, appelée tabulatrice. Elle consiste, essentiellement, en
  deux mécanismes conjugués: 1. Un jeu d'additionneuses à chiffreur
  intermédiaire imprimantes (...); 2. Un mécanisme de connexion, qui
  met en correspondance les perforations d'une carte et les éléments
  d'une ou de plusieurs unités de tabulation (Couffignal, Mach. calc.,
  1933, p. 56).
\end{quote}
Tout en précisant:
\begin{quote}
  Les tabulatrices sont, dans les ordinateurs modernes, presque
  toujours remplacées par des imprimantes (Commun.~1971).
\end{quote}
Il est vrai que de nos jours, on n'en voit plus beaucoup. On pourra
cependant consulter l'article de Wikipédia sur les tabulatrices et la
mécanographie (une industrie aujourd'hui disparue).
