(* Type de données abstrait des tables d'assocations impératives
   polymorphes, associant une clé de type ['key] à une valeur de type
   ['val] *)
type ('key, 'value) table_assoc = ('key * 'value) list;;

(* crée une table d'association vide *)
let empty = [];;

(* [add k v t] ajoute la liaison [(k,v)] à la table [t] *)
let add k v t  =  (k, v) :: t;;

let rec assoc k t =
  match t with
  | [] -> raise Not_found
  | (k',v')::r -> if k = k' then v' else assoc k r
;;

(* [find k t] trouve la valeur [v] associée à la clé [k] dans la table [t].
   Si la valeur n'existe pas, elle lève l'exception [Not_found]. *)
let find k t = assoc k t;;


(* [mem t k] dit s'il y a une valeur associée à la clé [k] dans [t] *)
let mem t k =
  match find t k with
  | _ -> true
  | exception Not_found -> false;;
