(* On utilise la version fonctionnelle *)

type 'a pile = 'a Piles.pile ref;;

let cree_pile () = ref Piles.pile_vide;;

let empile x s =
  s := Piles.empile x !s;;

let sommet s = Piles.sommet !s;;

let depile s =
  let t = sommet s in
  s := Piles.depile !s;
  t;;

let est_vide s = Piles.est_vide !s;;

  
