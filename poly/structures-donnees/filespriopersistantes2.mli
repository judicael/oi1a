(* type de données concret des files persistantes polymorphes.
   Les priorités sont ici des valeurs entières. *)
type 'a fileprio == ('a * int) list;;

let file_vide = [];;

(* [insere f x p] insère [x] dans la file [f] avec la priorité [p] *)
let insere f x p = (x, p) :: f;;

(* trouve_couple_max : 'a fileprio -> 'a * int
   trouve_couple_max f  retourne le couple (x, p)
   où x appartient à la file f et p est sa priorité,
   de priorité p maximale.
   Lève une exception si f ne contient aucun élément.
 *)
let rec trouve_couple_max f = match f with
  | [] -> failwith "File vide"
  | [(x, p)] -> x, p
(*  | (x, p) :: r -> let x2, p2 = trouve_couple_max r in
		   if p <= p2 then x2, p2
		   else x, p *)
  | (x1, p1) :: (x2, p2) :: r ->
    let f' = if p1 <= p2 then (x2, p2) :: r else (x1, p1) :: r in
    trouve_couple_max f'
;;

		   
(* [trouve_max f] retourne un élément de priorité maximale dans la file [f].
   Lève une exception si la file est vide. *)
let trouve_max f =
  let x, _ = trouve_couple_max f in
  x
;;

(* enleve_elem_prio : 'a fileprio -> int -> 'a fileprio
   enleve_elem_prio f p  retourne la file f privée d'un
   élément de priorité p.
   Si aucun élément n'est de priorité p, le comportement
   de enleve_elem_prio est non spécifié.
 *)
let rec enleve_elem_prio f p =
  match f with
  | [] -> []
  | (x1, p1) :: r -> if p = p1 then r else (x1, p1) :: enleve_elem_prio r p
;;

(* [enleve_max f] retourne la file de priorité [f] privée de son élément
   de plus grande priorité.
   Lève une exception si la file est vide. *)
let enleve_max f =
  let _, p = trouve_couple_max f in
  enleve_elem_prio f p
;;

(* [est_vide f] dit si la file de priorité [f] est vide *)
let est_vide f =
  f = file_vide
;;
  
