(* Type de données abstrait des tables d'assocations impératives
   polymorphes, associant une clé de type ['key] à une valeur de type
   ['val] *)
type ('key, 'val) assoc_table == ('key * 'val) vect
;;

(* crée une table d'association vide *)
let empty = [| |]
;;

(* [add k v t] ajoute la liaison [(k,v)] à la table [t] *)
let add k v (t : ('a,'b) assoc_table) =
  let n = vect_length t in
  let t' = make_vect (n+1) (k, v) in
  blit_vect t 0 t' 0 n;
  t'
;;

(* [find t k] trouve la valeur [v] associée à la clé [k] dans la table [t] et retourne [Some v].
   Si la valeur n'existe pas, elle retourne [None]. *)
let find t k =
  let n = vect_length t in
  let i = ref 0 in
  while !i < n && fst(t.(!i)) <> k do
    i := !i + 1
  done;
  if !i < n then Some(snd(t.(!i))) else None
;;

(* [mem t k] dit s'il y a une valeur associée à la clé [k] dans [t] *)
(*
let mem t k =
  match find t k with
  | Some _ -> true
  | None -> false
;;
*)

let mem t k = find t k <> None;;

