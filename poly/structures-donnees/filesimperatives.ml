
type 'a file = {
  mutable donnees : 'a array;
  (* nombre de valeurs mises depuis la création de la file *)
  mutable mises : int;
  (* nombre de valeurs enlevées depuis la création de la file *)
  mutable enlevees : int;
};;

let taille_max = 1000;;

let cree_file () = {
  donnees = [| |];
  mises = 0;
  enlevees = 0;
};;

let taille_file f = f.mises - f.enlevees;;
let est_vide f =  taille_file f = 0;;
let est_pleine f =  taille_file f = taille_max;;

let enfile f x =
  if est_pleine f then failwith "File pleine";
  if f.donnees = [| |] then f.donnees <- Array.make taille_max x;
  let t = f.donnees in
  t.(f.mises mod taille_max) <- x;
  f.mises <- f.mises + 1
;;

let defile f =
  if est_vide f then failwith "File vide";
  let x = f.donnees.(f.enlevees mod taille_max) in
  f.enlevees <- f.enlevees + 1;
  x
;;
