
(* Type de données abstrait des tables d'assocations impératives
   polymorphes, associant une clé de type ['k] à une valeur de type
   ['v] *)
type ('k, 'v) assoc_table = ('k, 'v) Hashtbl.t;;

(* on crée une table dont la taille est un nombre premier *)
let create () = Hashtbl.create 1973;;

let add t k v = Hashtbl.add t k v;;

let find k t = Hashtbl.find t k;;

let mem t k =
  (* [k] est dans [t] si et seulement si une recherche dans la table ne
     lève pas d'exception *)
  try Hashtbl.find t k; true with
  | Not_found -> false
;;
