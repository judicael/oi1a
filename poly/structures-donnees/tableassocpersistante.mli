(* Type de données abstrait des tables d'associations persistantes polymorphes,
   associant une clé de type ['k] à une valeur de type ['v] *)
type ('k, 'v) table_assoc;;

(* table d'association vide *)
val empty : ('k, 'v) table_assoc;;

(* [add k v t] retourne la table [t'] obtenue en ajoutant la liaison [(k, v)] à [t] *)
val add : 'k -> 'v -> ('k, 'v) table_assoc -> ('k, 'v) table_assoc;;

(* [find t k] retourne la valeur [v] associée à la clé [k] dans la table [t].
   Si la valeur n'existe pas, elle lève l'exception [Not_found]. *)
val find : 'k -> ('k, 'v) table_assoc -> 'v;;
    
(* [mem t k] dit s'il y a une valeur associée à la clé [k] dans [t] *)
val mem : 'k -> ('k, 'v) table_assoc -> bool;;  
