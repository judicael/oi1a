(* On implante des piles à taille bornée en utilisant des tableaux. *)

type 'a pile = {
  mutable nb_elem : int; (* nombre d'éléments dans la pile *)
  mutable elements : 'a array; (* tableau pour les stocker *)
}
;;

let taille_pile = 1000;;
(* taille maximale d'une pile par défaut *)

let cree_pile () =
  (* Pour [creer_pile], on a un gros problème : Caml ne permet pas de
   créer un tableau si on n'a pas d'élément pour l'initialiser.
   Une façon de s'en sortir est de créer initialement un tableau vide
   qu'on remplacera par un tableau de la bonne taille dès qu'on aura un premier
   élément.
   *)
  { nb_elem = 0; elements = [| |]; };;

let empile x s =
  if Array.length s.elements = 0 then
    (* premier ajout, on met un vrai tableau *)
    s.elements <- Array.make x taille_pile;
  let t = s.elements in
  let i = s.nb_elem in
  if i >= Array.length t then
    failwith "Pile pleine !";
  (* tout va bien : on ajoute l'élément dans la première case libre
     du tableau et on incrémente le compteur du nombre d'éléments *)
  t.(i) <- x;
  s.nb_elem <- i+1
;;

let sommet s =
  if s.nb_elem <= 0 then invalid_arg "Pile vide !";
  s.elements.(s.nb_elem-1);;

let depile s =
  if s.nb_elem <= 0 then invalid_arg "Pile vide !";
  s.nb_elem <- s.nb_elem - 1;
  s.elements.(s.nb_elem)
;;

let est_vide s = s.nb_elem = 0;;
