(* Implantation des files persistantes à l'aide de deux listes *)
type 'a file = { entree : 'a list; sortie : 'a list};;

let file_vide = { entree = []; sortie = []; };;

let enfile { entree = e; sortie = s } x = 
  { entree = x::e; sortie = s };;
;;

let defile f =
  let f' = 
    if f.sortie = [] then { entree = []; sortie = rev f.entree; }
    else f in
  match f' with
  | { entree = e; sortie = x::s } -> (x, { entree = e; sortie = s })
  | { entree = _; sortie = [] } -> failwith "File vide"
;;

let est_vide f =  f = file_vide;;
