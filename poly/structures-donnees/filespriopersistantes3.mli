(* type de données concret des files persistantes polymorphes.
   Les priorités sont ici des valeurs entières.
   Les files seront représentées par des listes triées par ordre
   décroissant de priorité.
*)
type 'a fileprio == ('a * int) list;;

let file_vide = [];;

(* [insere f x p] insère [x] dans la file [f] avec la priorité [p] *)
let rec insere f x1 p1 = match f with
  | [] -> [(x1, p1)]
  | (x2, p2) :: r -> 
    if p1 >= p2 then (x1, p1) :: f
    else (x2, p2) :: insere r x1 p1
;;

(* [trouve_max f] retourne un élément de priorité maximale dans la file [f].
   Lève une exception si la file est vide. *)
let trouve_max f = match f with
  | [] -> failwith "File vide"
  | (x, _) :: _ -> x
;;

(* [enleve_max f] retourne la file de priorité [f] privée de son élément
   de plus grande priorité.
   Lève une exception si la file est vide. *)
let enleve_max f = match f with
  | [] -> failwith "File vide"
  | (_, _) :: r -> r
;;

(* [est_vide f] dit si la file de priorité [f] est vide *)
let est_vide f =
  f = file_vide
;;
  
