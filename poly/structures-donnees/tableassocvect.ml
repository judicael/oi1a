(* Type de données abstrait des tables d'assocations impératives
   polymorphes, associant une clé de type ['key] à une valeur de type
   ['val] *)
type ('key, 'val) assoc_table == ('key * 'val) vect ref
;;

(* crée une table d'association vide *)
let create () = ref [| |]
;;

(* [add k v] ajoute la liaison [(k,v)] à la table [t] *)
let add (rt : ('a,'b) assoc_table) k v =
  let t = !rt in
  let n = vect_length t in
  let t' = make_vect (n+1) (k, v) in
  let () = blit_vect t 0 t' 0 n in
  rt := t'
;;

(* [find t k] trouve la valeur [v] associée à la clé [k] dans la table [t] et retourne [Some v].
   Si la valeur n'existe pas, elle retourne [None]. *)
let find rt k =
  let t = !rt in
  let n = vect_length t in
  let i = ref 0 in
  while !i < n && fst(t.(!i)) <> k do
    i := !i + 1
  done;
  if !i < n then Some(snd(t.(!i))) else None
;;

(* [mem t k] dit s'il y a une valeur associée à la clé [k] dans [t] *)
(*
let mem t k =
  match find t k with
  | Some _ -> true
  | None -> false
;;
*)

let mem t k = find t k <> None;;

