type 'a fileprio;;
(* type de données abstrait des files persistantes polymorphes.
   Les priorités sont ici des valeurs entières. *)

val file_vide : 'a fileprio;;
  (* la file de priorité vide *)

val insere : 'a fileprio -> 'a -> int -> 'a fileprio;;
  (* [insere f x p] insère [x] dans la file [f] avec la priorité [p] *)

val trouve_max : 'a fileprio -> 'a;;
  (* [trouve_max f] retourne un élément de priorité maximale dans la file [f].
     Lève une exception si la file est vide. *)

val enleve_max : 'a fileprio -> 'a fileprio;;
  (* [enleve_max f] retourne la file de priorité [f] privée de son élément de plus grande priorité.
     Lève une exception si la file est vide. *)
  
val est_vide : 'a fileprio -> bool;;
  (* [est_vide f] dit si la file de priorité [f] est vide *)
