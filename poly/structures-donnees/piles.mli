type 'a pile;;
(* type de données abstrait des piles persistantes polymorphes *)

val pile_vide : 'a pile;;
  (* la pile vide *)
val empile : 'a -> 'a pile -> 'a pile;;
  (* [empile x s] retourne la pile obtenue en mettant l'élément [x] au sommet [s] *)

val depile : 'a pile -> 'a pile;;
  (* [depile s] retourne la pile privée de son sommet.
     Lève une exception si la pile est vide. *)
  
val sommet : 'a pile -> 'a;;
  (* [sommet s] retourne le sommet de la pile.
     Lève une exception si la pile est vide. *)

val est_vide : 'a pile -> bool;;
  (* [est_vide s] dit si la pile [s] est vide *)
