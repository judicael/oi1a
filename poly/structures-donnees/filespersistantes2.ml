type 'a file == 'a list;;

let file_vide = [];;

let enfile f x = f @ [x];;

let defile f =
  match f with
  | [] -> failwith "File vide"
  | x::r -> (x, r)
;;

let est_vide f =  f = [];;
