type 'a file = 'a list;;

let file_vide = [];;

let enfile f x = x :: f;;

let defile f =
  let f' = List.rev f in
  match f' with
  | [] -> failwith "File vide"
  | x::r -> (x, List.rev r)
;;

let est_vide f =  f = [];;
