
ocamlflags:=${ocamlflags} -I $d

$d/voyageurcommerce: $d/tableassoc.cmo $d/voyageurcommerce.cmo
	ocamlc ${ocamlflags} -o $@ $^

%.cmi: %.mli
	ocamlc ${ocamlflags} -c $<

%.cmo: %.ml
	ocamlc ${ocamlflags} -c $<

$d/voyageurcommerce.cmo: $d/tableassoc.cmi
$d/tableassoc.cmo: $d/tableassoc.cmi

modules:=$d/filesimperatives $d/filespersistantes \
  $d/piles $d/pilesimperativeslistes $d/pilesimperativestableaux \
  $d/tableassoc $d/tableassochashtbl $d/tableassoclist $d/tableassocpersistante \
  $d/voyageurcommerce

$d/pilesimperativestableaux.mli: $d/pilesimperatives.mli
	cp $< $@

$d/pilesimperativeslistes.mli: $d/pilesimperatives.mli
	cp $< $@

$d/tableassochashtbl.mli: $d/tableassoc.mli
	cp $< $@

$d/tableassoclist.mli: $d/tableassoc.mli
	cp $< $@

mli:=${patsubst %,%.mli,${modules}} \
  $d/filesprioimperatives.mli $d/filespriopersistantes.mli \
  $d/hashtbl.mli $d/pilesimperatives.mli
ml:=${patsubst %,%.ml,${modules}}

$d/filesimperatives.cmo: $d/filesimperatives.cmi
$d/filespersistantes.cmo: $d/filespersistantes.cmi

# omli:=${patsubst %.mli,%.omli,${mli}}
# omlitex:=${patsubst %.omli,%.omli.tex,${omli}}
#mlitex:=${patsubst %.omli.tex,%.mli.tex,${omlitex}}
mlitex:=${patsubst %.mli,%.mli.tex,${mli}}
mltex:=${patsubst %.ml,%.ml.tex,${ml}}
cmi:=${patsubst %.mli,%.cmi,${mli}}
cmo:=${patsubst %.ml,%.cmo,${ml}}

structdonnees:=${mlitex} ${mltex}

targets:=${targets} ${cmi} ${cmo} $d/voyageurcommerce

${mlitex}: %.mli.tex: %.mli
	ocamlweb -s --noweb --no-preamble --header --no-index  --intf $< -o $@

${mltex}: %.ml.tex: %.ml
	ocamlweb -s --noweb --no-preamble --header --no-index $< -o $@

