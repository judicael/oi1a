type 'a file;;
(* type de données abstrait des files persistantes polymorphes *)

val file_vide : 'a file;;
  (* la file vide *)
val enfile : 'a file -> 'a -> 'a file;;
  (* [enfile f x] retourne la file obtenue en mettant l'élément [x] à 
     la fin de la file [f] *)

val defile : 'a file -> 'a * 'a file;;
  (* [defile f] retourne le premier élément de la file [f] ainsi que la
     file [f] privée de son premier élément.
     Lève une exception si la file est vide. *)
  
val est_vide : 'a file -> bool;;
  (* [est_vide f] dit si la file [f] est vide *)
