type 'a fileprio;;
(* type de données abstrait des files impératives polymorphes.
   Les priorités sont ici des valeurs entières. *)

val file_vide : unit -> 'a fileprio;;
  (* la file de priorité vide. *)

val insere : 'a fileprio -> 'a -> int -> unit;;
  (* [insere f x p] insère [x] dans la file [f] avec la priorité [p]. *)

val trouve_max : 'a fileprio -> 'a;;
  (* [trouve_max f] retourne l'élément de la file [f] ayant la
     priorité maximale .
     Lève une exception si la file est vide. *)

val enleve_max : 'a fileprio -> 'a;;
  (* [enleve_max f] retourne l'élément de la file [f] ayant la
     priorité maximale et l'enlève de la file.
     Lève une exception si la file est vide. *)
  
val est_vide : 'a fileprio -> bool;;
  (* [est_vide f] dit si la file de priorité [f] est vide. *)
