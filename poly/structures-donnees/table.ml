(* Type de données abstrait des tables d'associations persistantes polymorphes,
   associant une clé de type ['k] à une valeur de type ['v] *)
type ('k, 'v) table_assoc == ('k * 'v) list;;

(* table d'association vide *)
let table_vide = [];;

let add k v t = (k, v)::t;;

let rec find k t = match t with
  | [] -> raise Not_found
  | (k', v')::t' -> if k = k' then v' else find k t'
;;
    
let rec mem t k = match t with
  | [] -> false
  | (k',v')::t' -> k = k' || mem t' k
;;

