(* Type de données abstrait des tables d'assocations impératives
   polymorphes, associant une clé de type ['k] à une valeur de type
   ['v] *)
type ('k, 'v) assoc_table;;

(* crée une table d'association vide *)
val create : unit -> ('k, 'v) assoc_table;;

(* [add t k v] ajoute la liaison [(k,v)] à la table [t] *)
val add : ('k, 'v) assoc_table -> 'k -> 'v -> unit;;

(* [find t k] retourne la valeur [v] associée à la clé [k] dans la table [t].
   Si la valeur n'existe pas, elle lève l'exception [Not_found]. *)
val find : 'k -> ('k, 'v) assoc_table -> 'v;;
    
(* [mem t k] dit s'il y a une valeur associée à la clé [k] dans [t] *)
val mem : ('k, 'v) assoc_table -> 'k -> bool;;
