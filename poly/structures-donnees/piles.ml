
type 'a pile = 'a list;;

let pile_vide = [];;

let empile x s = x::s;;

let depile s =
  match s with | _::s' -> s' | [] -> invalid_arg "Pile vide.";;

let sommet s =
  match s with | x::_ -> x | [] -> invalid_arg "Pile vide.";;

let est_vide s = match s with [] -> true | _ -> false;;
