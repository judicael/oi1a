type 'a pile;;
(* type de données abstrait des piles impératives polymorphes *)

val cree_pile : unit -> 'a pile;;
  (* crée une nouvelle pile vide *)
val empile : 'a -> 'a pile -> unit;;
  (* [empile x s] empile [x] sur la pile [s] *)

val depile : 'a pile -> 'a;;
  (* [depile s] dépile le sommet de la pile [s] et le retourne.
     Lève une exception si la pile est vide. *)
  
val sommet : 'a pile -> 'a;;
  (* [sommet s] retourne le sommet de la pile sans changer la pile.
     Lève une exception si la pile est vide. *)

val est_vide : 'a pile -> bool;;
  (* [est_vide s] dit si la pile [s] est vide *)
