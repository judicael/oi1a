
#open "piles";;

type instr =
| Entier of int
| Opp
| Plus
| Moins
| Mult
| Div
;;

let calcule i p =
  match i with
  | Entier n -> empile n p
  | Opp -> let s = sommet p in
	   empile (-s) (depile p)
  | Plus -> let x = sommet p in
	    let p1 = depile p in
	    let y = sommet p1 in
	    let p2 = depile p1 in
	    empile (x+y) p2
  | Moins -> let x = sommet p in
	     let p1 = depile p in
	     let y = sommet p1 in
	     let p2 = depile p1 in
	     empile (y-x) p2
  | Mult -> let x = sommet p in
	    let p1 = depile p in
	    let y = sommet p1 in
	    let p2 = depile p1 in
	    empile (x*y) p2
  | Div -> let x = sommet p in
	   let p1 = depile p in
	   let y = sommet p1 in
	   let p2 = depile p1 in
	   empile (y/x) p2
;;
