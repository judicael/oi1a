(* Type de données abstrait des tables d'assocations impératives
   polymorphes, associant une clé de type ['key] à une valeur de type
   ['val] *)
type ('key, 'value) assoc_table = ('key * 'value) list ref;;

(* crée une table d'association vide *)
let create () = ref [];;

(* [add t k v] ajoute la liaison [(k,v)] à la table [t] *)
let add t k v  =  t := (k, v) :: !t;;

(* assoc : 'key -> ('key, 'value) list -> 'value *)
let rec assoc k t =
  match t with
  | [] -> raise Not_found
  | (k',v')::r -> if k = k' then v' else assoc k r
;;

(* [find t k] trouve la valeur [v] associée à la clé [k] dans la table [t] et la retourne.
   Si la valeur n'existe pas, elle lève l'exception [Not_found]. *)
let find k t = assoc k !t


(* [mem t k] dit s'il y a une valeur associée à la clé [k] dans [t] *)
let mem k t =
  try find t k; true with
  | Not_found -> false
