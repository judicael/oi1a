
(* [genere s] génère tous les couples de la forme
   [(s-{j}, j)] pour [j] dans [s]
   [genere : 'a list -> ('a list * 'a) list]
 *)
let rec genere s =
  match s with
  | [] -> []
  | x::s' -> let e = genere s' in
	     let e' = List.map (fun (s2, i2) -> (x::s2, i2)) e in
	     (s', x) :: e'
;;

let rec min_list l = match l with
  | [] -> invalid_arg "liste vide"
  | [x] -> x
  | x::r -> min x (min_list r)
;;

let n = 20;;
let c = Array.make_matrix n n 0.;;
for i = 0 to n-1 do
  for j = 0 to i-1 do
    c.(i).(j) <- 1.;
    c.(j).(i) <- 1.;
  done
done;;

let rec f_work t s i =
  if s = [] then c.(i).(0)
  else
    let e = genere s in
    let ce = List.map (fun (s', j) -> f_memo t s' j +. c.(i).(j)) e
    in min_list ce
and f_memo t s i =
  if Tableassoc.mem t (s, i) then Tableassoc.find (s, i) t
  else let v = f_work t s i in
       let () = Tableassoc.add t (s, i) v in
       v
;;

let voyageur () =
  let t = Tableassoc.create () in
  let c = f_memo t [ 1; 2; 3; 4; 5; 6; 7; 8;9; 10 ] 0 in
  print_float c; print_newline()
;;

voyageur();;
