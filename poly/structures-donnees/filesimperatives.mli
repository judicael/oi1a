type 'a file;;
(* type de données abstrait des files impératives polymorphes *)

val cree_file : unit -> 'a file;;
  (* crée une nouvelle file vide *)

val enfile : 'a file -> 'a -> unit;;
  (* [enfile x f] enfile [x] sur la file [f] *)

val defile : 'a file -> 'a;;
  (* [defile f] défile le sommet de la file [f] et le retourne.
     Lève une exception si la file est vide. *)
  
val est_vide : 'a file -> bool;;
  (* [est_vide f] dit si la file [f] est vide *)
