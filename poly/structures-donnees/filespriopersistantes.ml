type 'a fileprio = ('a * int) list;;

let file_vide = [];;

let insere f x p = (x, p) :: f;;

let rec max_list cmp l =
  match l with
  | [] -> failwith "Liste vide"
  | [x] -> x
  | x::r -> let y = max_list cmp r in
	    if cmp x y then y else x
;;

let trouve_max f =
  let cmp (x,p) (x',p') = p <= p' in
  let x, _ = max_list cmp f in
  x
;;

let rec enleve x l = match l with
  | [] -> []
  | y :: r -> if x = y then r else y :: enleve x r;;

let enleve_max f =
  let cmp (x, p) (x', p') = p <= p' in
  let (x, p) = max_list cmp f in
  enleve (x, p) f
;;

let est_vide f =  f = [];;
