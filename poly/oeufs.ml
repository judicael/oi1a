(*
  minint : int -> int -> (int -> int) -> int
  minint a b f renvoie le minimum de f sur l'intervalle d'entiers
  [a, b].
  Précondition : [a, b] contient au moins un entier, c'est-à-dire
  a <= b.
*)
let rec minint a b f =
  if a = b then f a else min (f a) (minint (a+1) b f);;

(*
  iminint : int -> int -> (int -> int) -> int
  iminint a b f renvoie i appartenant à [a, b] tel que (f i) est minimal.
  Précondition : [a, b] contient au moins un entier, c'est-à-dire
  a <= b.
*)
let rec iminint a b f =
  if a = b then a
  else let i = iminint (a+1) b f in
       if f a < f i then a
       else i
;;

(* e2 : int -> int
   e2 n renvoie la valeur de e(n, 2)
*)
let rec e2 n =
  if n <= 0 then 0
  else minint 1 n (fun p -> max p (e2(n-p) + 1))
;;

let e2_tab = Array.make 100 (-1);;

let rec e2 n =
  if n <= 0 then 0
  else minint 1 n (fun p -> max p (e2_memo(n-p) + 1))
and e2_memo n =
  if e2_tab.(n) = -1 then e2_tab.(n) <- e2 n;
  e2_tab.(n)
;;

(*
type 'a option =
| None
| Some of 'a
;;
*)
let e2_tab = Array.make 100 None;;

let strategie = Array.make 100 None;;

let rec e2 n =
  if n <= 0 then 0
  else
    let f = fun p -> max p (e2_memo(n-p) + 1) in
    let i = iminint 1 n f in
    let () = strategie.(n) <- Some i in
    f i
and e2_memo n =
  match e2_tab.(n) with
  | None -> let v = e2 n in
	    let () = e2_tab.(n) <- Some v in
	    v
  | Some v -> v
;;

let t = Array.make_matrix 100 100 None;;

(* précondition : n >= 0 et k >= 1 *)
let rec e n k =
  if n = 0 then 0
  else if k = 1 then n
  else 1 + minint 1 n (fun p -> max (e_memo (p-1) (k-1)) (e_memo (n-p) k))
and e_memo n k =
  match t.(n).(k) with
  | None ->
    let v = e n k in
    let () = t.(n).(k) <- Some v in
    v
  | Some v -> v
;;
  
