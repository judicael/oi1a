(*
  (fusion t1 t2) retourne un tableau t ayant les mêmes élements que t1
  et t2. t doit être trié.
  Précondition : t1 non vide, t1 trié et t2 trié.
*)

let fusion t1 t2 =
  let n1 = vect_length t1 in
  let n2 = vect_length t2 in
  let i1 = ref 0 in
  let i2 = ref 0 in
  let n = n1 + n2 in
  let t = make_vect n t1.(0) in
  for i = 0 to n - 1 do
    (* invariant :
       - i = !i1 + !i2 ;
       - !i1 <= n1;
       - !i2 <= n2;
       - les i premiers éléments de t sont les i1 premiers
         éléments de t1 et les i2 premiers éléments de t2.
       - les i premiers éléments de t sont triés et sont plus petits
         que les éléments t1.(!i1), t1.(!i1+1), ..., t1(n1-1) et
         que les éléments t2.(!i2), t2.(!i2+2), ..., t2(n2-1).
    *)
    if !i1 < n1 && (!i2 = n2 || t1.(!i1) <= t2.(!i2)) then
      begin
	t.(i) <- t1.(!i1);
	i1 := !i1 + 1;
      end
    else
      begin
	t.(i) <- t2.(!i2);
	i2 := !i2 + 1
      end
  done;
  (* les n premiers éléments de t (donc tous les éléments de t) sont
  triés et ce sont les éléments de t1 et t2. *)
  t;;
