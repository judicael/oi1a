#! /bin/sh -e

FILE=$(basename $1 .tex)

run () {
    pdflatex -file-line-error -interaction=nonstopmode \
	 -recorder -shell-escape \
	 </dev/null \
	 $FILE.tex
}

run
while egrep -i 'rerun to get' $FILE.log || egrep 'run LaTeX again.' $FILE.log
do
    echo 'Running again on $FILE.tex...'
    run
done
