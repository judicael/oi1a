% -*- compile-command: "scons -C .."; -*-
Nous aimerions justifier que les programmes que nous écrivons en Caml
sont corrects, et en particulier qu'ils terminent.

Nous allons le faire sur des exemples:

\begin{Verbatim}
(* log2_inf : int -> int
   Pour n > 0, (log2_inf n) retourne la partie entière (par défaut) du
   logarithme en base 2 de n *)
let rec log2_inf n =
  if n = 1 then 0
  else 1 + log2_inf (n/2)
;;
\end{Verbatim}

Justifier que pour tout entier $n\in\N^*$, l'appel \texttt{(log2\_inf
  $n$)} termine et retourne $\floor{\log_2(n)}$.

\begin{Verbatim}
(* log2_sup : int -> int
   Pour n > 0, (log2_sup n) retourne la partie entière (par excès) du
   logarithme en base 2 de n *)
let rec log2_inf n =
  if n = 1 then 0
  else 1 + log2_inf ((n+1)/2)
;;
\end{Verbatim}
Justifier que pour tout entier $n\in\N^*$, l'appel \texttt{(log2\_sup
  $n$)} termine et retourne $\ceil{\log_2(n)}$.

Montrer que la fonction suivante termine et est correcte:
\begin{Verbatim}
(* pgcd : int -> int -> int
   Si a et b sont deux entiers positifs ou nuls, non tous les deux
   nuls, alors
   (pgcd a b) retourne le pgcd (positif) de a et b.
*)
let rec pgcd a b =
  if b = 0 then a
  else pgcd b (a mod b)
;;
\end{Verbatim}
(NB : besoin de restreindre ce qu'on montre pour y arriver)

Terminer ne veut pas dire terminer vite :
\begin{Verbatim}
(* fib : int -> int
   (fib n) calcule le terme de rang n de la suite de Fibonacci (dont
   les termes de rang 0 et 1 sont respectivement 0 et 1).
*)
let rec fib n = match n with
  | 0 -> 0
  | 1 -> 1
  | n -> fib (n-1) + fib (n-2)
;;
fib 35;;
\end{Verbatim}

Montrer que la fonction suivante termine:
\begin{Verbatim}
(* Fonction de Ackermann
   ack : int -> int -> int *)
let rec ack m n =
  match m, n with
  | 0, _ -> n+1
  | _, 0 -> ack (m-1) 1
  | _, _ -> ack (m-1) (ack m (n-1))
;;
\end{Verbatim}

Pour les fonctions suivantes : compter le nombre d'appels récursifs,
en déduire les complexités temporelles.

Calculer également les complexités spatiales.
\begin{Verbatim}
(* split : 'a list -> 'a list * 'a list
   (split l) partage la liste l en deux listes de mêmes longueur : si
   l est de longueur n, (split l) retourne un couple (l1, l2) tel que
   l1@l2 a les mêmes éléments que l, avec même multiplicité et
   l1 et l2 sont de longueurs respectives n' et n'' où n' vaut n/2,
   arrondi par excès et n'' vaut n/2, arrondi par défaut.
 *)
let rec split l = match l with
  | [] -> [], []
  | x::r -> let l1, l2 = split r in x::l2, l1
;;

(* merge : 'a list -> 'a list * 'a list
   (merge l1 l2) retourne une liste contenant les mêmes éléments que l, 
   avec même multiplicité.
   De plus, si l1 et l2 sont des listes triées par ordre croissant,
   alors (merge l1 l2) est une liste triée.
 *)
let rec merge l1 l2 = match l1, l2 with
  | [], l | l, [] -> l
  | x1::r1, x2::r2 -> if x1 <= x2 then x1 :: merge r1 l2
		      else x2 :: merge l1 r2
;;

(* tri_fusion : 'a list -> 'a list
   (tri_fusion l) trie la liste l par ordre croissant, i.e. retourne
   une liste triée par ordre croissant ayant les mêmes éléments que l,
   avec même multiplicité.
 *)
let rec tri_fusion l = match l with
  | [] -> []
  | _ -> let l1, l2 = split l in merge (tri_fusion l1) (tri_fusion l2)
;;
\end{Verbatim}
% NB : tri_fusion comporte évidemment un gros bogue !

Exercice :
\begin{Verbatim}
(* Type ? Justification de la terminaison ? Valeur calculée ? *)
let rec f91 x = if x > 100 then x - 100
  else f91(f91(x+11))
;;
\end{Verbatim}

Complexité: exemple du tri par insertion.

\begin{Verbatim}
(* insere : 'a -> 'a list -> 'a list
   (insere x l) retourne une liste l' contenant les mêmes éléments que
   x::l avec même multiplicité. De plus si l est trié, l' est triée.
*)
let rec insere x l = match l with
  | [] -> [x]
  | y::r -> if x <= y then x::l else y::insere x r
;;

(* tri_insertion : 'a list -> 'a list
   (tri_insertion l) retourne une liste l' contenant les mêmes
   éléments que ceux de l, triée par ordre croissant. L'algorithme
   utilisé est le tri par insertion.
*)
let rec tri_insertion l = match l with
  | [] -> []
  | x::r -> insere x (tri_insertion r)
;;
\end{Verbatim}

On montre qu'une insertion coûte dans le cas le pire un temps $O(n)$
où $n$ est la longueur de la liste dans laquelle on insère.

Pour cela, on résout $C_I(n+1) = C_I(n) + O(1)$.

Puis on montre que dans le cas le pire, le tri d'une liste de longueur
$n$ coûte au plus $C_T(n)$, où $C_T(n+1) = C_I(n) + C_T(n) + O(1)$.

Résolution:
\begin{align*}
  C_T(n+1) - C_T(n) &= O(n)\\
  C_T(n+1) - C_T(n) &\leq K n & \text{pour une certaine constante
                                $K$ et à partir d'un certain rang $n_0$}\\
  C_T(n) - C_T(n_0) &\leq K n(n-n_n0)\\
  C_T(n) &= O(n^2)
\end{align*}

Pour la complexité en espace, on raisonne de même: pour montrer que la
complexité de l'insertion et celle du tri sont linéaires en la taille
de $l$.

