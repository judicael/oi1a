type 'a abu =
  | ABUVide
  | ABUNoeud of 'a abu * 'a * 'a abu
;;

let rec taille a = match a with
  | ABUVide -> 0
  | ABUNoeud(fg, _, fd) -> 1 + taille fg + taille fd
;;

let rec hauteur a = match a with
  | ABUVide -> 0
  | ABUNoeud(fg, _, fd) -> 1 + max (hauteur fg) (hauteur fd)
;;

(* affiche_prefixe : int abu -> unit *)
let rec affiche_prefixe a = match a with
  | ABUVide -> ()
  | ABUNoeud(fg, x, fd) ->
    begin
      print_int x;
      print_string " ";
      affiche_prefixe fg;
      affiche_prefixe fd;
    end
;;

(* liste_prefixe_etiquettes_quad : 'a abu -> 'a list
   retourne la liste des étiquettes de l'arbre dans l'ordre préfixe.
*)
let rec liste_prefixe_etiquettes_quad a = match a with
  | ABUVide -> []
  | ABUNoeud(fg, x, fd) ->
    x :: liste_prefixe_etiquettes_quad fg @ liste_prefixe_etiquettes_quad fd
;;

(* liste_prefixe_etiquettes_aux : 'a abu -> 'a list -> 'a list
   ajoute la liste des étiquettes de l'arbre en tête de la liste
   passée en argument *)
let rec liste_prefixe_etiquettes_aux a acc = match a with
  | ABUVide -> acc
  | ABUNoeud(fg, x, fd) ->
     let acc1 = liste_prefixe_etiquettes_aux fd acc in
     x :: liste_prefixe_etiquettes_aux fg acc1
;;

(* liste_prefixe_etiquettes : 'a abu -> 'a list
   retourne la liste des étiquettes de l'arbre dans l'ordre préfixe.
   Complexité linéaire en la taille de l'arbre.
*)
let liste_prefixe_etiquettes a = liste_prefixe_etiquettes_aux a [];;

(* liste_infixe_etiquettes_quad : 'a abu -> 'a list
   retourne la liste des étiquettes de l'arbre dans l'ordre préfixe.
*)
let rec liste_infixe_etiquettes_quad a = match a with
  | ABUVide -> []
  | ABUNoeud(fg, x, fd) ->
    liste_infixe_etiquettes_quad fg @ (x :: liste_infixe_etiquettes_quad fd)
;;

(* liste_infixe_etiquettes_aux : 'a abu -> 'a list -> 'a list
   ajoute la liste des étiquettes de l'arbre en tête de la liste
   passée en argument *)
let rec liste_infixe_etiquettes_aux a acc = match a with
  | ABUVide -> acc
  | ABUNoeud(fg, x, fd) ->
     let acc1 = liste_infixe_etiquettes_aux fd acc in
     liste_infixe_etiquettes_aux fg (x::acc1)
;;

(* liste_infixe_etiquettes : 'a abu -> 'a list
   retourne la liste des étiquettes de l'arbre dans l'ordre préfixe.
   Complexité linéaire en la taille de l'arbre.
*)
let liste_infixe_etiquettes a = liste_infixe_etiquettes_aux a [];;

let a0 = ABUNoeud(ABUVide, 42, ABUVide);;
let a1 = ABUNoeud(a0, 23, ABUVide);;
let a2 = ABUNoeud(ABUVide, -5, ABUVide);;
let a3 = ABUNoeud(ABUVide, 15, a2);;
let a4 = ABUNoeud(ABUVide, 32, ABUVide);;
let a5 = ABUNoeud(a3, 11, a4);;
let a6 = ABUNoeud(ABUVide, 5, a5);;
let a7 = ABUNoeud(a1, 12, a6);;

let () = affiche_prefixe a7;;

(* retourne le renversement de l *)
let rec renverse_quadratique l = match l with
  | [] -> []
  | x::r -> renverse_quadratique r @ [x]
;;

(* ajouter devant a les éléments de l dans l'ordre inverse *)
let rec renverse_aux l acc = match l with
  | [] -> acc
  | x::r renverse_aux r (x::acc)
;;

let renverse l = renverse_aux l [];;

type 'a file = 'a list * 'a list;;
let file_vide = [], [];;
let ajouter x (l1, l2) = (x :: l1, l2);;
let est_vide p =  p = ([], []);;
let canonise (l1, l2) =
  match l2 with
  | [] -> ([], List.rev l1)
  | _ -> (l1, l2)
;;
let enlever f =
  match canonise f with
  | [], [] -> failwith "File vide !"
  | l1, x::r2 -> x, (l1, r2)
  | _::_, [] -> assert false
;;
let rec parcours_aux p =
  if not (est_vide p) then
    let a, p' = enlever p in
    match a with
    | ABUVide -> parcours_aux p'
    | ABUNoeud(fg, x , fd) ->
       begin
         print_int x; print_string " ";
         let p'' = ajouter fd (ajouter fg p') in
         parcours_aux p''
     end
;;
let parcours a = parcours_aux (ajouter a file_vide);;

type ('a, 'b) abe =
  | Feuille of 'a
  | Noeud of ('a, 'b) abe * 'b * ('a, 'b) abe
;;

let swap t i j =
  let vi = t.(i) in
  let vj = t.(j) in
  t.(i) <- vj;
  t.(j) <- vi
;;  
let rec p t i n =
  let i1 = 2*i + 1 in
  let i2 = 2*i + 2 in
  let i3 = if i1 < n && t.(i1) > t.(i)
           then i1 else i in
  let i4 = if i2 < n && t.(i2) > t.(i3)
           then i2 else i3 in
  if i <> i4 then (swap t i i4; p t i4 n)
;;

let s t =
  let n = Array.length t in
  for i = n-1 downto 0 do
    p t i n
  done;
  for i = 0 to n-1 do
    swap t 0 (n-1-i);
    p t 0 (n-1-i)
  done
;;
