let exemple = [|
  [| 131; 673; 234; 103;  18 |];
  [| 201;  96; 342; 965; 150 |];
  [| 630; 803; 746; 422; 111 |];
  [| 537; 699; 497; 121; 956 |];
  [| 805; 732; 524;  37; 331 |];
|]
;;

let n = 100;;
let m = make_matrix n n 0;;
for i = 0 to n-1 do
  for j = 0 to n-1 do
    m.(i).(j) <- (1+i) * (2+j) + i
  done
done;;
m;;

(* calcule c(i, j) coût minimal d'un chemin de (0, 0) à (i, j) pour la
   matrice m et ajoute au tableau t les valeurs calculées (pour chaque
   (a,b) pour lequel on a calculé c(a,b), on met la valeur c(a,b) dans
   t.(a).(b).

   Précondition : t est un tableau de mêmes dimensions que m tel que
   pour tout (a,b), t.(a).(b) vaut soit c.(a).(b) soit une valeur
   bidon strictement négative.
*)
let rec cmin t m i j =
  if t.(i).(j) >= 0 then
    begin
    (*  print_string "trouvé ";
      print_int i; print_string " ";print_int j; print_newline();*)
      t.(i).(j)
    end
  else 
    begin
(*      print_string "je dois calculer ";print_int i;
      print_string " ";print_int j; print_newline ();*)
    let res =

  match i, j with
  | 0, 0 -> m.(0).(0)
  | _, 0 -> m.(i).(0) + cmin t m (i-1) 0
  | 0, _ -> m.(0).(j) + cmin t 0 (j-1)
  | _, _ -> m.(i).(j) + min (cmin t m (i-1) j) (cmin t m i (j-1))

    in
    t.(i).(j) <- res;
(*    print_string "ajouté : ";print_int i; print_string " ";print_int j;print_newline();*)
    res
    end
;;

let t = make_matrix n n (-1);;

cmin t m 99 99;;

let cmin_it m =
  let p = vect_length m in
  let q = vect_length m.(0) in
  let t = make_matrix p q (-1) in
  for i = 0 to p - 1 do
    for j = 0 to q - 1 do
      let res =

  match i, j with
  | 0, 0 -> m.(0).(0)
  | _, 0 -> m.(i).(0) + t.(i-1).(0)
  | 0, _ -> m.(0).(j) + t.(0).(j-1)
  | _, _ -> m.(i).(j) + min t.(i-1).(j) t.(i).(j-1)

      in
      t.(i).(j) <- res
    done
  done;
  t
;;

let cmin_it2 m p q =
  let ancien = ref (make_vect (q+1) (-1)) in
  for i = 0 to p do
    let nouveau = make_vect (q+1) (-1) in
    for j = 0 to q do
      let res =
	match i, j with
	| 0, 0 -> m.(0).(0)
	| _, 0 -> m.(i).(0) + !ancien.(0)
	| 0, _ -> m.(0).(j) + nouveau.(j-1)
	| _, _ -> m.(i).(j) + min !ancien.(j) nouveau.(j-1)
      in
      nouveau.(i) <- res
    done;
    (* on a fini de calculer la ligne *)
    ancien := nouveau;
  done;
  !ancien.(q)
;;

let cmin_it3 m p q =
  let t = make_vect (q+1) (-1) in
  for i = 0 to p do
    for j = 0 to q do
      let res =
	match i, j with
	| 0, 0 -> m.(0).(0)
	| _, 0 -> m.(i).(0) + t.(0)
	| 0, _ -> m.(0).(j) + t.(j-1)
	| _, _ -> m.(i).(j) + min t.(j) t.(j-1)
      in
      t.(i) <- res
    done;
  done;
  t.(q)
;;
