(* \titrepartie{Implantation récursive naïve}
\sousparties

On peut définir la matrice exemple comme suit: *)
let exemple = [|
  [| 131; 673; 234; 103;  18 |];
  [| 201;  96; 342; 965; 150 |];
  [| 630; 803; 746; 422; 111 |];
  [| 537; 699; 497; 121; 956 |];
  [| 805; 732; 524;  37; 331 |];
|]
;;

(* De manière plus générale, on pourra s'intéresser à la matrice
   $(m_{ij})_{(i,j)\in\ii{0,n}}$ où $m_{ij} = (i+1)(2+j) + i$, où $n$
   est un paramètre qu'on pourra faire varier arbitrairement.
*)
let n = 17;;
let m = Array.make_matrix n n 0;;
for i = 0 to n-1 do
  for j = 0 to n-1 do
    m.(i).(j) <- (1+i) * (2+j) + i
  done
done;;
m;;


(*c [cmin : int vect vect -> int -> int -> int]*)
(* [cmin m i j] calcule $c(i, j)$ pour la matrice [m]. Précondition:
   [0 <= i && 0 <= j].
 *)
let rec cmin m i j =
  match (i, j) with
  | (0, 0) -> m.(0).(0)
  | (0, _) -> m.(i).(j) + cmin m i (j-1)
  | (_, 0) -> m.(i).(j) + cmin m (i-1) j
  | (_, _) -> m.(i).(j) + min (cmin m (i-1) j) (cmin m i (j-1))
;;

(* On peut calculer le résultat pour notre exemple: *)
let somme_min_exemple = cmin m (n-1) (n-1);;

(*
\finsousparties
\titrepartie{Équations régissant la complexité de la version naïve}

À chaque appel de [cmin] on effectue d'éventuels appels
récursifs plus des opérations de temps constant (au plus trois additions et
un calcul de minimum). Le temps de calcul de $c(i,j)$ par notre
algorithme, pour $0\leq i<p$ et $0\leq j<q$ est donc un
$O(C(i,j))$, où $C(i, j)$ est le nombre total d'appels
à [cmin] effectués lors de l'évaluation de [cmin i j] et
vérifie pour $i$ et $j$ non nuls:
\begin{align}
  C(0,0) &= 1 \\
  C(i,0) &= 1 + C(i-1,0)\\
  C(0,j) &= 1 + C(0, j-1)\\
  C(i,j) &= 1 + C(i-1,j) + C(i, j-1)
\end{align}

Remarquons qu'on peut facilement donner un programme calculant les
valeurs de $C$ en Caml:

*)

(*c [compl : int -> int -> int] *)
(* [compl i j] calcule $C(i,j)$.
 Précondition: [i>=0 && j>=0].
 *)
let rec compl i j = match i,j with
  | 0, 0 -> 1
  | 0, _ -> 1 + compl 0 (j-1)
  | _, 0 -> 1 + compl (i-1) 0
  | _, _ -> 1 + compl (i-1) j + compl i (j-1)
;;

(* Calculons la matrice des $C(i, j)$, pour $(i,j)\in\ii{0,7}^2$ pour
   avoir une idée de la complexité de notre programme naïf:
 *)

let n2 = 7;;
let compl_exemple = Array.make_matrix n2 n2 0;;

for i = 0 to n2-1 do
  for j = 0 to n2-1 do
    compl_exemple.(i).(j) <- compl i j
  done
done
;;

(*i On peut faire imprimer les valeurs sous forme d'un tableau LaTeX *)
let print_matrix m =
  let p = Array.length m in
  for i = 0 to p - 1 do
    let q = Array.length m.(i) in
    for j = 0 to q - 2 do
      print_int m.(i).(j);
      print_string " & ";
    done;
    print_int m.(i).(q-1);
    print_string " \\\\\n";
  done
;;

let _ = print_matrix compl_exemple;;
  
(* i*)
(*l On obtient la matrice de valeurs suivantes :
\begin{equation}
  \begin{pmatrix}
    1 & 2 & 3 & 4 & 5 & 6 & 7 \\
    2 & 5 & 9 & 14 & 20 & 27 & 35 \\
    3 & 9 & 19 & 34 & 55 & 83 & 119 \\
    4 & 14 & 34 & 69 & 125 & 209 & 329 \\
    5 & 20 & 55 & 125 & 251 & 461 & 791 \\
    6 & 27 & 83 & 209 & 461 & 923 & 1715 \\
    7 & 35 & 119 & 329 & 791 & 1715 & 3431 \\
  \end{pmatrix}
\end{equation}

Il semble donc que la croissance de $C(n,n)$ soit très rapide.

\titrepartie{Résolution des équations obtenues}

\begin{exo}
  Montrer, pour $(a,b)\in\N^2$
  \begin{equation}
    C(a, b) = \binom{a+b+2}{b+1} -1
  \end{equation}
\end{exo}

\begin{rmq}
  Je n'ai pas trouvé cette formule par magie. Mon cheminement a été
  approximativement le suivant:
  \begin{itemize}
  \item en considérant l'équation $C(a,b) = 1 + C(a-1, b) + C(a, b-1)$,
    on remarque que la fonction constante de valeur $-1$ est une
    solution particulière. On cherche donc donc $C(a, b)$ sous la
    forme $D(a, b) -1$, où $D$ vérifie alors
    $D(a, b) = D(a-1, b) + D(a, b-1)$ pour $a, b > 0$;
  \item cette égalité, proche de la formule du triangle de Pascal,
    conduit à introduire $E(\alpha, \beta) = D(\alpha-\beta,
    \beta)$. $E$ vérifie alors la formule du triangle de Pascal;
  \item cependant, $E(\alpha, 0) = \alpha + 2$, ce qui conduit à
    conjecturer $E(\alpha, \beta) = \binom{\alpha+2}{\beta+1}$;
  \item on peut voir que cette conjecture implique
    $C(a, b) = \binom{a+b+2}{b+1} -1$, qui se démontre directement
    à l'aide des égalités connues sur $C$.
\end{itemize}
\end{rmq}

\begin{exo}
Montrer, pour $n$ au voisinage de $+\infty$, l'équivalent
\begin{equation}
  C(n,n) \sim \frac{4^{n+1}}{\sqrt{\pi n}}
\end{equation}
On pourra utiliser l'équivalent donné par la formule de Stirling:
\begin{equation}
  n! \sim \sqrt{2\pi n}\p{\frac{n}{e}}^{n}
\end{equation}
\end{exo}

Pour donner une idée du temps de calcul de notre algorithme, on donne
figure~\ref{tab:tempsexeccheminnaif} les temps de calcul de programmes
demandant respectivement $n$, $n\log_2 n$, $n^2$, $n^3$ et $C(n,n)$ instructions sur
un même problème de taille $n$, en supposant que le temps de calcul
d'une instruction est d'une nano-seconde.

\begin{table}
  \centering
  \begin{tabular}{rrrrrr}\toprule
    $n$ & $n$ \si{ns} & $n\log_2 n$ \si{ns} & $n^2$ \si{ns} & $n^3\si{ns}$ & $C(n,n)$ \si{ns}\\ \midrule
    \num{e1} & \SI{10}{ns} & \SI{3.3e1}{ns} & \SI{e2}{ns} & \SI{1}{\micro s} & \SI{7e2}{\micro s}\\
    \num{2e1} & \SI{20}{ns} & \SI{8.6e1}{ns} & \SI{4e2}{ns} & \SI{8}{\micro s} & \SI{5e2}{s}\\
    \num{3e1} & \SI{30}{ns} & \SI{1.5e2}{ns} & \SI{9e2}{ns} & \SI{3e1}{\micro s} & \SI{14}{ans}\\
    \num{4e1} & \SI{40}{ns} & \SI{2.1e2}{ns} & \SI{1.6}{\micro s} & \SI{6e1}{\micro s} & \SI{1.3e7}{ans}\\
    \num{e2} & \SI{100}{ns} & \SI{6.6e2}{ns} & \SI{10}{\micro s}&\SI{1}{\milli s}&\SI{e43}{ans}\\
    \num{e3} & \SI{1}{\micro s} & \SI{10}{\micro s} & \SI{1}{\milli s}&\SI{1}{s}&\SI{e593}{ans}\\
        \bottomrule
  \end{tabular}
  \caption{Temps d'exécutions pour quelques complexités}
  \label{tab:tempsexeccheminnaif}
\end{table}


\finsousparties

\titrepartie{Mémoïsation}
\label{partie:memoisation}
\sousparties
 *)

(*
L'augmentation très rapide du temps de calcul de la fonction [cmin]
peut s'expliquer par le fait qu'elle calcule de nombreuses fois la
même chose. Par exemple pour $i, j > 2$, $C(i,j)$ conduit à calculer
$C(i, j-1)$ et $C(i-1, j)$ qui conduisent chacun à calculer $C(i-1,
j-1)$. Le calcul de $C(n,n)$, demande donc deux fois le calcul de
$C(n-1, n-1)$, qui demandent chacun deux fois le calcul de $C(n-2,
n-2)$, qui eux-mêmes, etc.
 *)

(*l

La technique de \emph{mémoïsation} consiste à dresser une table des
calculs des $c(i, j)$ déjà effectués pour ne pas les refaire.
On dit qu'on \emph{tabule} la valeur de la fonction $c$.\footnote{Voir page~\pageref{partie:etymologietabuler} pour l'origine du verbe tabuler.}

On distingue essentiellement deux façons de faire:
\begin{itemize}
\item On peut effectuer une simple modification de la méthode de calcul récursif
  qu'on a employée jusqu'ici, où la table est remplie comme effet de bord des calculs effectués.
\item On peut, de façon itérative, calculer toutes les valeurs de la table
  dans un ordre convenable.
\end{itemize}
 *)
  
(*
\titrepartie{Calcul récursif}

L'idée est de donner en argument supplémentaire à la fonction [cmin]
un tableau [t] contenant les valeurs déjà calculées et, avant tout
calcul, de regarder si le résultat ne serait pas déjà présent dans la
table.
*)

(*c [cmin : int vect vect -> int vect vect -> int -> int -> int] *)
(* [cmin t m i j] calcule $c(i,j)$ et, pour certaines valeurs du couple
   $(a,b)$ dont le couple $(i,j)$, affecte [t.(a).(b)] à la valeur
   de $c(a, b)$ s'il ne la contenait pas déjà.

   Préconditions : [i < n && j < n], [t] est une matrice carrée de
   taille $n$, et pour tous entiers [a] et [b], [t.(a).(b)]
   contient soit la valeur $-1$, soit $c(a, b)$.
 *)
  
let rec cmin t m i j =
  if t.(i).(j) >= 0 then t.(i).(j)
  else
    let res = match (i, j) with
      | (0, 0) -> m.(0).(0)
      | (0, _) -> m.(i).(j) + cmin t m i (j-1)
      | (_, 0) -> m.(i).(j) + cmin t m (i-1) j
      | (_, _) -> m.(i).(j) + min (cmin t m (i-1) j) (cmin t m i (j-1))
    in
    t.(i).(j) <- res;
    res
;;

let t = Array.make_matrix n n (-1);;

cmin t m (n-1) (n-1);;

(*
\titrepartie{Calcul itératif}
L'idée est de calculer toutes les valeurs de $c(i,j)$ itérativement et
de les stocker dans une table. Pour notre problème, il est intéressant
de remarquer que les $c(i, j)$ peuvent se calculer de proche en proche
en itérant par exemple sur $i$ puis sur $j$.
*)

(*c [cmin_it : int vect vect -> int vect vect] *)
(* [cmin_it m] calcule la matrice des $c(i, j)$ associée à la
   matrice $m$ *)
let cmin_it m =
  let p = Array.length m in
  let q = Array.length m.(0) in
  let t = Array.make_matrix p q (-1) in
  for i = 0 to p-1 do
    for j = 0 to q-1 do
      let res = match (i, j) with
	| (0, 0) -> m.(0).(0)
	| (0, _) -> m.(i).(j) + t.(i).(j-1)
	| (_, 0) -> m.(i).(j) + t.(i-1).(j)
	| (_, _) -> m.(i).(j) + min t.(i-1).(j) t.(i).(j-1)
      in
      t.(i).(j) <- res
    done
  done;
  t
;;

cmin_it m;;
  
(*
\finsousparties
\titrepartie{Complexité}
\label{partie:complprogdyn}
\sousparties
\titrepartie{Complexité temporelle}

\begin{exo}
  Quelle est la complexité temporelle du calcul de $c(i,j)$ avec les
  différents algorithmes que nous avons vus?
\end{exo}
\titrepartie{Complexité spatiale}
\begin{exo}
  Quelle est la complexité spatiale du calcul de $c(i,j)$ avec les
  différents algorithmes que nous avons vus?
\end{exo}

\titrepartie{Réduction de la complexité spatiale}

On peut en fait réduire la complexité spatiale en remarquant qu'on ne
s'intéresse qu'à la dernière case de la table. Pour la calculer, il
suffit de calculer la dernière ligne. Or pour calculer une ligne, il
suffit d'avoir la ligne précédente. On peut donc calculer les lignes
de proche en proche, en en conservant au plus deux en mémoire
simultanément. Il s'agit d'une méthode classique de la programmation
dynamique.

Ici, on peut même faire un mieux et ne garder que deux morceaux de
lignes à un instant donné:
 *)

(*c [cmin_it2 : int vect vect -> int -> int -> int] *)
(* [cmin_it2 m a b] calcule $c(a,b)$ pour la matrice $m$ *)
let cmin_it2 m a b =
  (* [b] est l'indice de colonne du dernier élément des lignes que
     nous allons calculer. Il y a donc $b+1$ éléments à garder par
     ligne *)
  let t = Array.make (b+1) (-1) in
  (* De même, il y a $a+1$ lignes à considérer, la dernière étant
     d'indice [a]. *)
  for i = 0 to a do
    (* Invariant : t contient les valeurs de $c(i-1, k)$
       pour $k=0, \ldots, b$,
       avec la convention $c(-1, k) = -1$
     *)
    for j = 0 to b do
      (* Invariant : [t] contient
         les valeurs de $c(i, 0)$, \ldots, $c(i, j-1)$ puis
         $c(i-1, j)$, \ldots, $c(i-1, b)$
       *)
      let res = match (i, j) with
	| (0, 0) -> m.(0).(0)
	| (0, _) -> m.(i).(j) + t.(j-1)
	| (_, 0) -> m.(i).(j) + t.(j)
	| (_, _) -> m.(i).(j) + min t.(j) t.(i)
      in
      t.(j) <- res
    done
  done;
  t.(b)
;;

(*
\finsousparties
\titrepartie{Obtention d'un chemin minimal}
\sousparties
\titrepartie{Par calcul direct}

Au lieu de calculer les poids des chemins, on peut calculer les chemins directement:
 *)

type chemin = (int * int) list;;

(*c [poids : int vect vect -> chemin -> int] *)
(* [poids m c] calcule le poids du chemin [c] pour la matrice de poids [m] *)
let rec poids m c = match c with
  | [] -> 0
  | (i, j)::c' -> m.(i).(j) + poids m c'
;;

(*c [cmin_it3 : int vect vect -> chemin vect vect] *)
(*c [cmin_it3 m] calcule une matrice [c] de mêmes dimensions que [m]
    telle que pour tout [(i,j)], [c.(i).(j)] est le chemin de poids
    minimal de [(0,0)] à [(i, j)] (renversé).
 *)
let cmin_it3 m =
  let p = Array.length m in
  let q = Array.length m.(0) in
  (* tableau des chemins minimaux, stockés dans l'ordre inverse *)
  let c = Array.make_matrix p q [] in
  for i = 0 to p-1 do
    for j = 0 to q-1 do
      let ch = match (i, j) with
	| (0, 0) -> [(0, 0)]
	| (0, _) -> (i, j) :: c.(i).(j-1)
	| (_, 0) -> (i, j) :: c.(i-1).(j)
	| (_, _) ->
	   if poids m c.(i-1).(j) <= poids m c.(i).(j-1) then
	     (i, j) :: c.(i-1).(j)
	   else
	     (i, j) :: c.(i).(j-1)
      in
      c.(i).(j) <- ch
    done
  done;
  c
;;

cmin_it3 m;;
(*
\begin{exo}
Quelle est la complexité en temps de [cmin_it3] ?
\end{exo}

On peut choisir de calculer simultanément les chemins et leurs poids:
 *)

(*c [cmin_it4 : int vect vect -> int vect vect * chemin vect vect] *)
(*c [cmin_it4 m] calcule des matrices [t] et [c] de mêmes dimensions
    que [m] telles que pour tout [(i,j)], [c.(i).(j)] est le chemin de
    poids minimal de [(0,0)] à [(i, j)] (renversé) et [t.(i).(j)] est
    son poids.
 *)
let cmin_it4 m =
  let p = Array.length m in
  let q = Array.length m.(0) in
  (* tableau des distances minimales *)
  let t = Array.make_matrix p q (-1) in
  (* tableau des chemins minimaux, stockés dans l'ordre inverse *)
  let c = Array.make_matrix p q [] in
  for i = 0 to p-1 do
    for j = 0 to q-1 do
      let poids, ch = match (i, j) with
	| (0, 0) -> m.(0).(0), [m.(0).(0)]
	| (0, _) -> m.(i).(j) + t.(i).(j-1), m.(i).(j) :: c.(i).(j-1)
	| (_, 0) -> m.(i).(j) + t.(i-1).(j), m.(i).(j) :: c.(i-1).(j)
	| (_, _) ->
	   if t.(i-1).(j) <= t.(i).(j-1) then
	     m.(i).(j) + t.(i-1).(j), m.(i).(j) :: c.(i-1).(j)
	   else
	     m.(i).(j) + t.(i).(j-1), m.(i).(j) :: c.(i).(j-1)
      in
      t.(i).(j) <- poids;
      c.(i).(j) <- ch;
    done
  done;
  t, c
;;

cmin m;;

(* En fait on préfère bien souvent ne stocker qu'une indication, pour
   dire d'où on est venu pour aller sur une case donnée:
 *)
type provenance =
  | NullePart
  | DuHaut
  | DeGauche
;;

(*c [cmin_it5 : int vect vect -> int vect vect * provenance vect vect] *)
(*c [cmin_it5 m] calcule des matrices [t] et [pr] de mêmes dimensions
    que [m] telles que pour tout [(i,j)], [pr.(i).(j)] est le chemin de
    poids minimal de [(0,0)] à [(i, j)] (renversé).
 *)
let cmin_it5 m =
  let p = Array.length m in
  let q = Array.length m.(0) in
  (* tableau des distances minimales *)
  let t = Array.make_matrix p q (-1) in
  (* tableau des provenances *)
  let pr = Array.make_matrix p q NullePart in
  for i = 0 to p-1 do
    for j = 0 to q-1 do
      let poids, prov = match (i, j) with
	| (0, 0) -> m.(0).(0), NullePart
	| (0, _) -> m.(i).(j) + t.(i).(j-1), DeGauche
	| (_, 0) -> m.(i).(j) + t.(i-1).(j), DuHaut
	| (_, _) ->
	   if t.(i-1).(j) <= t.(i).(j-1) then
	     m.(i).(j) + t.(i-1).(j), DuHaut
	   else
	     m.(i).(j) + t.(i).(j-1), DeGauche
      in
      t.(i).(j) <- poids;
      pr.(i).(j) <- prov;
    done
  done;
  t, pr
;;

(* À partir de la matrice des provenances, on peut reconstruire un
   chemin de poids minimal de [(0, 0)] à [(i, j)] ainsi: *)

(*c [chemin : provenance vect vect -> int -> int -> chemin] *)
let chemin pr i j =
  let rec loop i j ch =
    let ch' = (i, j) :: ch in
    match pr.(i).(j) with
    | NullePart -> ch'
    | DeGauche ->  loop i (j-1) ch'
    | DuHaut -> loop (i-1) j ch'
  in
  loop i j []
;;

(*
\titrepartie{Par reconstruction a posteriori}

La dernière technique que nous avons vu se contente de donner des
indications sur la façon de reconstruire le chemin de poids
minimal. On peut en fait pousser plus loin cette idée : au lieu de
calculer ces indications, on se contente, comme précédemment de
calculer les poids des chemins minimaux dans une table [t]. Il est
alors très facile de reconstruire le chemin à partir de cette seule
table:
 *)

(*c [chemin2 : int vect vect -> int -> int -> chemin] *)
(* [chemin2 t i j] reconstruit un chemin de poids minimal de [(0, 0)]
   à [(i, j)] à partir de la table [t] des poids minimaux.
 *)
let chemin t i j =
  let rec loop i j ch =
    let ch' = (i, j) :: ch in
    match (i, j) with
    | (0, 0) -> ch'
    | (0, _) -> loop i (j-1) ch'
    | (_, 0) -> loop (i-1) j ch'
    | (_, _) ->
       if t.(i-1).(j) <= t.(i).(j-1) then
	 loop (i-1) j ch'
       else
	 loop i (j-1) ch'
  in
  loop i j []
;;

(*
\finsousparties
*)
