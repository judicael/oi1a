type jour_semaine =
  | Lundi
  | Mardi
  | Mercredi
  | Jeudi
  | Vendredi
  | Samedi
  | Dimanche
;;

Lundi;;

Mardi;;

let week_end x = match x with
  | Dimanche -> true
  | Samedi -> true
  | _ -> false
;;
  
week_end Lundi;; (* un lundi au soleil ? *)

week_end Samedi;;

true;;
type bool =
  | true
  | false
;;
