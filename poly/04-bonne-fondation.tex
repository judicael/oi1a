\pflongnumbers
\pfhidelevel{0}
\titrepartie{Relations d'ordre}
\sousparties
\begin{df}[Relation]
  Étant donné un ensemble $E$, on appelle relation $\cR$ sur $E$ toute
  partie de $E^{2}$, c'est-à-dire tout ensemble de couples $(x,y)\in
  E$.

  Étant donné une relation $\cR$ sur un ensemble $E$ et $x$ et $y$
  deux éléments de $E$, on dit que $x$ est en relation avec $y$ par
  $\cR$ et on note $x\,\cR\,y$ pour exprimer $(x,y)\in \cR$ et
  $x\not\!\!\cR\,y$ pour exprimer $(x,y)\notin \cR$.
\end{df}
Exemple: la relation $\leq$ sur $\N$ est l'ensemble $\Set{(0,0), (0,
  1), \ldots, (1, 1), (1, 2), \ldots, (2, 2), (2, 3), \ldots,
  \ldots}$.

\begin{df}
  On dit qu'une relation $\cR$ sur un ensemble $E$ est
  \begin{description}
  \item[Réflexive] si
    %\begin{equation}
     % \label{eq:refl}
     $\forall x \in E\quad x\,\cR\,x $
    %\end{equation}
  \item[Irréflexive] si
    % \begin{equation}
    %   \label{eq:irrefl}
      $\forall x \in E\quad x\not\!\!\cR\,x$
    % \end{equation}
  \item[Antisymétrique] si
    % \begin{equation}
    %   \label{eq:antisym}
      $\forall (x,y)\in E^{2}\quad (x\,\cR\,y \text{ et } y\,\cR\,x)
      \Rightarrow x = y$
    % \end{equation}
  \item[Transitive] si
    % \begin{equation}
    %   \label{eq:trans}
    $  \forall (x,y,z)\in E^{3}\quad (x,\cR\,y \text{
     et } y \,\cR\,z) \Rightarrow x\,\cR\,z $
    % \end{equation}
  \item[Un ordre] (ou une \emph{relation d'ordre}) si elle est à la fois
    réflexive, antisymétrique et transitive.
  \item[Un ordre strict] si elle est transitive et irréflexive.
  \end{description}
\end{df}
Exemple:
\begin{enumerate}
\item La relation $\cR$ sur $\Z$ définie par $x\,\cR\,y$ si et
  seulement si $x+y > 0$ n'est ni réflexive ($-1$ n'est pas en
  relation avec lui-même), ni irréflexive ($1$ est en relation avec lui-même).
\item La relation de congruence modulo $17$ sur $\Z$ est réflexive
  et transitive mais non antisymétrique. Ce n'est donc pas une relation
  d'ordre ni une relation d'ordre strict.
\item La relation $\leq$ sur $\R$ est une relation d'ordre (de même
  pour $\leq$ restreinte à toute partie de $\R$).
\item La relation $<$ sur $\R$ est une relation d'ordre strict (de
  même pour $<$ restreinte à toute partie de $\R$).
\end{enumerate}

\titrepartie{Quelques remarques très élémentaires}
On présente ici quelques résultats quasiment évidents sur les
ordres.

\begin{rmq}\label{rem:transeq}
  Soit $(E, \leq)$ un ensemble ordonné. Alors pour tout $(x,y,z)\in
  E^{3}$, si $x\leq y\leq z$, on a $x=y=z$ ou $x\neq z$.
\end{rmq}
\begin{proof} \pf{La démonstration se fait par étude de cas:}
  \step{1}{\case{$x\neq z$.}}
  \step{2}{\case{$x=z$.}}
  \begin{proof}\pf{}
    \step{2-1}{\prove{$y\leq x$}}
    \begin{proof}\pf{}
      On a $x=z$ d'après \stepref{2} et $y\leq z$ d'après les hypothèses.
    \end{proof}
    \step{2-2}{\prove{$x=y$}}
    \begin{proof}\pf{}
      Par transitivité appliquée à \stepref{2-1} et l'hypothèse $x\leq
      y$.
    \end{proof}
    \step{2-3}{\qed{}}
  \end{proof}
  \step{3}{\qed{}}
\end{proof}

\begin{df}[Ordre strict associé à un ordre]
  Soit $E$ un ensemble muni d'un ordre $\leq$. Alors la relation notée
  $<$ et
    définie par
    \begin{equation}
      \label{eq:osassoc}
      \forall (x,y)\in E^{2}\quad x< y\iff (x\leq y \text{ et }
      x\neq y)
    \end{equation}
    est appelée ordre strict associé à l'ordre $\leq$.
\end{df}
\begin{prop}
  Il s'agit d'un ordre strict.
\end{prop}
\begin{proof}
  \step{1}{\prove{$<$ irréflexive}}
  \begin{proof}\pf{}
    \step{1-1}{\assume{\qcq{$x\in E$}}\prove{$x\not<x$}}
    \begin{proof}\pf{}
      Par définition de $<$~(\ref{eq:osassoc}).
    \end{proof}
    \step{1-2}{\qed{}}
  \end{proof}
  \step{2}{\prove{$<$ transitive}}
  \begin{proof}\pf{}
    \step{2-1}{\sassume{\qcq{$(x,y,z)\in E^{3}$} avec $x<y$ et
        $y<z$}
      \prove{$x<z$}}
    \begin{proof}\pf{}
      Par définition de la transitivité.
    \end{proof}
    \step{2-2}{\prove{$x\leq y$ et $y\leq z$}}
    \begin{proof}\pf{}
      Par définition de $<$.
    \end{proof}
    \step{2-3}{\prove{$x\leq z$}}
    \begin{proof}\pf{}
      Par transitivité de $\leq$, qui est une relation d'ordre (\stepref{1}).
    \end{proof}
    \step{2-4}{\prove{$x\neq z$}}
    \begin{proof}\pf{}
      \step{2-4-1}{\sassume{$x=z$}\prove{\falsePfkwd{}}}
      \step{2-4-2}{\prove{$x=y$}}
      \begin{proof}\pf{}
        On a $x\leq y$ et $y\leq z$ (\stepref{2-2}), $x=z$
        (\stepref{2-4-1}) et antisymétrie de $\leq$ qui est une
        relation d'ordre.
      \end{proof}
      \step{2-4-3}{\prove{$x<x$}}
      \begin{proof}\pf{}
        On a $x<y$ (\stepref{2-1}) et $x=y$ (\stepref{2-4-2}).
      \end{proof}
      \step{2-4-4}{\prove{\falsePfkwd}}
      \begin{proof}\pf{}
        On a $x<x$ (\stepref{2-4-3}) et $<$ est irréflexive (\stepref{1}).
      \end{proof}
    \end{proof}
  \end{proof}
  \step{3}{\qed{}}
  \begin{proof}\pf{}
    Par définition de la notion de relation d'ordre strict.
  \end{proof}
\end{proof}

\begin{df}[Ordre associé à un ordre strict]
  Soit $E$ un ensemble muni d'un ordre strict $<$. Alors la relation notée
  $\leq$ et
    définie par
    \begin{equation}
      \label{eq:oassoc}
      \forall (x,y)\in E^{2}\quad x\leq y\iff (x< y \text{ ou }
      x = y)
    \end{equation}
    est appelée ordre associé à l'ordre strict $\leq$.
\end{df}
\begin{prop}
  Il s'agit d'un ordre.
\end{prop}
\begin{proof}
  \step{1}{\prove{$\leq$ réflexive}}
  \begin{proof}\pf{}
    \step{1-1}{\assume{\qcq{$x\in E$}}\prove{$x\leq x$}}
    \begin{proof}\pf{}
      Par définition de $\leq$ (\ref{eq:oassoc}).
    \end{proof}
    \step{1-2}{\qed{}}
    \begin{proof}\pf{}
      Par \stepref{1-1} et définition de la réflexivité.
    \end{proof}
  \end{proof}
  \step{2}{\prove{$\leq$ antisymétrique}}
  \begin{proof}\pf{}
    \step{2-1}{\sassume{\qcq{$(x,y)\in E$} avec $x\leq y$ et
        $y\leq x$ et $x\neq y$}
      \prove{\falsePfkwd}}
    \begin{proof}\pf{}
      Par définition de l'antisymétrie.
    \end{proof}
    \step{2-2}{\prove{$x<y$ et $y<x$}}
    \begin{proof}\pf{}
      Par définition de $\leq$ et \stepref{2-1}.
    \end{proof}
    \step{2-3}{\prove{$x<x$}}
    \begin{proof}\pf{}
      C'est une conséquence de la transitivité de $<$ qui est un ordre
      strict, appliquée à~\stepref{2-2}.
    \end{proof}
    \step{2-4}{\prove{\falsePfkwd}}
    \begin{proof}\pf{}
      On a $x<x$~(\stepref{2-3}) et $<$ est irréflexive
      en tant qu'ordre strict.
    \end{proof}
    \step{2-5}{\qed{}}
  \end{proof}
  \step{3}{\prove{$\leq$ transitive}}
  \begin{proof}\pf{}
    \step{3-1}{\assume{\qcq{$(x,y,z)\in E$} avec $x\leq y$ et
        $y\leq z$}
      \prove{$x\leq z$}}
    \begin{proof}\pf{}
      \step{3-1-6}{\prove{$x=y$ ou $x=z$ ou ($x<y$ et $y<z$)}}
      \step{3-1-1}{\case{$x=y$}}
      \step{3-1-2}{\case{$y=z$}}
      \step{3-1-3}{\case{$x<y$ et $y<z$}}\begin{proof}\pf{}
        Par transitivité de $<$ qui est une relation d'ordre
        strict (\stepref{3}).
      \end{proof}
      % \step{3-1-4}{Si on n'est pas dans le cas \stepref{3-1-1}, alors $x<y$}
      % \step{3-1-5}{Si on n'est pas dans le cas \stepref{3-1-2}, alors
      %   $y<z$}
    \end{proof}
  \end{proof}
\end{proof}
% \begin{proof}
%   \step{1}{\prove{$\leq$ réflexive}}
%   \begin{proof}\pf{}
%     \step{1-1}{\assume{\qcq{$x\in E$}}\prove{$x\leq x$}}
%     \begin{proof}\pf{}
%       Par définition de $\leq$ (\ref{eq:oassoc}).
%     \end{proof}
%     \step{1-2}{\qed{}}
%     \begin{proof}\pf{}
%       Par \stepref{1-1} et définition de la réflexivité.
%     \end{proof}
%   \end{proof}
%   \step{2}{\prove{$\leq$ antisymétrique}}
%   \begin{proof}\pf{}
%     \step{2-1}{\sassume{\qcq{$(x,y)\in E$} avec $x\leq y$ et
%         $y\leq x$ et $x\neq y$}
%       \prove{\falsePfkwd}}
%     \begin{proof}\pf{}
%       Par définition de l'antisymétrie.
%     \end{proof}
%     \step{2-2}{\prove{$x<y$ et $y<x$}}
%     \begin{proof}\pf{}
%       Par définition de $\leq$ et \stepref{2-1}.
%     \end{proof}
%     \step{2-3}{\prove{$x<x$}}
%     \begin{proof}\pf{}
%       C'est une conséquence de la transitivité de $<$ qui est un ordre
%       strict, appliquée à~\stepref{2-2}.
%     \end{proof}
%     \step{2-4}{\prove{\falsePfkwd}}
%     \begin{proof}\pf{}
%       On a $x<x$~(\stepref{2-3}) et $<$ est irréflexive
%       en tant qu'ordre strict.
%     \end{proof}
%     \step{2-5}{\qed{}}
%   \end{proof}
%   \step{3}{\prove{$\leq$ transitive}}
%   \begin{proof}\pf{}
%     \step{3-1}{\sassume{\qcq{$(x,y,z)\in E$} avec $x\leq y$ et
%         $y\leq z$}
%       \prove{$x\leq z$}}
%     \begin{proof}\pf{}
%       Par définition de la transitivité.
%     \end{proof}
%     \step{3-2}{\prove{$x=y$, ou $y=z$, ou ($x<y$ et $y<z$)}}
%     \begin{proof}\pf{}
%       \step{3-2-1}{\prove{$x<y$ ou $x=y$, ainsi que $y<z$ ou $y=z$}}
%       \begin{proof}\pf{}
%         Par définition de $\leq$ (\ref{eq:oassoc}) et \stepref{3-1}.
%       \end{proof}
%       \step{3-2-2}{\qed{}}
%     \end{proof}
%     \step{3-3}{\case{$x=y$}}
%     \begin{proof}\pf{}
%       \step{3-3-1}{\prove{$x\leq z$}}
%       \begin{proof}\pf{}
%         On a $x=y$ (\stepref{3-3}) et $y\leq z$ (\stepref{3-1}).
%       \end{proof}
%       \step{3-3-2}{\qed{}}
%     \end{proof}
%     \step{3-4}{\case{$y=z$}}
%     \begin{proof}\pf{}
%       \step{3-4-1}{\prove{$x\leq z$}}
%       \begin{proof}\pf{}
%         On a $y=z$ (\stepref{3-4}) et $x\leq y$ (\stepref{3-1}).
%       \end{proof}
%       \step{3-4-2}{\qed{}}
%     \end{proof}
%     \step{3-5}{\case{$x<y$ et $y<z$}}
%     \begin{proof}\pf{}
%       \step{3-5-1}{\prove{$x<z$}}
%       \begin{proof}\pf{}
%         Par transitivité de $<$ qui est une relation d'ordre
%         strict (\stepref{3}).
%       \end{proof}
%       \step{3-5-2}{\qed{}}
%     \end{proof}
%   \end{proof}
% \end{proof}
\begin{prop}
  Soit $E$ un ensemble.
  Alors pour toute relation d'ordre $\leq$ et toute relation d'ordre strict
  $<$, $\leq$ est associée à $<$ si et seulement si $<$ est
  associée à $\leq$.
\end{prop}
\begin{proof}
  \step{1}{\sassume{\qcqs{$\leq$ relation d'ordre et $<$ 
      relation d'ordre strict}}
    \prove{$<$ est l'ordre strict associé à $\leq$ si et seulement si $<$ est
      l'ordre associé à $\leq$.}}
  \step{1-1}{\sassume{\qcq{$(x,y)\in E^{2}$}}
    \prove{
      \begin{equation}
        x<y \iff (x\leq y \text{ et } x\neq y) 
      \end{equation}
      si et seulement si
      \begin{equation}
        x\leq y \iff (x<y \text{ ou } x=y)
      \end{equation}
    }}
  \begin{proof}\pf{}
    On en déduit immédiatement la double implication entre
    \begin{equation}
      \forall (x,y)\in E^{2}\quad x<y \iff (x\leq y
      \text{ et } x\neq y)
    \end{equation}
    et
    \begin{equation}
      \forall (x,y)\in E^{2}\quad x\leq y \iff (x<y \text{ ou } x=y)
    \end{equation}
  \end{proof}
  \step{1-2}{\case{$x=y$}}
  \begin{proof}\pf{}
    \step{1-2-1}{\prove{$x\not< y$}}
    \begin{proof}\pf{}
      $<$ est une relation d'ordre strict~(\stepref{1}) et on
      a $x=y$~(\stepref{1-2}).
    \end{proof}
    \step{1-2-2}{\prove{$x\leq y$}}
    \begin{proof}\pf{}
      D'après \stepref{1-2}, puisque $\leq$ est une relation
      d'ordre, donc est réflexive.
    \end{proof}
    \step{1-2-3}{\prove{$\p{x<y \iff (x\leq y \text{ et } x\neq y)}$}}
    \begin{proof}\pf{}
      En notant $\equiv$ l'égalité des valeurs de vérité des formules,
      on a
      \begin{align*}
        \p{x<y \iff (x\leq y \text{ et } x\neq y)}
        &\equiv \p{\Faux \iff (\Vrai \text{ et }\Faux)}
        & \text{ d'après \stepref{1-2}, \stepref{1-2-1} et \stepref{1-2-2}}\\
        &\equiv \Vrai
      \end{align*}
    \end{proof}
    \step{1-2-4}{\prove{$\p{x\leq y\iff(x<y \text{ ou } x=y)}$}}
    \begin{proof}\pf{}
      \begin{align*}
        \p{x\leq y \iff (x< y \text{ ou } x = y)}
        &\equiv \p{\Vrai \iff (\Faux \text{ ou }\Vrai)}
                & \text{ d'après \stepref{1-2}, \stepref{1-2-1} et \stepref{1-2-2}}\\
        &\equiv \Vrai
      \end{align*}
    \end{proof}
    \step{1-2-5}{\qed{}}
    \begin{proof}\pf{}
      \stepref{1-2-3} et \stepref{1-2-4} étant toutes deux vraies,
      elles sont équivalentes.
    \end{proof}
  \end{proof}
  \step{1-3}{\case{$x\neq y$}}
  \begin{proof}\pf{}
    \begin{align*}
      \p{x<y \iff (x\leq y \text{ et } x\neq y)} &\equiv
            \p{x< y \iff x\leq y} & \text{car $x\neq y\equiv \Faux$}\\
      &\equiv \p{x\leq y \iff x<y}&\text{car $x = y \equiv \Vrai$}\\
      &\equiv \p{x\leq y\iff(x<y \text{ ou } x=y)}
    \end{align*}
    \step{1-3-1}{\suffices{$\p{x<y \iff x\leq y}$ si et seulement si
        $\p{x\leq y \iff x<y}$}}
    \begin{proof}\pf{}
      C'est ce qu'on obtient en simplifiant les deux formules
      de~\stepref{1-1} dont on veut montrer l'équivalence en utilisant
      le fait que $x\neq y$
      est vrai et $x=y$ faux.
    \end{proof}
    \step{1-3-2}{\qed{}}
    \begin{proof}\pf{}
      L'équivalence \stepref{1-3-1} est une tautologie.
    \end{proof}
  \end{proof}
  \step{1-4}{\qed{}}
\end{proof}

\titrepartie{L'ordre lexicographique}
\begin{df}[Ordre lexicographique]
  Soit $(E_{1},\leq_{1})$ et $(E_{2},\leq_{2})$ deux ordres. Alors on définit
  l'ordre lexicographique $\leq_{lex}$ sur $E_{1}\times E_{2}$ par
  \begin{equation}
    \forall (x_{1},y_{1})\in E_{1}^{2}\forall (x_{2},y_{2})\in
    E_{2}^{2}\quad
    (x_{1},x_{2})\leq_{lex}(y_{1},y_{2}) \iff 
    \left\{
      \begin{array}{l}
        x_{1}=y_{1} \text{ et } x_{2}\leq_{2} y_{2}\\
        \text{ou } x_{1}<_{1} y_{1}
      \end{array}
    \right.
  \end{equation}
  où $<_{1}$ est l'ordre strict associé à $\leq_{1}$.
\end{df}
\begin{rmq}\label{rem:lex}
  Pour tout $(x_{1},y_{1})\in E_{1}^{2}$ et tout $(x_{2},y_{2})\in
  E_{2}^{2}$ vérifiant $(x_{1},x_{2})\leq_{lex} (y_{1},y_{2})$, on a
  $x_{1}\leq_{1} x_{2}$.
\end{rmq}
\begin{proof}
  \step{1}{\sassume{\qcqs{$(x_{1},y_{1})\in E_{1}^{2}$ et $(x_{2},y_{2})\in E_{2}^{2}$}
      avec $(x_{1},x_{2})\leq_{lex}(y_{1},y_{2})$}
    \prove{$x_{1}\leq_{1} y_{1}$}}
  \step{2}{$x_{1}<_{1}y_{1}$ ou $x_{1}=y_{1}$}
  \begin{proof}\pf{}
    Découle de la définition de $\leq_{lex}$ et du fait qu'on a
    $(x_{1},x_{2})\leq_{lex}(y_{1},y_{2})$ par \stepref{1}.
  \end{proof}
  \step{3}{\qed{}}
\end{proof}
\begin{prop}
  La relation ainsi construite est bien un ordre.
\end{prop}
\begin{proof}
  \step{1}{\assume{\qcq{$(x_{1},x_{2})\in E_{1}\times
      E_{2}$}}\prove{$(x_{1},x_{2})\leq_{lex}(x_{1},x_{2})$}}
  \begin{proof}\pf{}
    \step{1-1}{$x_{1}=x_{1}$}
    \step{1-2}{$x_{2}\leq_{2} x_{2}$}
    \begin{proof}\pf{}
      Par réflexivité de l'ordre $\leq_{2}$.
    \end{proof}
    \step{1-3}{\qed{}}
    \begin{proof}\pf{}
      Par définition de l'ordre lexicographique.
    \end{proof}
  \end{proof}
  \step{2}{\assume{\qcqs{$(x_{1}, y_{1})\in E_{1}^{2}$ et $(x_{2},
      y_{2})\in E_{2}^{2}$} avec $(x_{1},x_{2})\leq_{lex}(y_{1},y_{2})$
      et $(y_{1},y_{2})\leq_{lex}(x_{1},x_{2})$}
    \prove{$(x_{1},x_{2})=(y_{1},y_{2})$}
  }
  \begin{proof}\pf{}
    \step{2-1}{\prove{$x_{1}=y_{1}$}}
    \begin{proof}\pf{}
      \step{2-1-2}{\prove{$x_{1}\leq_{1} y_{1}$ et $y_{1}\leq_{1} x_{1}$}}
      \begin{proof}\pf{}
        Application directe de la remarque~\ref{rem:lex} et des
        hypothèses introduites au \stepref{2}.
      \end{proof}
      \step{2-1-3}{\qed{}}
      \begin{proof}\pf{}
        Par antisymétrie de $\leq_{1}$.
      \end{proof}
    \end{proof}
    \step{2-2}{\prove{$x_{2}=y_{2}$}}
    \begin{proof}\pf{}
      \step{2-2-1}{\prove{$x_{2}\leq y_{2}$ et $y_{2}\leq x_{2}$}}
      \begin{proof}\pf{}
        Par définition de l'ordre lexicographique, \stepref{2-1} et
        les hypothèses de \stepref{2}.
      \end{proof}
      \step{2-2-2}{\qed{}}
      \begin{proof}\pf{}
        Par antisymétrie de $\leq_{2}$.
      \end{proof}
    \end{proof}
    \qed{}
  \end{proof}
  \step{3}{\assume{\qcqs{$(x_{1}, y_{1}, z_{1})\in E_{1}^{3}$ et $(x_{2},
      y_{2}, z_{2})\in E_{2}^{3}$}\\ avec
    $(x_{1},x_{2})\leq_{lex}(y_{1},y_{2})$ et $(y_{1},y_{2})\leq_{lex}(z_{1},z_{2})$.
    }
    \prove{$(x_{1},x_{2})\leq_{lex}(z_{1},z_{2})$}
  }
  \begin{proof}\pf{}
    \step{3-1}{\prove{$x_{1}\leq_{1} y_{1}\leq_{1} z_{1}$}}
    \begin{proof}\pf{}
      C'est une conséquence de la remarque~\ref{rem:lex} et des
      hypothèses de \stepref{3}.
    \end{proof}
    \step{3-3}{\case{$x_{1}\neq z_{1}$}}
    \begin{proof}\pf{}
      \step{3-3-1}{\prove{$x_{1}<_{1}z_{1}$}}
      \begin{proof}\pf{}
        Par \stepref{3-3}, \stepref{3-1} et définition de l'ordre
        strict associé à $\leq_{1}$.
      \end{proof}
      \step{3-3-2}{\qed{}}
    \end{proof}
    \step{3-2}{\case{$x_{1}=y_{1}=z_{1}$}}
    \begin{proof}\pf{}
      \step{3-2-1}{\prove{$x_{2}\leq_{2}y_{2}\leq_{2} z_{2}$}}
      \begin{proof}\pf{}
        C'est une conséquence des hypothèses de \stepref{3} et de la
        définition de l'ordre lexicographique.
      \end{proof}
      \step{3-2-2}{\qed{}}
    \end{proof}
    \step{3-4}{\qed{}}
    \begin{proof}\pf{}
      La remarque~\ref{rem:transeq} appliquée à \stepref{3-1} montre qu'on est dans le cas
      \stepref{3-3} ou \stepref{3-2}.
    \end{proof}
  \end{proof}
  \step{4}{\qed{}}
\end{proof}

\titrepartie{Suites décroissantes}

\begin{df}[Suites décroissantes]
Étant donné un ensemble ordonné $(E, \leq)$, on dit qu'une suite $u$ à
valeurs dans $E$ est \emph{décroissante} pour cet ordre si pour tout
$n\in \N$, on a
\begin{equation}
  \forall n\in\N\quad u_{n+1}\leq u_{n}
\end{equation}
On dit qu'elle est \emph{strictement décroissante} si cette inégalité
est stricte.
\end{df}

\begin{df}[Suites stationnaires]
On rappelle qu'on dit qu'une suite $u$ est \emph{stationnaire} si et
seulement si elle est constante à partir d'un certain rang,
c'est-à-dire si et seulement si
\begin{equation}
  \exists N\in \N\forall n\geq N\quad u_{n}=  u_{N}
\end{equation}
\end{df}
\begin{rmq}\label{rem:prodstationnaires}
  Soit $u$ et $v$ deux suites stationnaires respectivement à valeurs dans des
  ensembles $E_{1}$ et $E_{2}$. Alors la suite
  $\p{(u_{n},v_{n})}_{n\in\N}$ est stationnaire.
\end{rmq}
\begin{proof}
  \step{1}{\pflet{$(N,N')\in \N^{2}$ tel que $u$ et $v$ soient
      respectivement constantes à partir des rangs $N$ et $N'$}}
  \begin{proof}\pf{}
    L'existence de ces entiers découle de la définition de suite stationnaire.
  \end{proof}
  \step{2}{\define{$N'' = \max(N,N')$}}
  \step{3}{$\p{(u_{n},v_{n})}_{n\in\N}$ est constante à partir du
    rang $N''$}
  \step{4}{\qed{}}
\end{proof}
\finsousparties

\titrepartie{Ordres bien fondés}
\sousparties
\titrepartie{Définition}
\begin{df}[Ordre bien fondé]
  On dit qu'un ordre $\leq$ sur un ensemble $E$ est \emph{bien fondé} si
  toute suite $u$ décroissante pour cet ordre est stationnaire. De
  manière équivalente, on dira que $(E, \leq)$ est un ensemble
  \emph{bien fondé}.
\end{df}

\begin{exo}
  Les ensembles $\N$, $\Z$, $\Z^{-}$, $\Q^+$, $\Q^-$, $\Q$, $\R$, $\C$
  munis de l'ordre usuel $\leq$ sont-ils bien fondés?
\end{exo}

\begin{rmq}\label{rem:bfdecrapcr}
  Soit $(E, \leq)$ un ensemble bien fondé et $u$ une suite
  décroissante à partir d'un certain rang. Alors $u$ est stationnaire.
\end{rmq}
\begin{proof}
  \step{1}{\pflet{$N\in\N$ tel que $u$ est décroissante à partir du
      rang $N$}}
  \begin{proof}\pf{}
    Un tel entier existe puisque $u$ est décroissante à partir d'un certain rang.
  \end{proof}
  \step{2}{\define{$v$ la suite vérifiant $\forall n\in\N \quad v_{n}
      = u_{n+N}$}}
  \step{3}{\prove{$v$ décroissante}}
  \begin{proof}\pf{}
    \step{3-1}{\assume{\qcq{$n\in\N$}}\prove{$v_{n+1}\leq v_{n}$}}
    \begin{proof}\pf{}
      On a
      \begin{align*}
        v_{n+1}&=u_{N+n+1} &\text{par \stepref{2}}\\
               &\leq u_{N+n} &\text{par décroissance de $u$ à partir de
                               $N$}\\
               &= v_{n} &\text{par \stepref{2}}
      \end{align*}
    \end{proof}
    \step{3-2}{\qed{}}
  \end{proof}
  \step{4}{\prove{$v$  stationnaire}}
  \begin{proof}\pf{}
    $(E,\leq)$ est bien fondé et $v$ est décroissante d'après \stepref{3}.
  \end{proof}
  \step{5}{\prove{$u$  stationnaire}}
  \begin{proof}\pf{}
    \step{5-1}{\pflet{$N'\in\N$ tel que $v$ est constante à partir du
        rang $N'$}}
    \begin{proof}\pf{}
      Un tel $N'$ existe, $v$ étant stationnaire.
    \end{proof}
    \step{5-2}{\prove{$u$ est constante à partir du rang $N+N'$}}
    \begin{proof}\pf{}
      \step{5-2-1}{\assume{\qcq{$n\in\N$}}\prove{$u_{n+N+N'1}=u_{n+N+N'}$}}
      \step{5-2-2}{\qed{}}
    \end{proof}
  \end{proof}
  \step{6}{\qed{}}
\end{proof}

\titrepartie{Propriétés}
\begin{prop}
  Un ordre est bien fondé si et seulement s'il n'existe pas de suite
  strictement décroissante à valeurs dans $E$.
\end{prop}
\begin{proof}
  \step{1}{\sassume{$(E, \leq)$ est un ensemble ordonné.}
    \prove{$(E, \leq)$ est mal fondé si et seulement s'il existe
      une suite strictement décroissante à valeurs dans $E$.}}
  \begin{proof}\pf{}
    C'est une simple application du principe de contraposition: pour
    montrer $A\iff B$, il suffit de montrer
    $\lnot A\iff  \lnot B$.
  \end{proof}
  \step{2}{\assume{\qcq{$u$ une suite strictement décroissante}}
    \prove{$(E, \leq)$ est mal fondé}}
  \begin{proof}\pf{}
    $u$ est une suite décroissante et non stationnaire.
  \end{proof}
  \step{3}{\assume{$(E,\leq)$ est mal fondé}
    \prove{Il existe une suite à valeurs dans $E$ strictement
      décroissante.}}
  \begin{proof}\pf{}
    \step{3-1}{\pflet{$u$ une suite à valeurs dans $E$, décroissante
        et non stationnaire.}}
    \begin{proof}\pf{}
      Une telle suite existe par définition des ordres bien
      fondés et du fait que $(E, \leq)$ est supposé mal fondé.
    \end{proof}
    \step{3-2}{\pflet{$\tau : \N \to \N$ telle que pour tout
        $n\in\N$, $u_{\tau(n)}<u_{n}$.
      }}
    \begin{proof}\pf{}
      \step{3-2-1}{\sassume{\qcq{$n\in\N$}}
        \prove{$\Set{k \in \N| u_{k}<u_{n}}\neq \emptyset$}}
      \begin{proof}\pf{}
        Si cet ensemble est non-vide il possède un plus
        petit élément et il suffit de définir $\tau(n)$ comme étant
        cet élément.
      \end{proof}
      \step{3-2-2}{\pflet{$k>n$ un entier tel que $u_{k}\neq
          u_{n}$}}
      \begin{proof}\pf{}
        Un tel entier existe, $u$ n'étant pas stationnaire.
      \end{proof}
      \step{3-2-3}{On a $u_{k}\leq u_{n}$}
      \begin{proof}\pf{}
        $u$ est décroissante.
      \end{proof}
      \step{3-2-4}{On a $u_{k}<u_{n}$}
      \begin{proof}\pf{}
        D'après les deux points précédents.
      \end{proof}
      \step{3-2-5}{\qed{}}
    \end{proof}
    \step{3-3}{\define{$\sigma : \N\to \N$ la fonction vérifiant
        \begin{align*}
          \sigma(0) &= 0\\
          \forall n\in\N\quad \sigma(n+1)&=\tau(\sigma(n))
        \end{align*}}}
    \step{3-5}{\define{$v = u \circ\sigma$}}
    \step{3-6}{$v$ est strictement décroissante}
    \begin{proof}\pf{}
      \step{3-6-1}{\assume{\qcq{$n\in\N$}}\prove{$v_{n+1}<v_{n}$}}
      \begin{proof}\pf{}
        On a
        \begin{align*}
         v_{n+1}&=u_{\sigma(n+1)} & \text{par \stepref{3-5}}\\
          &=u_{\tau(\sigma(n))} &\text{par \stepref{3-3}}\\
          &< u_{\sigma(n)} &\text{par \stepref{3-2}}\\
          &= v_{n}&\text{par \stepref{3-5}}
        \end{align*}
      \end{proof}
      \step{3-6-2}\qed{}
    \end{proof}
    \step{3-7}{\qed{}}
  \end{proof}
  \step{3-8}{\qed{}}
\end{proof}


\begin{prop}
  Soit $(E_{1}, \leq_{1})$ et $(E_{2}, \leq_{2})$ deux ensembles bien
  fondés.
  Alors le produit lexicographique de $\leq_{1}$ et $\leq_{2}$ est un
  ordre bien fondé sur $E_{1}\times E_{2}$.
\end{prop}
\begin{proof}
  \step{1}{\sassume{\qcq{$u$ une suite à valeurs dans
        $E_{1}\leq E_{2}$ décroissante pour l'ordre lexicographique}}
    \prove{$u$ stationnaire}
  }
  \begin{proof}\pf{}
    Par définition d'ordre bien fondé.
  \end{proof}
  \step{2}{\define{$v$ et $w$ les suites à valeurs respectivement
      dans $E_{1}$ et $E_{2}$ définies par
      \begin{equation}
       \forall n\in \N\quad
      (v_{n},w_{n})=u_{n} 
      \end{equation}
      }}
  \step{3}{\prove{$\forall n\in\N\quad
      (v_{n+1},w_{n+1})\leq_{lex}(v_{n},w_{n})$}}
  \step{4}{\prove{$v$ est stationnaire}}
  \begin{proof}\pf{}
    \step{4-1}{\suffices{$v$ est décroissante}}
    \begin{proof}\pf{}
      $(E_{1},\leq_{1})$ est bien fondé.
    \end{proof}
    \step{4-2}{\prove{$v$ est décroissante}}
    \begin{proof}\pf{}
      Par \stepref{3} et la remarque~\ref{rem:lex}.
    \end{proof}
    \step{4-3}{\qed{}}
  \end{proof}
  \step{5}{\pflet{$N\in\N$ tel que $v$ est constante à partir du rang $N$}}
  \begin{proof}\pf{}
    $v$ est stationnaire~(\stepref{4}) donc un tel $N$ existe.
  \end{proof}
  \step{6}{\prove{$w$ est stationnaire}}
  \begin{proof}\pf{}
    \step{6-1}{\suffices{$w$ est décroissante à partir du rang $N$}}
    \begin{proof}\pf{}
      Par la remarque~\ref{rem:bfdecrapcr} et le fait que
      $(E_{2},\leq_{2})$ est bien fondé.
    \end{proof}
    \step{6-2}{\assume{\qcq{$n\in \N$}}\prove{$w_{n+N+1}\leq_{2}w_{n+N}$}}
    \begin{proof}\pf{}
      \step{6-1-2}{\prove{$v_{n+N+1}=v_{n+N}$}}
      \begin{proof}\pf{}
        $v$ est constante à partir du rang $N$ d'après \stepref{5}.
      \end{proof}
      \step{6-1-3}{\prove{$w_{n+N+1}\leq w_{n+N}$}}
      \begin{proof}\pf{}
        D'après la définition d'ordre lexicographique, \stepref{3} et \stepref{6-1-2}.
      \end{proof}
      \step{6-1-4}{\qed{}}
    \end{proof}
    \step{6-3}{\qed{}}
  \end{proof}
  \step{7}{$u$ est stationnaire}
  \begin{proof}\pf{}
    D'après la remarque~\ref{rem:prodstationnaires}, le produit de $v$
    et $w$ qui sont deux suites stationnaires~(\stepref{4} et
    \stepref{6}) est lui-même stationnaire et $u$ est précisément ce
    produit~(\stepref{2}).
  \end{proof}
  \step{8}{\qed{}}
\end{proof}

\begin{exo}
  Soit $E_1$ et $E_2$ deux ensembles munis respectivement des
  relations d'ordre $\leq_1$ et $\leq_2$.
  On note $\leq_{prod}$ la relation d'ordre produit définie sur
  $E_1\times E_2$ par $(x_1, x_2)\leq_{prod} (y_1, y_2)$ si et
  seulement si $x_1\leq_1 y_1$ et $x_2 \leq_2 y_2$.
  \begin{enumerate}
  \item $\leq_{prod}$ est-elle alors nécessairement une relation
    d'ordre?
  \item Si on suppose que $\leq_1$ et $\leq_2$ sont bien fondées,
    peut-on affirmer que $\leq_{prod}$ est une relation d'ordre bien
    fondée?
  \end{enumerate}
\end{exo}

\titrepartie{Induction bien fondée}

On rappelle le principe de récurrence (faible) et celui de récurrence (forte):

\begin{prop}[Principe de récurrence]
  Soit $P$ un prédicat sur les entiers naturels. On dit que $P$
  est héréditaire s'il vérifie la propriété
  \begin{equation*}
    \forall n\in\N\quad (P(n) \Rightarrow P(n+1))
  \end{equation*}
  Si la propriété
  $P(0)$ est vraie et que le prédicat est héréditaire, alors le
  prédicat $P$ est vrai pour tous les entiers naturels. 
\end{prop}

\begin{prop}[Principe de récurrence forte]
  Soit $P$ un prédicat sur les entiers naturels. On dit que $P$
  est fortement héréditaire s'il vérifie la propriété:
  \begin{equation}\label{eq:herediteforte}
    \forall n\in\N\quad \big[\forall k\in \N \quad k<n \Rightarrow
        P(k)\big] \Rightarrow P(n)
  \end{equation}
  ou de façon équivalente:
  \begin{equation}\label{eq:herediteforte2}
    \forall n\in\N\quad \big[\forall k\in\ii{0, n}
        \quad P(k)\big] \Rightarrow P(n)
  \end{equation}
  Si le prédicat $P$ est héréditaire alors le prédicat $P$ est vrai
  pour tous les entiers naturels.
\end{prop}
\begin{rmq}
  Il n'est pas nécessaire de supposer en outre $P(0)$ car tout
  prédicat $P$ fortement héréditaire vérifie $P(0)$. En effet,
  d'après~(\ref{eq:herediteforte2}), tout prédicat $P$ fortement
  héréditaire vérifie en particulier
  \begin{equation*}
     \big[\forall k \in \ii{0,0} \quad P(k)\big] \Rightarrow P(0)
  \end{equation*}
  Or la quantification $\forall k \in \ii{0,0}$ est une quantification sur
  l'ensemble vide. Elle est donc vraie. On a donc $P(0)$.
\end{rmq}

\begin{rmq}
  On peut montrer le principe de récurrence forte à partir de celui de
  récurrence faible. Pour cela, il suffit d'introduire un nouveau
  prédicat, $Q$, où $Q(n)$ est défini comme étant $\forall k<n\quad
  P(k)$. On a $Q(0)$ (quantification vide) et on peut montrer que $Q$
  est héréditaire (au sens faible). D'où, d'après le principe de
  récurrence faible, $Q$ est vrai pour tous les entiers. Soit alors
  $n\in\N$. De  $Q(n+1)$, on déduit $P(n)$. Le prédicat $P$ est donc
  vrai sur tous les entiers.
\end{rmq}

De façon très similaire au principe de récurrence forte, on peut
introduire le principe de récurrence bien fondée:
\begin{prop}[Principe de récurrence bien fondée]
  Soit $(E, \leq)$ un ensemble bien fondé et $P$ un prédicat sur
  $E$. On dit que $P$ est héréditaire s'il vérifie la propriété
  suivante:
  \begin{equation}\label{eq:herediteordrebienfonde}
    \forall n\in E\quad \big[\forall k \in E \quad k <n \Rightarrow
        P(k)\big] \Rightarrow P(n)
  \end{equation}
  Dans ce cas, le prédicat $P$ est vrai sur tous les éléments de $E$.
\end{prop}

Une autre formulation de ce principe est le suivant:
\begin{prop}[Méthode de la descente infinie]
  Soit $(E, \leq)$ un ensemble bien fondé et $P$ un prédicat sur
  $E$. On suppose que $P$ vérifie la propriété suivante: pour tout
  $n\in E$ tel que $P(n)$ est faux, il existe $k<n$ tel que $P(k)$
  soit faux également.

  Alors $P$ est vraie sur tous les éléments de $E$.  
\end{prop}
\begin{rmq}[Justification]
  Ce principe est sans doute plus évident que le principe de
  récurrence bien fondé. En effet, si $P$ vérifie la propriété et que
  $P$ n'est pas vraie sur tous les éléments de $E$, on peut trouver
  $x_0\in E$ tel que $P(x_0)$ soit faux. Alors, d'après l'hypothèse,
  on peut trouver $x_1<x_0$ tel que $P(x_1)$ soit faux également,
  puis $x_2<x_0$ tel que $P(x_2)$ soit faux, etc. On construit ainsi
  une suite infinie décroissante dans $E$ (d'où le nom du principe),
  ce qui est absurde, $\leq$ étant bien fondé. Donc si le prédicat $P$
  vérifie la propriété donnée, il est vrai sur tous les éléments de
  $E$.
\end{rmq}
\begin{rmq}[Équivalence des deux principes]
  La propriété demandée sur $P$ pour la méthode de la descente
  infinie peut s'écrire:
  \begin{equation*}\label{eq:descenteinfinie}
    \forall n\in E\quad (\lnot P(n)) \Rightarrow \exists k\in E\quad
    (k<n \text{ et } \lnot P(k))
  \end{equation*}
  Or $(\lnot P(n)) \Rightarrow \exists k\in E\quad
    (k<n \text{ et } \lnot P(k))$
  est simplement la contraposée de $\big[\forall k \in E \quad k <n \Rightarrow
    P(k)\big] \Rightarrow P(n)$.
  Les propriétés~\ref{eq:descenteinfinie}
  et~(\ref{eq:herediteordrebienfonde}) sont donc
  équivalentes!
\end{rmq}

\begin{ex}
  Soit $k\in\N^*$ tel que $\sqrt{k}\notin \N$. Montrons alors
  $\sqrt{k}\notin\Q$, c'est-à-dire que pour tout couple d'entiers $(m,
  n) \in \N\times \N^*$, on a $\sqrt{k} \neq m / n$.  L'ordre produit
  sur $\N\times \N^*$ étant bien fondé, on peut procéder par méthode
  de descente infinie: étant donné $(m,n)\in\N\times \N^*$ tel que
  $\sqrt{k}=m/n$, il suffit de montrer montrons qu'il existe
  $(m', n')\in \N\times\N^*$ vérifiant $(m',n') < (m, n)$ et $\sqrt{k}
  = m'/n'$ pour montrer le résultat.

  $\sqrt{k}$ n'étant pas un entier, il s'écrit $q + x$ où $q =
  \floor{\sqrt{k}}$ et $x \in ]0, 1[$. 
  Posons $m' = mx$ et $n' = nx$.
  On a évidemment $\sqrt{k} = m'/n'$. Et de plus, $m' < m$ et $n' <
  n$.
  Il suffit de montrer que $m'$ et $n'$ sont entiers pour conclure.
  Or
  \begin{align*}
    n' &= n\p{\sqrt{k} - q} = n\p{\frac{m}{n} - q} = m - nq\\
    \text{et } m' &= m\p{\sqrt{k} - q} = m\p{\frac{k}{\sqrt{k}} - q}
                    = m\p{k \frac{n}{m} - q} = kn - mq
  \end{align*}
  Donc $(m', n') \in \N\times\N^*$ et $(m',n') < (m, n)$ pour
  l'ordre produit sur $\N$, d'où le résultat.
\end{ex}

\begin{exo}
  On admet qu'il existe une fonction $f:\N\times\N \to \N$ vérifiant
  les propriétés suivantes:
  \begin{align*}
    f(0, 0) &= 0\\
    \forall n\in \N \quad \forall k\in \N^{0} \quad f(n, k) &= 2 (f(n,
    k-1))^5 + 3\\
    \forall n\in \N^{*} \quad f(n, 0) &= \sum_{k=0}^{3n^2-1} (f(n-1,
    k)-1)^2
  \end{align*}
  Montrer que pour tout $(n, k)\in \N^2$, $f(n,k)$ est divisible par $3$.
\end{exo}

\finsousparties
