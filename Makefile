default: all

#ocamlweb:=/home/judicael/.opam/4.02.2+improved-errors/bin/ocamlweb
ocamlweb:=ocamlweb

targets:=

d:=tp
include $d/Make.mk

d:=ds-2018
include $d/Make.mk

d:=dm-2018
include $d/Make.mk

d:=exercices
include $d/Make.mk

all: ${targets}

info:
	${info ${targets}}

TEXINPUTS=latexlib//:

%.pdf: %.tex
	env TEXINPUTS=latexlib//:exercices//: tools/pdflatex.sh $<

# pdfbook met son résultat dans le répertoire courant, d'où le cd.
# --short-edge doit être la première option
%-book.pdf: %.pdf
	cd $(dir $<) && pdfbook --short-edge --quiet \
                      --paper a3paper --suffix book $(notdir $<)

# on veut effacer les pdf si latex échoue :
.DELETE_ON_ERROR:

.SUFFIXES: .ml.tex .ml .tex .mli .cmo .cmi

%.cmo: %.ml
	ocamlc -c -o $@ $<

%.ml.tex: %.ml
	${ocamlweb} -s --noweb --no-preamble --header --no-index -o $@ $<

%.pdf: %.asy
	cd $(dir $<); asy $(notdir $<)
