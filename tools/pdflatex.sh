#! /bin/sh -ex

FILE=$(basename "$1" .tex)
DIR=$(dirname "$1")

run () {
    pdflatex \
	-output-directory "$DIR" \
	-file-line-error -interaction=nonstopmode \
	 -recorder -shell-escape \
	 </dev/null \
	 "$FILE.tex"
}

run
while egrep -i 'rerun to get' "$DIR/$FILE.log" || egrep 'run LaTeX again.' "$DIR/$FILE.log"
do
    echo 'Running again on $DIR/$FILE.tex...'
    run
done
